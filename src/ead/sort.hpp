/*******************************************************************************
 * Open Source License
 *
 * Copyleft (l) 2010 Ramon Salvador Soto Mathiesen (http://www.stermon.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyleft notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software shall be used for Good, not Evil.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYLEFT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * License based on Douglas Crockford modified MIT Open Source License:
 * http://www.opensource.org/licenses/mit-license.html
 * 
 ******************************************************************************/
#ifndef EAD_SORT_HPP
#define EAD_SORT_HPP

#include <algorithm>
#include <cmath>
#include <functional>
#include <iterator>
using std::inplace_merge;
using std::advance;
using std::binary_function;
using std::bind2nd;
using std::ceil;
using std::distance;
using std::inplace_merge;
using std::iterator_traits;
using std::less;
using std::log10;
using std::max;
using std::merge;
using std::min;
using std::nth_element;
using std::partition;
using std::swap;

#include <tr1/memory>
using std::tr1::shared_ptr;

#include "../ead++/ead++.hpp"

namespace ead{
  template<typename RandomAccessIterator, typename Comparator>
  class Mergesort : public Job{
    RandomAccessIterator f,l;
    Comparator c;
  public:
    Mergesort(RandomAccessIterator first, RandomAccessIterator last, 
	      Comparator comp)
      : Job(), f(first), l(last), c(comp) { }
    void run(){
      mergesort_(f,l,c);
      done();
    }
  };

  template<typename RandomAccessIterator, typename Comparator>
  void mergesort_(RandomAccessIterator first, RandomAccessIterator last, 
		  Comparator comp){
    size_t n(distance(first,last));
    if(1 >= n){ return; }
    spj j(gtp_add_optimized_args_three<Mergesort
          <RandomAccessIterator,Comparator> >
	  (mergesort_<RandomAccessIterator,Comparator>,first,first+(n >> 1),
	   comp));
    mergesort_(first+(n >> 1),last,comp);
    gtp_sync(j);
    inplace_merge(first,first+(n >> 1),last,comp);

    //try with merge? Merge is TO SLOW !!!
    /*
    {
      typename iterator_traits<RandomAccessIterator>::value_type * a = 
	new typename iterator_traits<RandomAccessIterator>::value_type[n];
      merge(first,first+(n >> 1),first+(n >> 1),last,a);
      RandomAccessIterator ai(a); RandomAccessIterator gai(first);
      while(ai != a+n){ *gai++ = *ai++; }
      delete[] a;
    }
    */
  }

  template<typename RandomAccessIterator, typename Comparator>
  void mergesort(RandomAccessIterator first, RandomAccessIterator last,
		 Comparator comp, const size_t & nr_threads = 1, 
		 const bool & heap = false, const size_t & buffer = 0){
    gtp_init(nr_threads,heap,buffer);
    spj j(new Mergesort<RandomAccessIterator,Comparator>(first,last,comp)); 
    gtp_add(j);
    gtp_sync_all();
    gtp_stop();
  }

  template<typename RandomAccessIterator>
  void mergesort(RandomAccessIterator first, RandomAccessIterator last,
                 const size_t & nr_threads = 1, const bool & heap = false,
                 const size_t & buffer = 0){
    mergesort<RandomAccessIterator,less<typename iterator_traits
      <RandomAccessIterator>::value_type> >
      (first,last,less<typename iterator_traits
       <RandomAccessIterator>::value_type>(),nr_threads, heap, buffer); 
  }

  template<typename RandomAccessIterator, typename Comparator>
  class Quicksort : public Job{
    RandomAccessIterator f,l;
    Comparator c;
  public:
    Quicksort(RandomAccessIterator first, RandomAccessIterator last, 
	      Comparator comp)
      : Job(), f(first), l(last), c(comp) { }
    void run(){
      quicksort_(f,l,c);
      done();
    }
  };

  template<typename RandomAccessIterator, typename Comparator>
  void quicksort_(RandomAccessIterator first, RandomAccessIterator last, 
		  Comparator comp){
    //A)
    if(first == last){ return; }
    --last;
    RandomAccessIterator q = 
      partition(first,last,bind2nd(comp,*last));
    swap(*q,*last);
    gtp_add_optimized_args_three<Quicksort
      <RandomAccessIterator,Comparator> >
      (quicksort_<RandomAccessIterator,Comparator>,first,q,comp);
    quicksort_(++q,++last,comp);

    //B)
    /*
    if(2 > (last - first)){ return; }
    nth_element(first,first+((last - first) >> 1),last);
    RandomAccessIterator q = first+((last - first) >> 1);
    gtp_add_optimized_args_two<Quicksort
      <RandomAccessIterator> >(quicksort_<RandomAccessIterator>,first,q);
    quicksort_(q,last);
    */
  }

  template<typename RandomAccessIterator, typename Comparator>
  void quicksort(RandomAccessIterator first, RandomAccessIterator last,
		 Comparator comp, const size_t & nr_threads = 1, 
		 const bool & heap = false, const size_t & buffer = 0){
    gtp_init(nr_threads,heap,buffer);
    spj j(new Quicksort<RandomAccessIterator,Comparator>(first,last,comp)); 
    gtp_add(j);
    gtp_sync_all();
    gtp_stop();
  }

  template<typename RandomAccessIterator>
  void quicksort(RandomAccessIterator first, RandomAccessIterator last,
                 const size_t & nr_threads = 1, const bool & heap = false,
                 const size_t & buffer = 0){
    quicksort<RandomAccessIterator,less<typename iterator_traits
      <RandomAccessIterator>::value_type> >
      (first,last,less<typename iterator_traits
       <RandomAccessIterator>::value_type>(),nr_threads, heap, buffer); 
  }

  double log2(double n){
    return (1.0/log10(2))*log10(n);
  }

  template<typename T>
  void comparator(T &x, T &y){
    T x_(min(x,y));
    T y_(max(x,y));
    x = x_;
    y = y_;
  }

  template<typename RandomAccessIterator>
  class HalfCleaner : public Job{
    RandomAccessIterator f,l;
    const size_t m_;
  public:
    HalfCleaner(RandomAccessIterator first, RandomAccessIterator last, 
                const size_t & m)
      : Job(), f(first), l(last), m_(m) { }
    static
    void half_cleaner(RandomAccessIterator first, RandomAccessIterator last, 
                      const size_t & m){
      size_t n(distance(first,last));
      if((m + 1) == n){ 
        comparator(*first,*--last); 
        return; 
      }
      size_t s((n - m) >> 1);
      spj j(new HalfCleaner<RandomAccessIterator>(first,first+m+s,m));
      gtp_add(j);
      half_cleaner(first+s,last,m);
      gtp_sync(j);
    }
    void run(){
      half_cleaner(f,l,m_);
      done();
    }
  };

  template<typename RandomAccessIterator>
  class BitonicSorter : public Job{
    RandomAccessIterator f,l;
  public:
    BitonicSorter(RandomAccessIterator first, RandomAccessIterator last)
      : Job(), f(first), l(last) { }
    static
    void bitonic_sorter(RandomAccessIterator first, RandomAccessIterator last){
      size_t n(distance(first,last));
      HalfCleaner<RandomAccessIterator>::half_cleaner(first,last,n >> 1);
      if(2 >= n){ return; }
      spj j(new BitonicSorter<RandomAccessIterator>(first,first+(n >> 1)));
      gtp_add(j);
      bitonic_sorter(first+(n >> 1),last);
      gtp_sync(j);
    }
    void run(){
      bitonic_sorter(f,l);
      done();
    }
  };

  template<typename RandomAccessIterator>
  class MergerHelper : public Job{
    RandomAccessIterator f,l;
    const size_t m__;
  public:
    MergerHelper(RandomAccessIterator first, RandomAccessIterator last, 
                 const size_t & m)
      : Job(), f(first), l(last), m__(m) { }
    static
    void merger_helper(RandomAccessIterator first, RandomAccessIterator last,
                       const size_t & m){
      if(2 == m){
        comparator(*first,*--last); 
        return; 
      }
      size_t m_(m >> 1);
      spj j(new MergerHelper<RandomAccessIterator>(first,last,m_)); 
      gtp_add(j);
      merger_helper(first+(m_ >> 1),last-+(m_ >> 1),m_);
      gtp_sync(j);
    }
    void run(){
      merger_helper(f,l,m__);
      done();
    }
  };

  template<typename RandomAccessIterator>
  class Merger : public Job{
    RandomAccessIterator f,l;
  public:
    Merger(RandomAccessIterator first, RandomAccessIterator last)
      : Job(), f(first), l(last) { }
    static
    void merger(RandomAccessIterator first, RandomAccessIterator last){
      size_t n(distance(first,last));
      MergerHelper<RandomAccessIterator>::merger_helper(first,last,n);
      if(2 >= n){ return; }
      spj j(new BitonicSorter<RandomAccessIterator>(first,first+(n >> 1)));
      gtp_add(j);
      BitonicSorter<RandomAccessIterator>::bitonic_sorter(first+(n >> 1),last);
      gtp_sync(j);
    }
    void run(){
      merger(f,l);
      done();
    }
  };

  template<typename RandomAccessIterator>
  class Sorter : public Job{
    class SorterHelper : public Job{
      RandomAccessIterator f,l;
    public:
      SorterHelper(RandomAccessIterator first, RandomAccessIterator last)
        : Job(), f(first), l(last) { }
      static
      void sorter_helper(RandomAccessIterator first, RandomAccessIterator last){
        size_t n(distance(first,last));
        if(2 >= n){ return; }
        spj j(new Sorter(first,first+(n >> 1))); 
        gtp_add(j);
        Sorter<RandomAccessIterator>::sorter(first+(n >> 1),last);
        gtp_sync(j);
      }
      void run(){
        sorter_helper(f,l);
        done();
      }
    };
    
    RandomAccessIterator f,l;
  public:
    Sorter(RandomAccessIterator first, RandomAccessIterator last)
      : Job(), f(first), l(last) { }
    static
    void sorter(RandomAccessIterator first, RandomAccessIterator last){
      SorterHelper::sorter_helper(first,last);
      Merger<RandomAccessIterator>::merger(first,last);
    }
    void run(){
      sorter(f,l);
      done();
    }
  };

  template<typename RandomAccessIterator>
  class FindMax : public Job{
    RandomAccessIterator f,l;
    shared_ptr<size_t> m;
  public:
    FindMax(RandomAccessIterator first,RandomAccessIterator last,
            shared_ptr<size_t> nr)
      : Job(), f(first), l(last), m(nr) { }
    static
    void find_max(RandomAccessIterator first, RandomAccessIterator last,
                  shared_ptr<size_t> nr){
      size_t n(distance(first,last));
      if(1 == n){ *nr = *first; return; }
      shared_ptr<size_t> f1(new size_t(0));
      shared_ptr<size_t> f2(new size_t(0));
      spj j(new FindMax<RandomAccessIterator>(first,first+(n>>1),f1));
      gtp_add(j);
      find_max(first+(n>>1),last,f2);
      gtp_sync(j);
      *nr = max(*f1,*f2);
    }
    void run(){
      find_max(f,l,m);
      done();
    }
  };

  template<typename RandomAccessIterator>
  class Populate : public Job{
    RandomAccessIterator f1,l1,f2,l2;
    const size_t & x_;
  public:
    Populate(RandomAccessIterator first1, RandomAccessIterator last1,
             RandomAccessIterator first2, RandomAccessIterator last2,
             const size_t & x) 
      : Job(), f1(first1), l1(last1), f2(first2), l2(last2), x_(x) { }
    static 
    void 
    populate(RandomAccessIterator first1, RandomAccessIterator last1,
             RandomAccessIterator first2, RandomAccessIterator last2,
             const size_t & x){
      size_t n(distance(first1,last1));
      size_t n_(distance(first2,last2));
      if(1 == n_){ *first2 = x; if(1 == n){ *first2 = *first1; } return; }
      if((n_ >> 1) <= n){
        spj j(new Populate<RandomAccessIterator>(first1,first1+(n_ >> 1),
                                                 first2,first2+(n_ >> 1),x));
        gtp_add(j);
        populate(first1+(n_ >> 1),first1+(n_ >> 1)+(n - (n_ >> 1)),
                 first2+(n_ >> 1),last2,x);
        gtp_sync(j);
      }else{
        spj j(new Populate<RandomAccessIterator>(first1,last1,
                                                 first2,first2+(n_ >> 1),x));
        gtp_add(j);
        populate(last1,last1,first2+(n_ >> 1),last2,x);
        gtp_sync(j);
      }
    }
    void run(){
      populate(f1,l1,f2,l2,x_);
      done();
    }
  };

  template<typename RandomAccessIterator>
  class Sorter_ : public Job{
    RandomAccessIterator f,l;
  public:
    Sorter_(RandomAccessIterator first, RandomAccessIterator last)
      : Job(), f(first), l(last) { }
    void sorter_(RandomAccessIterator first, RandomAccessIterator last){
      const size_t n(distance(first,last));
      const size_t n_((1 << static_cast<size_t>(ceil(log2(n)))));
      if(n == n_){ Sorter<RandomAccessIterator>::sorter(first,last); return; }
      int a[n_];
      shared_ptr<size_t> m(new size_t(0));
      FindMax<RandomAccessIterator>::find_max(first,last,m);
      Populate<RandomAccessIterator>::populate(first,last,a,a+n_,*m);
      Sorter<RandomAccessIterator>::sorter(a,a+n_);
      Populate<RandomAccessIterator>::populate(a,a+n_,first,last,*m);
    }
    void run(){
      sorter_(f,l);
    }
  };

  template<typename RandomAccessIterator>
  void sorter_(RandomAccessIterator first, RandomAccessIterator last,
               const size_t & nr_threads = 1, const bool & heap = false,
               const size_t & buffer = 0){
    gtp_init(nr_threads,heap,buffer);
    spj j(new Sorter_<RandomAccessIterator>(first,last)); gtp_add(j);
    gtp_sync_all();
    gtp_stop();
  }
}
#endif //EAD_SORT_HPP

