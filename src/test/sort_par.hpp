#ifndef TEST_SORT_PAR_HPP
#define TEST_SORT_PAR_HPP

#include "sort.hpp"

template<typename RandomAccessIterator>
class Sort : public Process{
  void (*fun)(RandomAccessIterator f, RandomAccessIterator l, const size_t & k);
  RandomAccessIterator f,l;
  const size_t k;
public:
  Sort(void (*function)(RandomAccessIterator first, RandomAccessIterator last, 
			const size_t & k), RandomAccessIterator first, 
       RandomAccessIterator last, const size_t & nr_threads) 
    : fun(function), f(first), l(last), k(nr_threads) {}
  void run(){
    fun(f,l,k);
    correctness(f,l);
  }
};

#endif //TEST_SORT_PAR_HPP

