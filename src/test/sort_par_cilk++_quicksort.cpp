#include "../cilk++/sort.hpp"
using cilkpp::quicksort;

#include "sort_seq.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last){
  quicksort(first,last);
}

#include "sort_par_cilk++.cpp"

