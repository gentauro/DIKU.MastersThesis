#ifndef TEST_MST_PAR_HPP
#define TEST_MST_PAR_HPP

#include "mst.hpp"

template<typename RandomAccessIterator1,typename RandomAccessIterator2>
class MST : public Process{
  void (*fun) (const Graph<RandomAccessIterator1,RandomAccessIterator2>&,
	       vector<Edge>&,const size_t &);
  Graph<RandomAccessIterator1,RandomAccessIterator2> g;
  const size_t k;
public:
  MST(void (*function)
      (const Graph<RandomAccessIterator1,RandomAccessIterator2>&,vector<Edge>&, 
       const size_t & nr_threads),
      const Graph<RandomAccessIterator1,RandomAccessIterator2> & graph, 
      const size_t & nr_threads) 
    : fun(function), g(graph), k(nr_threads) {}
  void run(){
    vector<Edge> t;
    fun(g,t,k);
    correctness(g,t);
  }
};

#endif //TEST_MST_PAR_HPP

