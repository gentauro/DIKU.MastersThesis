#include "../seq/mst.hpp"
using sequential::mst_boruvka;

#include "mst_seq.hpp"

void f(const Graph<Vertex*,Edge*> & g, vector<Edge> & t){
  t = mst_boruvka<Vertex*,Edge*>(g);
}

#include "mst_seq.cpp"

