#! /bin/bash
################################################################################
# ./sort.bash 1 27 1 4 <--- from 1 to 27 perform desired action with 1 to 4
#                           threads
################################################################################
shopt -s nullglob
gc=$1
dir=$(date +'%Y-%m-%d_%H:%M:%S')
mkdir output/$dir
until [ $gc -gt $2 ]; do
    # sequential
    for f in sort_seq_*-*; do
	echo $f-$gc >> output/$dir/$dir.txt
	./$f --n=$gc >> output/$dir/$f.txt
    done;
    # parallel tp
    pc=$3
    until [ $pc -gt $4 ]; do
	for f in sort_par_tp_*-*; do
	    echo $f-$gc-$pc >> output/$dir/$dir.txt
	    ./$f --n=$gc --k=$pc >> output/$dir/$f-$pc.txt
	done;
	let pc=pc+1;
    done;
    # parallel mcstl
    pc=$3
    until [ $pc -gt $4 ]; do
	for f in sort_par_mcstl_*-*; do
	    echo $f-$gc-$pc >> output/$dir/$dir.txt
	    ./$f --n=$gc --k=$pc >> output/$dir/$f-$pc.txt
	done;
	let pc=pc+1;
    done;
    # parallel cilk++
    pc=$3
    until [ $pc -gt $4 ]; do
	for f in sort_par_cilk++_*-*; do
	    echo $f-$gc-$pc >> output/$dir/$dir.txt
	    ./$f -cilk_set_worker_count=$pc --n=$gc >> output/$dir/$f-$pc.txt
	done;
	let pc=pc+1;
    done;
    let gc=gc+1;
done;

