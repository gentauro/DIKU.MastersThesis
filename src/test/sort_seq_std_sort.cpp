#include <algorithm>
using std::sort;

#include "sort_seq.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last){
  sort(first,last);
}

#include "sort_seq.cpp"

