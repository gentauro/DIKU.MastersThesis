#include "../cppcsp2/sort.hpp"
using cppcsp2::recursion;

#include "sort_par.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last, const size_t & k){
  recursion(first,last);
}

#include "sort_par.cpp"

