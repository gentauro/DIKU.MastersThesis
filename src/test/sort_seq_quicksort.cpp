#include "../seq/sort.hpp"
using seq::quicksort;

#include "sort_seq.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last){
  quicksort(first,last);
}

#include "sort_seq.cpp"

