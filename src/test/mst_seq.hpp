#ifndef TEST_MST_SEQ_HPP
#define TEST_MST_SEQ_HPP

#include "mst.hpp"

template<typename RandomAccessIterator1,typename RandomAccessIterator2>
class MST : public Process{
  void (*fun) (const Graph<RandomAccessIterator1,RandomAccessIterator2>&,
	       vector<Edge>&);
  Graph<RandomAccessIterator1,RandomAccessIterator2> g;
public:
  MST(void (*function)
      (const Graph<RandomAccessIterator1,RandomAccessIterator2>&,vector<Edge>&),
      const Graph<RandomAccessIterator1,RandomAccessIterator2> & graph)
    : fun(function), g(graph) {}
  void run(){
    vector<Edge> t;
    fun(g,t);
    correctness(g,t);
  }
};

#endif //TEST_MST_SEQ_HPP

