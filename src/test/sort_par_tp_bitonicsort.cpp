#include "../ead/sort.hpp"
using ead::sorter_;

#include "sort_par.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last, const size_t & k){
  sorter_(first,last,k);
}

#include "sort_par.cpp"

