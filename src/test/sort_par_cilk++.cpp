int cilk_main(int argc, char *argv[]){
  const int n(1 << strtoul(arg_parser(argc, argv,"--n=").c_str(),NULL,0));
  int * a = Random<int>::array(n, (n >> 1));

  //cout << "a unsorted" << endl;
  //for(int i(0); i < n; ++i){
  //  cout << a[i] << " ";
  //}
  //cout << endl;

  (Sort<int*>(f,a,a+n)).run();

  //cout << "a sorted" << endl;
  //for(int i(0); i < n; ++i){
  //  cout << a[i] << " ";
  //}
  //cout << endl;

  return EXIT_SUCCESS;
}

