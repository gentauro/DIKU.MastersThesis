#ifndef TEST_SORT_HPP
#define TEST_SORT_HPP

#include "test.hpp"
#include "../utilities/random.hpp"

template<typename RandomAccessIterator>
void correctness(RandomAccessIterator first, RandomAccessIterator last){
  RandomAccessIterator c(first);
  RandomAccessIterator n(first); n++;
  bool t(true);
  while((first != last) && (n != last) && t){ t &= (*c++ <= *n++); }
  cout << t << endl;
}

#endif //TEST_SORT_HPP

