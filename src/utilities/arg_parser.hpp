#ifndef UTILITIES_ARG_PARSER_HPP
#define UTILITIES_ARG_PARSER_HPP

#include <string>
using std::string;

string arg_parser(int argc, char *argv[], string s){
  string v("1");
  for(size_t i(argc); i--;){
    string sa(argv[i]);
    size_t se(sa.find(s));
    if(string::npos != se){
      string tmp(sa.replace(sa.find(s), s.length(),""));
      v = tmp;
    }
  }
  return v;
}

#endif //UTILITIES_ARG_PARSER_HPP

