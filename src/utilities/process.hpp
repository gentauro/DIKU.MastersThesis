#ifndef UTILITIES_PROCESS_HPP
#define UTILITIES_PROCESS_HPP

class Process{
public:
  virtual void run() = 0;
};
#endif //UTILITIES_PROCESS_HPP
