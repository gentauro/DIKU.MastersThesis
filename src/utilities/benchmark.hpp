#ifndef UTILITIES_BENCHMARK_HPP
#define UTILITIES_BENCHMARK_HPP

#include <stdio.h> 
#include <sys/timeb.h>
#include <sys/time.h>
#include <time.h>  

#include "utils.hpp"
#include "process.hpp"

class Benchmark{
  Benchmark(){}
public:
  static string time(Process & p){
    string v;
#if 0 > _POSIX_TIMERS
    struct timeb s,e;
    ftime(&s);
    p.run();
    ftime(&e);
    string tmp(nr_to_str<size_t>((e.time-s.time) * 1000 + 
				 (e.millitm-s.millitm)));
    v.append(xsec_to_sec(tmp,4));
#else
    struct timespec s, e;
    clock_gettime(CLOCK_REALTIME, &s);
    p.run();
    clock_gettime(CLOCK_REALTIME, &e);
    string tmp(nr_to_str<long long>((e.tv_sec - s.tv_sec) * 1000000000LL + 
				    (e.tv_nsec - s.tv_nsec)));
    v.append(xsec_to_sec(tmp,10));
#endif
    return v;
  }
};
#endif //UTILITIES_BENCHMARK_HPP
