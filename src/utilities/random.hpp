#ifndef UTILITIES_RANDOM_HPP
#define UTILITIES_RANDOM_HPP

#include <algorithm>
#include <ctime>
#include <map>
#include <stdlib.h>
#include <vector>
using std::map;
using std::random_shuffle;
using std::vector;

#include "utils.hpp"
#include "graph.hpp"

template <class T1, class T2, class T3> struct triple{
  typedef T1 first_type;
  typedef T2 second_type;
  typedef T3 third_type;

  T1 first;
  T2 second;
  T3 third;
  triple() : first(T1()), second(T2()), second(T3()) {}
  triple(const T1& x, const T2& y, const T3& z) 
    : first(x), second(y), third(z) {}
  template <class U, class V, class W>
  triple (const triple<U,V,W> &t) 
    : first(t.first), second(t.second), triple(t.triple) { }
};
typedef map<string,triple<Vertex*,Vertex*,size_t> > mst;

template<typename T>
class Random{
  Random(){}
public: 
  static T * array(const T & n, const T & s){
    T * a = new T[n];
    srand(time(0));
    for(T i(n); i--;){ a[i] = (rand() % s + 1); }
    return a;
  }
  static Graph<Vertex*,Edge*> graph(const T & n, const T & d){
    if(d > (n - 1)){
      throw "max degree at most n-1 in a undirected connected graph";
    }

    srand(time(0));
    size_t s(n >> 1);
    mst edge_control;

    Vertex * vertices = new Vertex[n];

    //add V vertices
    for(T i(0); i < n; ++i){
      vertices[i] = Vertex(nr_to_str<T>(i));
    }
    //change the order so not always the same MST
    random_shuffle(vertices+0,vertices+n);
  
    // create V-1 edges with weight between 1 and n/2 (MST)
    for(T i(1); i < n; ++i){
      size_t r(rand() % s + 1);
      edge_control.
        insert(make_pair(string(vertices[i-1].id).
                         append(",").append(vertices[i].id),
                         triple<Vertex*,Vertex*,size_t>
                         (&vertices[i-1],&vertices[i],r)));
    }

    // create extra edges limited to max degree
    for(size_t i(0), d_(d-1); i < d_; ++i){
      for(size_t j(0), n_(n-1); j < n_; ++j){
        size_t r(rand() % s + (s + 1));
        size_t u(j);
        while(u == j){ u = (rand() % n); }
      
        string e1(string(vertices[j].id).append(",").append(vertices[u].id));
        string e2(string(vertices[u].id).append(",").append(vertices[j].id));
        mst::iterator it1(edge_control.find(e1));
        mst::iterator it2(edge_control.find(e2));
        if(edge_control.end() == it1 && edge_control.end() == it2){
          edge_control.
            insert(make_pair(string(vertices[j].id).
                             append(",").append(vertices[u].id),
                             triple<Vertex*,Vertex*,size_t>
                             (&vertices[j],&vertices[u],r)));
        }
      }
    }
  
    Edge * edges = new Edge[edge_control.size()];
    size_t edge_counter(0);
    for(mst::iterator it(edge_control.begin()); it != edge_control.end(); ++it){
      edges[edge_counter] = 
        Edge(it->second.first,it->second.second,it->second.third);
      ++edge_counter;
    }
    edge_control.clear();

    Graph<Vertex*,Edge*>
      g(vertices+0,vertices+n,edges+0,edges+edge_counter);
  
    return g;
  }
};
#endif //UTILITIES_RANDOM_HPP

