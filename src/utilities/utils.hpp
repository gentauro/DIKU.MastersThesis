#ifndef UTILITIES_UTILS_HPP
#define UTILITIES_UTILS_HPP

#include <sstream>
#include <string>
using std::string;
using std::stringstream;

template<typename T>
string nr_to_str(const T & nr){
   stringstream ss;
   ss << nr;
   return ss.str();
}

string xsec_to_sec(const string & nr, const size_t & b){
  string v(nr);
  while(b > v.size()){ v.insert(0,"0"); }
  v.insert(v.size() - b + 1,".");
  return v;
}

#endif //UTILITIES_UTILS_HPP
