#ifndef UTILITIES_GRAPH_HPP
#define UTILITIES_GRAPH_HPP

#include <string>
using std::string;

class Vertex{
public:
  string id;
  Vertex() {}
  Vertex(const string & label) : id(label) {}
};

class Edge{
public:
  Vertex * u;
  Vertex * v;
  size_t weight;
  Edge() {}
  Edge(Vertex * v1, Vertex * v2, size_t edge_weight) 
    : u(v1), v(v2), weight(edge_weight) {}
};

template<typename RandomAccessIterator1, typename RandomAccessIterator2>
class Graph{
public:
  const RandomAccessIterator1 vertex_begin;
  const RandomAccessIterator1 vertex_end;
  const RandomAccessIterator2 edge_begin;
  const RandomAccessIterator2 edge_end;
  Graph(RandomAccessIterator1 first_vertex, RandomAccessIterator1 last_vertex, 
	RandomAccessIterator2 first_edge, RandomAccessIterator2 last_edge) 
    : vertex_begin(first_vertex), vertex_end(last_vertex), 
      edge_begin(first_edge), edge_end(last_edge) { }
};

Graph<Vertex*,Edge*> graph_clrs(){
  const size_t n = 9;
  const size_t m = 14;

  Vertex vertices[n] = {
    Vertex("a"), //0
    Vertex("b"), //1
    Vertex("c"), //2
    Vertex("d"), //3
    Vertex("e"), //4
    Vertex("f"), //5
    Vertex("g"), //6
    Vertex("h"), //7
    Vertex("i")  //8
  };
  
  Edge edges[m] = {
    Edge(vertices+0,vertices+1,4),  //(a,b)
    Edge(vertices+0,vertices+7,8),  //(a,h)
    Edge(vertices+1,vertices+2,8),  //(b,c)
    Edge(vertices+1,vertices+7,11), //(b,h)
    Edge(vertices+2,vertices+3,7),  //(c,d)
    Edge(vertices+2,vertices+5,4),  //(c,f)
    Edge(vertices+2,vertices+8,2),  //(c,i)
    Edge(vertices+3,vertices+4,9),  //(d,e)
    Edge(vertices+3,vertices+5,14), //(d,f)
    Edge(vertices+4,vertices+5,10), //(e,f)
    Edge(vertices+5,vertices+6,2),  //(f,g)
    Edge(vertices+6,vertices+8,6),  //(g,i)
    Edge(vertices+6,vertices+7,1),  //(g,h)
    Edge(vertices+7,vertices+8,7)   //(h,i)
  };

  Graph<Vertex*,Edge*> g(vertices+0,vertices+n,edges+0,edges+m);

  return g;
}

#endif //UTILITIES_GRAPH_HPP

