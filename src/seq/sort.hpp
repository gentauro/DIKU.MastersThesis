#ifndef SEQUENTIAL_SORT_HPP
#define SEQUENTIAL_SORT_HPP

#include <iostream> //debug

#include <algorithm>
#include <cmath>
#include <ctime>
#include <functional>
#include <iterator>
#include "stdlib.h" //srand
using std::bind2nd;
using std::binary_function;
using std::ceil;
using std::inplace_merge;
using std::iterator_traits;
using std::less;
using std::log10;
using std::min;
using std::max;
using std::nth_element;
using std::partition;
using std::swap;

namespace seq{
  template<typename BidirectionalIterator, typename Comparator>
  void 
  inplace_merge_(BidirectionalIterator first, BidirectionalIterator middle,
		BidirectionalIterator last, Comparator comp){
    while(first != middle){
      if(!comp(*first,*middle)){
	swap(*first,*middle);
	BidirectionalIterator _middle(middle);
	while(_middle + 1 != last && !comp(*_middle,*(_middle + 1))){
	  swap(*_middle,*(_middle + 1));
	  ++_middle;
	}
      }
      ++first;
    }
  }

  template<typename BidirectionalIterator>
  void 
  inplace_merge_(BidirectionalIterator first, BidirectionalIterator middle,
		BidirectionalIterator last){
    inplace_merge_(first,middle,last,less<typename iterator_traits
		  <BidirectionalIterator>::value_type>());
  }

  template<class InputIterator, class OutputIterator>
  void 
  append(InputIterator first, InputIterator last, OutputIterator result){
    while (first!=last) { *result++ = *first++; }
  }

  template<typename InputIterator1 ,typename InputIterator2, 
	   typename OutputIterator, typename Comparator>
  void 
  merge(InputIterator1 first1, InputIterator1 last1,
	InputIterator2 first2, InputIterator2 last2,
	OutputIterator result, Comparator comp){
    while(true){ 
      *result++ = (comp(*first1,*first2)) ? *first1++ : *first2++;
      if (first1==last1){
	return append(first2,last2,result);
      }
      if (first2==last2){
	return append(first1,last1,result);    
      }
    }
  }

  template<typename InputIterator1 ,typename InputIterator2, 
	   typename OutputIterator>
  void 
  merge(InputIterator1 first1, InputIterator1 last1,
	InputIterator2 first2, InputIterator2 last2,
	OutputIterator result){
    merge(first1,last1,first2,last2,result,less<typename iterator_traits
	  <InputIterator1>::value_type>());
  }

  template<typename RandomAccessIterator, typename Comparator>
  void
  mergesort(RandomAccessIterator first, RandomAccessIterator last,
	    Comparator comp){
    size_t n(last - first);
    if(1 >= n){ return; }
    RandomAccessIterator q(first+(n >> 1));
    mergesort(first,q,comp);
    mergesort(q,last,comp);
    //inplace_merge_(first,q,last,comp); // in O(n^2) ... TO SLOW !!!
    inplace_merge(first,q,last,comp);
  }

  template<typename RandomAccessIterator>
  void
  mergesort(RandomAccessIterator first, RandomAccessIterator last){
    mergesort(first,last,less<typename iterator_traits
	      <RandomAccessIterator>::value_type>());
  }

  template<typename RandomAccessIterator>
  RandomAccessIterator
  pivot(RandomAccessIterator first, RandomAccessIterator last){
    srand(time(0));
    RandomAccessIterator p(first);
    advance(p,rand() % (last - first));
    return p;
  }

  template<typename RandomAccessIterator, typename Comparator>
  RandomAccessIterator
  partition(RandomAccessIterator first, RandomAccessIterator last, 
	    RandomAccessIterator pivot, Comparator comp){
    typename iterator_traits<RandomAccessIterator>::value_type x(*pivot);
    swap(*first,*pivot);
    RandomAccessIterator i(first);
    for(RandomAccessIterator j(first+1); j != last; ++j){
      if(comp(*j,x)){
	++i;
	swap(*i,*j);
      }
    }
    swap(*first,*i);
    return i;
  }

  template<typename RandomAccessIterator, typename Comparator>
  void
  quicksort(RandomAccessIterator first, RandomAccessIterator last,
	    Comparator comp){
    //A)
    if(first == last){ return; }
    --last;
    RandomAccessIterator q = 
      partition(first,last,bind2nd(comp,*last));
    swap(*q,*last);
    quicksort(first,q,comp);
    quicksort(++q,++last,comp);

    //B)
    /*
    if(2 > (last - first)){ return; }
    nth_element(first,first+((last - first) >> 1),last);
    RandomAccessIterator q = first+((last - first) >> 1);
    quicksort(first,q,comp);
    quicksort(q,last,comp);
    */
  }
  
  template<typename RandomAccessIterator>
  void
  quicksort(RandomAccessIterator first, RandomAccessIterator last){
    quicksort(first,last,less<typename iterator_traits
	      <RandomAccessIterator>::value_type>());
  }

  double log2(double n){
    return (1.0/log10(2))*log10(n);
  }

  template<typename T>
  void comparator(T &x, T &y){
    T x_(min(x,y));
    T y_(max(x,y));
    x = x_;
    y = y_;
  }

  template<typename RandomAccessIterator>
  void half_cleaner(RandomAccessIterator first, RandomAccessIterator last, 
		    const size_t & m){
    size_t n(last - first);
    if((m + 1) == n){ 
      comparator(*first,*--last); 
      return; 
    }
    size_t s((n - m) >> 1);
    half_cleaner(first,first+m+s,m);
    half_cleaner(first+s,last,m);
  }

  template<typename RandomAccessIterator>
  void bitonic_sorter(RandomAccessIterator first, RandomAccessIterator last){
    size_t n(last - first);
    half_cleaner(first,last,n >> 1);
    if(2 >= n){ return; }
    bitonic_sorter(first,first+(n >> 1));
    bitonic_sorter(first+(n >> 1),last);
  }

  template<typename RandomAccessIterator>
  void merger_helper(RandomAccessIterator first, RandomAccessIterator last, 
		     const size_t & m){
    if(2 == m){
      comparator(*first,*--last); 
      return; 
    }
    size_t m_(m >> 1);
    merger_helper(first,last,m_);
    merger_helper(first+(m_ >> 1),last-+(m_ >> 1),m_);
  }

  template<typename RandomAccessIterator>
  void merger(RandomAccessIterator first, RandomAccessIterator last){
    size_t n(last - first);
    merger_helper(first,last,n);
    if(2 >= n){ return; }
    bitonic_sorter(first,first+(n >> 1));
    bitonic_sorter(first+(n >> 1),last);
  }

  template<typename RandomAccessIterator>
  void sorter_helper(RandomAccessIterator first, RandomAccessIterator last){
    size_t n(last - first);
    if(2 >= n){ return; }
    sorter(first,first+(n >> 1));
    sorter(first+(n >> 1),last);
  }

  template<typename RandomAccessIterator>
  void sorter(RandomAccessIterator first, RandomAccessIterator last){
    sorter_helper(first,last);
    merger(first,last);
  }

  template<typename RandomAccessIterator>
  struct less_rai 
    : binary_function<RandomAccessIterator,RandomAccessIterator,bool> {
    bool operator()(const RandomAccessIterator x, 
		    const RandomAccessIterator y) const { 
      return (*x <= *y);
    }
  };

  template<typename RandomAccessIterator>
  RandomAccessIterator find_max(RandomAccessIterator first,
				RandomAccessIterator last){
    size_t n(last - first);
    if(1 == n){ return first; }
    return max(find_max<RandomAccessIterator>(first,first+(n>>1)),
	       find_max<RandomAccessIterator>(first+(n>>1),last),
	       less_rai<RandomAccessIterator>());
  }

  template<typename RandomAccessIterator>
  void populate(RandomAccessIterator first1, RandomAccessIterator last1,
		RandomAccessIterator first2, RandomAccessIterator last2,
		const size_t & x){
    size_t n(last1 - first1);
    size_t n_(last2 - first2);
    if(1 == n_){ *first2 = x; if(1 == n){ *first2 = *first1; } return; }
    if((n_ >> 1) <= n){
      populate(first1,first1+(n_ >> 1),first2,first2+(n_ >> 1),x);
      populate(first1+(n_ >> 1),first1+(n_ >> 1)+(n - (n_ >> 1)),
	       first2+(n_ >> 1),last2,x);
    }else{
      populate(first1,last1,first2,first2+(n_ >> 1),x);
      populate(last1,last1,first2+(n_ >> 1),last2,x);
    }
  }

  template<typename RandomAccessIterator>
  void sorter_(RandomAccessIterator first, RandomAccessIterator last){
    const size_t n(last - first);
    const size_t n_((1 << static_cast<size_t>(ceil(log2(n)))));
    if(n == n_){ sorter(first,last); return; }
    int a[n_];
    size_t m(*find_max<RandomAccessIterator>(first,last));
    populate<RandomAccessIterator>(first,last,a,a+n_,m);
    sorter(a,a+n_);
    populate<RandomAccessIterator>(a,a+n_,first,last,m);
  }

}

#endif //SEQUENTIAL_SORT_HPP
