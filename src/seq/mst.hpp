/*******************************************************************************
 * Open Source License
 *
 * Copyleft (l) 2010 Ramon Salvador Soto Mathiesen (http://www.stermon.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyleft notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software shall be used for Good, not Evil.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYLEFT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * License based on Douglas Crockford modified MIT Open Source License:
 * http://www.opensource.org/licenses/mit-license.html
 * 
 ******************************************************************************/
#ifndef SEQUENTIAL_MST_HPP
#define SEQUENTIAL_MST_HPP

#include <algorithm>
#include <functional>
#include <iterator>
#include <limits>
#include <map>
#include <set>
#include <string>
#include <vector>
using std::binary_function;
using std::greater_equal;
using std::iterator_traits;
using std::make_heap;
using std::make_pair;
using std::map;
using std::numeric_limits;
using std::pair;
using std::pop_heap;
using std::set;
using std::sort;
using std::string;
using std::swap;
using std::vector;

#include "../utilities/graph.hpp"

namespace sequential{
  struct less_edge : binary_function <Edge,Edge,bool> {
    bool operator() (const Edge & x, const Edge & y) const { 
      return (x.weight < y.weight);
    }
  };
  struct less_equal_edge : binary_function <Edge,Edge,bool> {
    bool operator() (const Edge & x, const Edge & y) const { 
      return (x.weight <= y.weight);
    }
  };

  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  vector<Edge>
  mst_kruskal(const Graph<RandomAccessIterator1,RandomAccessIterator2> & g){
    const size_t v(g.vertex_end - g.vertex_begin);
    vector<Edge> t;
    //1) create a set for each vertex
    vector<set<Vertex*> > s(v);
    {
      size_t counter(0);
      for(RandomAccessIterator1 it(g.vertex_begin); it != g.vertex_end; ++it){
        s.at(counter).insert(it);
        ++counter;
      }
    }
    //2) sort edges nondecreasing by weight
    sort(g.edge_begin,g.edge_end,less_edge());
    //3) for each edge (u,v) if vertex in diferent sets, add edge to a and union
    //   sets
    {
      for(RandomAccessIterator2 it(g.edge_begin); it != g.edge_end; ++it){
        // then check for for u v in set and union and insert
        size_t i1(0),i2(0);
        //we look in all set if we can find "v"
        {
          for(size_t i(0); i < s.size(); ++i){
            if(s[i].find(it->u) != s[i].end()){
              i1 = i;
              break;
            }
          }
        }
        //we look in all set if we can find "u"
        {
          for(size_t i(0); i < s.size(); ++i){
            if(s[i].find(it->v) != s[i].end()){
              i2 = i;
              break;
            }
          }
        }
        //if the sets where we found the vertex are different, add edge and
        //union the sets
        if(i1 != i2){
          t.push_back(*it);
          s[i1].insert(s[i2].begin(),s[i2].end()); s[i2].clear(); 
          swap(s[i2],s[s.size()-1]); s.pop_back();
	  if(1 == s.size()){ break; }
        }
      }
    }
    return t;
  }

  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  void
  mst_kruskal_filter_helper(RandomAccessIterator2 first, 
                            RandomAccessIterator2 last,
                            vector<Edge> & t,
                            vector<set<Vertex*> > & s){
    if(2 > (last - first)){ 
      if(0 < (last - first)){ 
        // then check for for u v in set and union and insert
        size_t i1(0),i2(s.size());
        //we look in all set if we can find "v"
        {
          for(size_t i(0); i < s.size(); ++i){
            if(s[i].find(first->u) != s[i].end()){
              i1 = i;
              break;
            }
          }
        }
        //we look in all set if we can find "u"
        {
          for(size_t i(0); i < s.size(); ++i){
            if(s[i].find(first->v) != s[i].end()){
              i2 = i;
              break;
            }
          }
        }
        //if the sets where we found the vertex are different, add edge and
        //union the sets
        if(i1 != i2){
          t.push_back(*first);
          s[i1].insert(s[i2].begin(),s[i2].end()); s[i2].clear(); 
          swap(s[i2],s[s.size()-1]); s.pop_back();
        }
      }
      return;
    }
    // 1) partition
    --last;
    RandomAccessIterator2 q = 
      partition(first,last,bind2nd(less_equal_edge(),*last));
    swap(*q,*last);
    // 2) call recursively
    mst_kruskal_filter_helper<RandomAccessIterator2>(first,q,t,s);
    // 3) for each edge in second vector, check if in u and v in same set. if
    // yes swap with last and exclude
    --q;
    for(RandomAccessIterator2 it(last); it != q; --it){
      for(size_t i(0); i < s.size(); ++i){
        if((s[i].end() != s[i].find(it->u)) &&
           (s[i].end() != s[i].find(it->v))){
          swap(*it,*last--);
          break;
        }
      }
    }
    // 4) call function on second part.
    mst_kruskal_filter_helper<RandomAccessIterator2>(++q,++last,t,s);
  }  
  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  vector<Edge>
  mst_kruskal_filter(const Graph
		     <RandomAccessIterator1,RandomAccessIterator2> & g){
    const size_t v(g.vertex_end - g.vertex_begin);
    vector<Edge> t;
    t.reserve(v-1);
    vector<set<Vertex*> > s(v);
    {
      size_t counter(0);
      for(RandomAccessIterator1 it(g.vertex_begin); it != g.vertex_end; ++it){
        s.at(counter).insert(it);
        ++counter;
      }
    }
    mst_kruskal_filter_helper 
      <RandomAccessIterator2>(g.edge_begin,g.edge_end,t,s);
    return t;
  }  

  class Vertex_ : public Vertex{
  public:
    size_t key;
    size_t adj;
    Vertex_ * pi;
    bool in_heap;
    Vertex_(const string & label, size_t adj_index) 
      : Vertex(label), key(numeric_limits<size_t>::max()), adj(adj_index), pi(0), 
        in_heap(true) {}
  };

  struct greater_vertex : binary_function <Vertex_*,Vertex_*,bool> {
    bool operator() (const Vertex_ * x, const Vertex_ * y) const { 
      return (x->key > y->key);
    }
  };

  template<typename RandomAccessIterator, typename Compare>
  void crease_heap(RandomAccessIterator first, RandomAccessIterator index, 
                   Compare comp){
    size_t i(index - first);
    while(i > 0){
      size_t pi((i-1) >> 1);
      if(comp(*(first+i),*(first+pi))){ return; }
      swap(*(first+i),*(first+pi));
      i = pi;
    }
  }
  template<typename RandomAccessIterator, typename Compare>
  void increase_heap(RandomAccessIterator first, RandomAccessIterator index, 
                     Compare comp){
    crease_heap(first,index,comp);
  }
  template<typename RandomAccessIterator, typename Compare>
  void decrease_heap(RandomAccessIterator first, RandomAccessIterator index, 
                     Compare comp){
    crease_heap(first,index,comp);
  }

  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  vector<Edge>
  mst_prim(const Graph<RandomAccessIterator1,RandomAccessIterator2> & g){
    const size_t v(g.vertex_end - g.vertex_begin);
    vector<Edge> t;
    vector<Vertex_ *> t_;
    vector<Vertex_ *> q;
    vector<vector<pair<Vertex_*,size_t> > > a(v);
    // 1) create copy of V
    {
      size_t i(0);
      for(RandomAccessIterator1 it(g.vertex_begin); it != g.vertex_end; ++it){
        q.push_back(new Vertex_(it->id,i));
        ++i;
      }
    }
    // 2) add edges to adjency list
    {
      for(RandomAccessIterator2 it(g.edge_begin); it != g.edge_end; ++it){
        size_t ui(it->u - g.vertex_begin);
        size_t vi(it->v - g.vertex_begin);
        a.at(ui).push_back(make_pair(q.at(vi),it->weight));
        a.at(vi).push_back(make_pair(q.at(ui),it->weight));
      }
    }
    // 3) set r 0
    q.at(0)->key = 0;
    // 4) create heap
    make_heap(q.begin(),q.end(),greater_vertex());
    // 5) iterate heap
    while(0 < q.size()){
      pop_heap(q.begin(),q.end(),greater_vertex());
      Vertex_ * u_(q.back()); u_->in_heap = false; q.pop_back(); 
      for(size_t i(0), n(a.at(u_->adj).size()); i < n; ++i){
        pair<Vertex_*,size_t> e_(a.at(u_->adj).at(i));
        Vertex_ * v_(e_.first);
        size_t w_(e_.second);
        if(v_->in_heap && w_ < v_->key){
          v_->pi = u_;
          v_->key = w_;
          {
	    //find v_ in heap and decrease
            size_t i(0);
            for(size_t n(q.size()); i < n; ++i){
              if(!(v_->id.compare(q.at(i)->id))){ break; }
            }
            decrease_heap(q.begin(),q.begin()+i,greater_vertex());
          }
        }
      }
      //add vertex to t_ unless it's r
      if(q.size() < v-1){ t_.push_back(u_); }
    }
    //iterate through t_ and add edge to t
    for(size_t i(0); i < t_.size(); ++i){
      t.push_back(Edge(t_.at(i)->pi, t_.at(i), t_.at(i)->key));
    }
    return t;
  }


  class Supervertex : public Vertex{
  public:
    vector<Edge> t;
    set<Edge,less_equal_edge> adj_list;
    set<Vertex*> vertices;
    Supervertex(const string & label) : Vertex(label) {}
  };

  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  vector<Edge> 
  mst_boruvka(const Graph<RandomAccessIterator1,RandomAccessIterator2> & g){
    vector<Edge> t;
    set<Supervertex*> g_;
    map<Vertex*,Supervertex*> adj_map;

    // 1) convert each vertex to a supervertex and add to hash
    for(RandomAccessIterator1 it(g.vertex_begin); it != g.vertex_end; ++it){
      adj_map.insert(make_pair(it,new Supervertex(it->id)));
      g_.insert(adj_map[it]);
      adj_map[it]->vertices.insert(it);
    }

    // 2) add edges to adjency list for each supervertex. The min weigthed will
    //   be the first in the set.
    for(RandomAccessIterator2 it(g.edge_begin); it != g.edge_end; ++it){
      adj_map[it->u]->adj_list.insert(*it);
      adj_map[it->v]->adj_list.insert(*it);
    }

    // 3) for each supervertex add the minium edge. union vector<Edge> t and
    // set<Edge> adj_list. Then remove joining edge from adj_list and place in
    // t. copy all vertices from B to A and update adj_map. Move B supervertex
    // to end of vector and pop_back.
    while(1 < g_.size()){
      for(set<Supervertex*>::iterator it(g_.begin()); it != g_.end(); ++it){
	//if u and v are not in the same supervertex
	if(adj_map[(*it)->adj_list.begin()->u] != 
	   adj_map[(*it)->adj_list.begin()->v]){
	  Supervertex * s1 = adj_map[(*it)->adj_list.begin()->u];
	  Supervertex * s2 = adj_map[(*it)->adj_list.begin()->v];
	  // add lowest edge joining supervertex and all of s2 to T
	  s1->t.push_back((*(*it)->adj_list.begin()));
	  s1->t.insert(s1->t.end(),s2->t.begin(),s2->t.end());
	  // update vertices from s2 to s1
	  for(set<Vertex*>::iterator it_(s2->vertices.begin());
	      it_ != s2->vertices.end(); ++it_){
	    adj_map[*it_] = s1;
	    s1->vertices.insert(*it_);
	  }
	  // add edges from s2 to s1
	  s1->adj_list.insert(s2->adj_list.begin(), s2->adj_list.end());
	  // contract the supervertex
	  for(set<Edge,less_equal_edge>::iterator it_(s1->adj_list.begin());
	      it_ != s1->adj_list.end(); ++it_){
	    if(adj_map[it_->u] == adj_map[it_->v]){
	      s1->adj_list.erase(it_);
	    }
	  }
	  // remove s2
	  g_.erase(s2);
	}
      }
    }

    t = (*(g_.begin()))->t;
    
    return t;
  }
  
}

#endif //SEQUENTIAL_MST_HPP
