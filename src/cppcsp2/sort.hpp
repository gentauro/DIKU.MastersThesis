#ifndef CPPCSP2_SORT_HPP
#define CPPCSP2_SORT_HPP

#include <iterator>
using std::advance;
using std::distance;

#include <cppcsp/cppcsp.h>
using csp::Start_CPPCSP;
using csp::End_CPPCSP;
using csp::CSProcess;
using csp::Chanin;
using csp::Chanout;
using csp::InParallel;
using csp::InParallelOneThread;
using csp::Run;
using csp::RunInThisThread;

//#define stack 1048576 // 1024K
//#define stack 65536   //   64K
#define stack 4096      //    4K

namespace cppcsp2{
  template<typename RandomAccessIterator>
  class RecursionHelper : public CSProcess{
  private:
    RandomAccessIterator first;
    RandomAccessIterator last;
  protected:
    void run(){
      size_t n(distance(first,last));
      if(1 < (n >> 1)){
	RandomAccessIterator m(first); //replace with pivot/partition
	advance(m,(n >> 1));
	Run( InParallel
	//RunInThisThread( InParallelOneThread
	     ( new RecursionHelper<RandomAccessIterator>(first,m) )
	     ( new RecursionHelper<RandomAccessIterator>(m,last) )
	     );
      }
    }
  public:
    RecursionHelper(RandomAccessIterator _first, RandomAccessIterator _last) 
      : CSProcess(stack), first(_first), last(_last) {}
  };
  
  template<typename RandomAccessIterator>
  class Recursion : public CSProcess{
  private:
    RandomAccessIterator first;
    RandomAccessIterator last;
  protected:
    void run(){
      Run( new RecursionHelper<RandomAccessIterator>(first,last) );
    }
  public:
    Recursion(RandomAccessIterator _first, RandomAccessIterator _last) 
      : CSProcess(stack), first(_first), last(_last) {}
  };
  
  template<typename RandomAccessIterator>
  void
  recursion(RandomAccessIterator first, RandomAccessIterator last){
    Start_CPPCSP();
    Run( new Recursion<RandomAccessIterator> (first,last) );
    End_CPPCSP();
  }
}

#endif //CPPCSP2_SORT_HPP

