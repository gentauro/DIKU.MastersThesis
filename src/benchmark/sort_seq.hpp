#ifndef TEST_SORT_SEQ_HPP
#define TEST_SORT_SEQ_HPP

#include "sort.hpp"

template<typename RandomAccessIterator>
class Sort : public Process{
  void (*fun)(RandomAccessIterator f, RandomAccessIterator l);
  RandomAccessIterator f,l;
public:
  Sort(void (*function)(RandomAccessIterator first, RandomAccessIterator last),
       RandomAccessIterator first, RandomAccessIterator last) 
    : fun(function), f(first), l(last) {}
  void run(){
    fun(f,l);
  }
};

#endif //TEST_SORT_SEQ_HPP

