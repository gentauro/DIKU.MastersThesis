//stdc++
#include <parallel/algorithm>
#include <parallel/settings.h>
//openmp
#include <omp.h>
using std::__parallel::sort;
using __gnu_parallel::force_parallel;
using __gnu_parallel::multiway_mergesort_exact_tag;

typedef __gnu_parallel::_Settings settings;

#include "sort_par.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last, const size_t & k){
  //openmp
  omp_set_dynamic(false);
  omp_set_num_threads(k);
  //stdc++
  settings s;
  s.algorithm_strategy = force_parallel;
  settings::set(s);
  sort(first,last,multiway_mergesort_exact_tag(k));
}

#include "sort_par.cpp"

