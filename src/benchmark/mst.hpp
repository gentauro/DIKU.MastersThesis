#ifndef TEST_MST_HPP
#define TEST_MST_HPP

#include <iterator>
using std::iterator;

#include "test.hpp"
#include "../utilities/random.hpp"

template<typename RandomAccessIterator1,typename RandomAccessIterator2>
void correctness(const Graph<RandomAccessIterator1,RandomAccessIterator2> & g,
		 const vector<Edge> & mst){
  /*
  cout << "vertices:" << endl;
  for(Vertex * i(g.vertex_begin); i != g.vertex_end; ++i){
    cout << i->id << endl;
  }
  cout << endl;

  cout << "edges:" << endl;
  for(Edge * i(g.edge_begin); i != g.edge_end; ++i){
    cout << "(" << i->u->id
	 << "," << i->v->id
	 << ") - " << i->weight << endl;
  }
  cout << endl;
  */

  bool t(true);
  for(vector<Edge>::const_iterator eit(mst.begin()); eit != mst.end(); ++eit){
    for(RandomAccessIterator1 vit(g.vertex_begin); vit != g.vertex_end; ++vit){
      if(0 == vit->id.compare(eit->u->id)){
	++vit;
	t &= (0 == vit->id.compare(eit->v->id));
	break;
      }
    }
  }
  cout << t << endl;

  /*
  cout << "mst:" << endl;
  for(size_t i(0); i < t.size(); ++i){
    cout << "(" << t.at(i).u->id
	 << "," << t.at(i).v->id
	 << ") - " << t.at(i).weight << endl;
  }
  cout << t.size() << endl;
  */
}

#endif //TEST_MST_HPP

