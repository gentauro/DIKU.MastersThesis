#include "../mcstl/mst.hpp"
using mcstl::mst_kruskal;

#include "mst_par.hpp"

void f(const Graph<Vertex*,Edge*> & g, vector<Edge> & t, const size_t & k){
  t = mst_kruskal<Vertex*,Edge*>(g,k);
}

#include "mst_par.cpp"

