int main(int argc, char *argv[]){
  const int n(1 << strtoul(arg_parser(argc, argv,"--n=").c_str(),NULL,0));
  const int k(strtoul(arg_parser(argc, argv,"--k=").c_str(),NULL,0));
  int * a = Random<int>::array(n, (n >> 1));
  Sort<int*> s(Sort<int*>(f,a,a+n,k));
  cout << Benchmark::time(s) << endl;
  return EXIT_SUCCESS;
}

