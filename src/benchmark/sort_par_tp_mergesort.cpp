#include "../ead/sort.hpp"
using ead::mergesort;

#include "sort_par.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last, const size_t & k){
  mergesort(first,last,k);
}

#include "sort_par.cpp"

