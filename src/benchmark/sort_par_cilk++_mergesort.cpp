#include "../cilk++/sort.hpp"
using cilkpp::mergesort;

#include "sort_seq.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last){
  mergesort(first,last);
}

#include "sort_par_cilk++.cpp"

