#include "../seq/mst.hpp"
using sequential::mst_kruskal_filter;

#include "mst_seq.hpp"

void f(const Graph<Vertex*,Edge*> & g, vector<Edge> & t){
  t = mst_kruskal_filter<Vertex*,Edge*>(g);
}

#include "mst_seq.cpp"

