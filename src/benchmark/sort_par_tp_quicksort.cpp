#include "../ead/sort.hpp"
using ead::quicksort;

#include "sort_par.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last, const size_t & k){
  quicksort(first,last,k);
}

#include "sort_par.cpp"

