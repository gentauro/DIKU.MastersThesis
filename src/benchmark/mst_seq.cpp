int main(int argc, char *argv[]){
  const int n(1 << strtoul(arg_parser(argc, argv,"--n=").c_str(),NULL,0));
  Graph<Vertex*,Edge*> g(Random<int>::graph(n,n >> 1));
  MST<Vertex*,Edge*> m(MST<Vertex*,Edge*>(f,g));
  cout << Benchmark::time(m) << endl;
  return EXIT_SUCCESS;
}

