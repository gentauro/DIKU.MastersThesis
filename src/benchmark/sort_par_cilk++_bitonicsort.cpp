#include "../cilk++/sort.hpp"
using cilkpp::sorter_;

#include "sort_seq.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last){
  sorter_(first,last);
}

#include "sort_par_cilk++.cpp"

