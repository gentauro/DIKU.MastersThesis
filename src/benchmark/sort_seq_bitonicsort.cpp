#include "../seq/sort.hpp"
using seq::sorter_;

#include "sort_seq.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last){
  sorter_(first,last);
}

#include "sort_seq.cpp"

