#include "../seq/mst.hpp"
using sequential::mst_prim;

#include "mst_seq.hpp"

void f(const Graph<Vertex*,Edge*> & g, vector<Edge> & t){
  t = mst_prim<Vertex*,Edge*>(g);
}

#include "mst_seq.cpp"

