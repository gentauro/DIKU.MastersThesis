#ifndef BENCHMARK_BENCHMARK_HPP
#define BENCHMARK_BENCHMARK_HPP

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using std::cout;
using std::endl;

#include "../utilities/benchmark.hpp"
#include "../utilities/arg_parser.hpp"

#endif //BENCHMARK_BENCHMARK_HPP

