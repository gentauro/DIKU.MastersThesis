#include "../seq/sort.hpp"
using seq::mergesort;

#include "sort_seq.hpp"

template<typename RandomAccessIterator>
void f(RandomAccessIterator first, RandomAccessIterator last){
  mergesort(first,last);
}

#include "sort_seq.cpp"

