#ifndef POSIX_PROCESS_HPP
#define POSIX_PROCESS_HPP

//maybe use the threadpool wrapper !!!

namespace posix{
  class Thread{
    pthread_t _t;
    pthread_attr_t _a;
    static void * 
    routine(void * p){
      Thread * t = static_cast<Thread*>(p);
      t->run();
      pthread_exit(0);
    }
  public:
    void 
    create(bool join){ 
      pthread_attr_init(&_a);
      pthread_attr_setdetachstate(&_a,(join && PTHREAD_CREATE_JOINABLE));
      //set scope ...
      pthread_create(&_t, 0, routine, this); }
    virtual void run() { throw "you must implement the run method"; }
    void join(){ pthread_join(_t,0); }
  };

  class Process{
  public:
    virtual void run(){ throw "you must implement the run method"; }
  };
}

#endif //PROCESS_HPP
