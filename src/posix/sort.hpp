#ifndef POSIX_SORT_HPP
#define POSIX_SORT_HPP

//in posix as in csp add just one example that they don't work as expected:
//http://www2.net.in.tum.de/~gregor/docs/pthread-scheduling.html <--- look on the scope

//#include sequential algorithm for pivot/partition
#include "process.hpp"

namespace posix{
  template<typename RandomAccessIterator, typename Comparator>
  class T : public Thread{
    RandomAccessIterator _first;
    RandomAccessIterator _last;
    Comparator _comp;
  public:
    T(RandomAccessIterator first, RandomAccessIterator last, Comparator comp) :
      _first(first), _last(last), _comp(comp) {}
    void 
    run(){ e_quicksort_helper(_first, _last, _comp); }
  public:
    static void 
    e_quicksort_helper(RandomAccessIterator first, 
		       RandomAccessIterator last,
		       Comparator comp){
      if(first != last){
	RandomAccessIterator p(pivot(first,last));
	RandomAccessIterator q(partition(first,last,p,comp));
	T * t = new T(first,q,comp); t->create(true);
	e_quicksort_helper(q+1,last,comp);
	t->join();
      }
    }
  };
  
  template<typename RandomAccessIterator, typename Comparator>
  class P : public Process{
    RandomAccessIterator _first;
    RandomAccessIterator _last;
    Comparator _comp;
  public:
    P(RandomAccessIterator first, RandomAccessIterator last, Comparator comp) :
      _first(first), _last(last), _comp(comp) {}
    void 
    run(){ 
      T<RandomAccessIterator,
	Comparator>::e_quicksort_helper(_first, _last, _comp); 
    }
  };

  template<typename RandomAccessIterator, typename Comparator>
  void
  e_quicksort(RandomAccessIterator first, RandomAccessIterator last,
	    Comparator comp){
    (new P<RandomAccessIterator,Comparator>(first,last,comp))->run();
  }

  template<typename RandomAccessIterator>
  void
  e_quicksort(RandomAccessIterator first, RandomAccessIterator last){
    e_quicksort(first,last,less_equal<typename 
	      iterator_traits<RandomAccessIterator>::value_type>());
  }
}

#endif //POSIX_SORT_HPP

