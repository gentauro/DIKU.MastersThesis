#ifndef CILKPP_SORT_HPP
#define CILKPP_SORT_HPP

#include <algorithm>
#include <cmath>
#include <functional>
#include <iterator>
using std::binary_function;
using std::bind2nd;
using std::ceil;
using std::inplace_merge;
using std::iterator_traits;
using std::less;
using std::log10;
using std::max;
using std::min;
using std::partition;
using std::swap;

#include <cilk.h>

namespace cilkpp{
  template<typename RandomAccessIterator, typename Comparator>
  void mergesort(RandomAccessIterator first, RandomAccessIterator last,
		 Comparator comp){
    size_t n(last - first);
    if(1 >= n){ return; }
    RandomAccessIterator q(first+(n >> 1));
    cilk_spawn mergesort(first,q,comp);
    mergesort(q,last,comp);
    cilk_sync;
    inplace_merge(first,q,last,comp);
  }

  template<typename RandomAccessIterator>
  void mergesort(RandomAccessIterator first, RandomAccessIterator last){
    mergesort(first,last,less<typename iterator_traits
	      <RandomAccessIterator>::value_type>());
  }

  template<typename RandomAccessIterator, typename Comparator>
  void quicksort(RandomAccessIterator first, RandomAccessIterator last,
		 Comparator comp){
    if(first == last){ return; }
    --last;
    RandomAccessIterator middle = 
      partition(first, last, bind2nd(comp,*last));
    swap(*last, *middle);
    cilk_spawn quicksort(first, middle,comp); 
    quicksort(++middle,++last,comp);
    cilk_sync;
  }

  template<typename RandomAccessIterator>
  void quicksort(RandomAccessIterator first, RandomAccessIterator last){
    mergesort(first,last,less<typename iterator_traits
	      <RandomAccessIterator>::value_type>());    
  }

  double log2(double n){
    return (1.0/log10(2))*log10(n);
  }

  template<typename T>
  void comparator(T &x, T &y){
    T x_(min(x,y));
    T y_(max(x,y));
    x = x_;
    y = y_;
  }

  template<typename RandomAccessIterator>
  void half_cleaner(RandomAccessIterator first, RandomAccessIterator last, 
		    const size_t & m){
    size_t n(last - first);
    if((m + 1) == n){ 
      comparator(*first,*--last); 
      return; 
    }
    size_t s((n - m) >> 1);
    cilk_spawn half_cleaner(first,first+m+s,m);
    half_cleaner(first+s,last,m);
    cilk_sync;
  }

  template<typename RandomAccessIterator>
  void bitonic_sorter(RandomAccessIterator first, RandomAccessIterator last){
    size_t n(last - first);
    half_cleaner(first,last,n >> 1);
    if(2 >= n){ return; }
    cilk_spawn bitonic_sorter(first,first+(n >> 1));
    bitonic_sorter(first+(n >> 1),last);
    cilk_sync;
  }

  template<typename RandomAccessIterator>
  void merger_helper(RandomAccessIterator first, RandomAccessIterator last, 
		     const size_t & m){
    if(2 == m){
      comparator(*first,*--last); 
      return; 
    }
    size_t m_(m >> 1);
    cilk_spawn merger_helper(first,last,m_);
    merger_helper(first+(m_ >> 1),last-+(m_ >> 1),m_);
    cilk_sync;
  }

  template<typename RandomAccessIterator>
  void merger(RandomAccessIterator first, RandomAccessIterator last){
    size_t n(last - first);
    merger_helper(first,last,n);
    if(2 >= n){ return; }
    cilk_spawn bitonic_sorter(first,first+(n >> 1));
    bitonic_sorter(first+(n >> 1),last);
    cilk_sync;
  }

  template<typename RandomAccessIterator>
  void sorter_helper(RandomAccessIterator first, RandomAccessIterator last){
    size_t n(last - first);
    if(2 >= n){ return; }
    cilk_spawn sorter(first,first+(n >> 1));
    sorter(first+(n >> 1),last);
    cilk_sync;
  }

  template<typename RandomAccessIterator>
  void sorter(RandomAccessIterator first, RandomAccessIterator last){
    sorter_helper(first,last);
    merger(first,last);
  }

  template<typename RandomAccessIterator>
  void find_max(RandomAccessIterator first,
		RandomAccessIterator last,
		size_t & nr){
    size_t n(last - first);
    if(1 == n){ nr = *first; return; }
    size_t f1, f2;
    cilk_spawn find_max<RandomAccessIterator>(first,first+(n>>1),f1);
    find_max<RandomAccessIterator>(first+(n>>1),last,f2);
    cilk_sync;
    nr = max(f1,f2);
  }

  template<typename RandomAccessIterator>
  void populate(RandomAccessIterator first1, RandomAccessIterator last1,
		RandomAccessIterator first2, RandomAccessIterator last2,
		const size_t & x){
    size_t n(last1 - first1);
    size_t n_(last2 - first2);
    if(1 == n_){ *first2 = x; if(1 == n){ *first2 = *first1; } return; }
    if((n_ >> 1) <= n){
      cilk_spawn populate(first1,first1+(n_ >> 1),first2,first2+(n_ >> 1),x);
      populate(first1+(n_ >> 1),first1+(n_ >> 1)+(n - (n_ >> 1)),
	       first2+(n_ >> 1),last2,x);
    }else{
      cilk_spawn populate(first1,last1,first2,first2+(n_ >> 1),x);
      populate(last1,last1,first2+(n_ >> 1),last2,x);
    }
    cilk_sync;
  }

  template<typename RandomAccessIterator>
  void sorter_(RandomAccessIterator first, RandomAccessIterator last){
    const size_t n(last - first);
    const size_t n_((1 << static_cast<size_t>(ceil(log2(n)))));
    if(n == n_){ sorter(first,last); return; }
    int a[n_];
    size_t m; find_max<RandomAccessIterator>(first,last,m);
    populate<RandomAccessIterator>(first,last,a,a+n_,m);
    sorter(a,a+n_);
    populate<RandomAccessIterator>(a,a+n_,first,last,m);
  }

}

#endif //CILKPP_SORT_HPP
