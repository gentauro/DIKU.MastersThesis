/*******************************************************************************
 * Open Source License
 *
 * Copyleft (l) 2010 Ramon Salvador Soto Mathiesen (http://www.stermon.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyleft notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software shall be used for Good, not Evil.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYLEFT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * License based on Douglas Crockford modified MIT Open Source License:
 * http://www.opensource.org/licenses/mit-license.html
 * 
 ******************************************************************************/
#ifndef EAD_HPP
#define EAD_HPP

#include <algorithm>
#include <iterator>
#include <functional>
#include <map>
#include <pthread.h>
#include <vector>  
using std::binary_function;
using std::iterator;
using std::map;
using std::pop_heap;
using std::push_heap;
using std::vector;

#include <tr1/memory>
using std::tr1::shared_ptr;

/*******************************************************************************
 * Headers
 ******************************************************************************/
class Mutex{
protected:
  pthread_mutex_t m;
  pthread_mutexattr_t ma;
public:  
  Mutex(){
    pthread_mutexattr_init(&ma);
    pthread_mutex_init(&m,&ma);
  }
  ~Mutex(){
    pthread_mutex_destroy(&m);
    pthread_mutexattr_destroy(&ma);
  }
public:
  int lock() { return pthread_mutex_lock(&m); }
  int unlock() { return pthread_mutex_unlock(&m); }
  bool is_locked() {
    if (0 != pthread_mutex_trylock(&m)){ return true; }
    else { pthread_mutex_unlock(&m); return false; }
  }
};

class Condition : public Mutex{
protected:
  pthread_cond_t c;
public:  
  Condition(){ pthread_cond_init(&c,0); }
  ~Condition(){ pthread_cond_destroy(&c); }
public:
  void wait(){ pthread_cond_wait(&c,&m); }
  void signal(){ pthread_cond_signal(&c); }
  void broadcast() { pthread_cond_broadcast(&c); }
};

class Thread{
protected:
  pthread_t t;
  pthread_attr_t ta;
  bool r;            //running
public:
  Thread () : r(false) { }
  virtual ~Thread(){ if(r){ cancel(); } }
public:  
  virtual void run() = 0;
  void create(const bool detached = false, const bool scope = false);
  void detach();
  void join();
  void cancel();
  void reset();
protected:
  void exit();
  void sleep(const double seconds = 0.001);
};

class Job{
protected:
  size_t p;       // priority
  bool s;         // status
  Condition cond; // condition for the job
public:
  Job(size_t priority = 0) : p(priority), s(false) {}
  virtual ~Job(){ if(cond.is_locked()){ return; } }
public:
  virtual void run() = 0;
  int lock(){ return cond.lock(); }
  int unlock(){ return cond.unlock(); }
public:
  const size_t priority(){ return p; }
  void set_priority(const size_t & n){ p = n; }
  bool status(){ return s; }
  void done(){ s = true; }
};
typedef shared_ptr<Job> spj;

class ThreadPool{
  class ThreadPoolThread : public Thread{
  protected:
    ThreadPool * tp;
  public:
    ThreadPoolThread(ThreadPool * thread_pool) : Thread(), tp(thread_pool) { }
  public:
    void run();
    void quit();
  };
protected:
  size_t k;                      // (k)ores/processors (threads)
  vector<ThreadPoolThread> tpts; // threadpool threads
  vector<spj> jl;                // job list, default array (LIFO)
  bool h;                        // use min heap instead of array
  size_t b;                      // buffer size of job list (k+b)
  size_t its;                    // idle threads
  Condition cond_its;            // condition for idle threads
  Condition cond_jl;             // condition for job list
public:
  ThreadPool(size_t nr_threads = 1, bool use_heap = false, size_t job_buffer = 0);
  ~ThreadPool();
public:
  void add(spj & job);
  void sync(spj & job);
  void sync_all();
  const bool any_sleeping();
  const bool buffer_full();
  template<typename T, typename F, typename P>
  spj add_optimized_args_one(F function, P param);
  template<typename T, typename F, typename P1, typename P2>
  spj add_optimized_args_two(F function, P1 param_1, P2 param_2);
  template<typename T, typename F, typename P1, typename P2, typename P3>
  spj add_optimized_args_three(F function, P1 param_1, P2 param_2, P3 param_3);
#ifdef __GXX_EXPERIMENTAL_CXX0X__
  template<typename T, typename F, typename... PS>
  spj add_optimized(F function, PS... params);
#endif
};
/******************************************************************************/

/*******************************************************************************
 * Utilities
 ******************************************************************************/
struct less_equal_job : binary_function <spj,spj,bool> {
  bool operator() (spj j1, spj j2) const { 
    return (j1->priority() <= j2->priority());
  }
};
struct greater_equal_job : binary_function <spj,spj,bool> {
  bool operator() (spj j1, spj j2) const { 
    return (j1->priority() >= j2->priority());
  }
};
/******************************************************************************/

/*******************************************************************************
 * Thread
 ******************************************************************************/
void * start_routine(void * t){
  (static_cast<Thread*>(t))->run();
  (static_cast<Thread*>(t))->reset();
  return 0;
}
void Thread::create(const bool detached, const bool scope){
  if(!r){
    int s;
    if(0 != (s = pthread_attr_init(&ta))){
      //error, do something with s
      return;
    }
    if(detached){
      if(0 != (s = pthread_attr_setdetachstate(&ta,PTHREAD_CREATE_DETACHED))){
	//error, do something with s
	return;
      }
    }
    if(scope){
      if(0 != (s = pthread_attr_setscope(&ta, PTHREAD_SCOPE_SYSTEM))){
	//error, do something with s
	return;
      }
    }
    if(0 != (s = pthread_create(&t,&ta,start_routine,this))){
      //error, do something with s
      return;
    }else{
      r = true;
    }
  }
}
void Thread::detach(){ 
  if(!r){ int s; if(0 != (s = pthread_detach(t))){ return; } } 
}
void Thread::join(){ 
  if(!r){ int s; if(0 != (s = pthread_join(t,0))){ return; } } 
}
void Thread::cancel(){ 
  if(!r){ int s; if(0 != (s = pthread_cancel(t))){ return; } } 
}
void Thread::reset(){ r = false; }
void Thread::exit(){ 
  if(r && (pthread_self() == t)){ r = false; pthread_exit(0); } }
void Thread::sleep(const double seconds){ if(r){ sleep(seconds); } }
/******************************************************************************/

/*******************************************************************************
 * ThreadPool
 ******************************************************************************/
ThreadPool::ThreadPool(size_t threads, bool heap, size_t buffer) 
  : k(threads), h(heap),b(threads + buffer), its(0){
  tpts.reserve(k);
  jl.reserve(b);
  for(size_t i(0); i < k; ++i){
    tpts.push_back(ThreadPoolThread(this));
  }
  for(vector<ThreadPoolThread>::iterator ib(tpts.begin()), ie(tpts.end()); 
      ib != ie; ++ib){
    ib->create(true,true);
  }
}
ThreadPool::~ThreadPool(){
  sync_all();
  for(vector<ThreadPoolThread>::iterator ib(tpts.begin()), ie(tpts.end()); 
      ib != ie; ++ib){
    ib->quit();
  }
  tpts.clear();
}
void ThreadPool::add(spj & j){
  if(b > jl.size()){ //fill first the buffer
    cond_jl.lock();
    jl.push_back(j);
    if(h){ push_heap(jl.begin(),jl.end(),greater_equal_job()); }
    cond_jl.broadcast();
    cond_jl.unlock();
  }else{
    j->run();
  }
}
void ThreadPool::sync(spj & j){
  if(0 == j){ return; }
  j->lock();
  if(!j->status()){ j->run(); }
  j->unlock();
}
void ThreadPool::sync_all(){
  while(true){ 
    cond_its.lock();
    if(k > its){
      cond_its.wait();
    }else{
      if(0 < jl.size()){
	cond_its.unlock();
	cond_jl.wait();
	cond_its.lock();
      }else{
	cond_jl.unlock();
	cond_its.unlock();
	break;
      }
      cond_jl.unlock();
    }
    cond_its.unlock();
  }
}
const bool ThreadPool::any_sleeping(){
  return ((1 < k) && (0 < its));
}
const bool ThreadPool::buffer_full(){
  return (b == jl.size());
}
template<typename T, typename F, typename P>
spj ThreadPool::add_optimized_args_one(F function, P param){
  spj j;
  if(any_sleeping()){ spj j_(new T(param)); j = j_; add(j); }
  else{ function(param); }
  return j;
}
template<typename T, typename F, typename P1, typename P2>
spj ThreadPool::add_optimized_args_two(F function, P1 param_1, P2 param_2){
  spj j;
  if(any_sleeping()){ spj j_(new T(param_1,param_2)); j = j_; add(j); }
  else{ function(param_1,param_2); }
  return j;
}
template<typename T, typename F, typename P1, typename P2, typename P3>
spj ThreadPool::add_optimized_args_three(F function, P1 param_1, P2 param_2, 
					 P3 param_3){
  spj j;
  if(any_sleeping()){ spj j_(new T(param_1,param_2,param_3)); j = j_; add(j); }
  else{ function(param_1,param_2,param_3); }
  return j;
}
#ifdef __GXX_EXPERIMENTAL_CXX0X__
template<typename T, typename F, typename... PS>
spj ThreadPool::add_optimized(F function, PS... params){
  spj j;
  if(any_sleeping()){ spj j_(new T(params...)); j = j_; add(j); }
  else{ function(params...); }
  return j;
}
#endif
/******************************************************************************/

/*******************************************************************************
 * ThreadPoolThread
 ******************************************************************************/
void ThreadPool::ThreadPoolThread::run(){
  while(r){
    spj j;
    tp->cond_jl.lock();
    while(0 == tp->jl.size() && r){ 
      //thread is going to sleep
      tp->cond_its.lock();
      ++tp->its;
      tp->cond_its.signal();
      tp->cond_its.unlock();
      tp->cond_jl.wait();
      //thread awakes
      tp->cond_its.lock();
      --tp->its;
      tp->cond_its.signal();
      tp->cond_its.unlock();
    }
    if(0 < tp->jl.size()){
      if(tp->h){ pop_heap(tp->jl.begin(),tp->jl.end(),greater_equal_job()); }
      j = tp->jl.back(); tp->jl.pop_back();
    }
    tp->cond_jl.broadcast();
    tp->cond_jl.unlock();
    if(0 != j){
      j->lock();
      if(!j->status()){ j->run(); }
      j->unlock();
    }
  }
}
void ThreadPool::ThreadPoolThread::quit(){
  r = false;
}
/******************************************************************************/

/*******************************************************************************
 * GTP (Global ThreadPool)
 ******************************************************************************/
ThreadPool * gtp = 0;

void gtp_stop(){ delete gtp; gtp = 0; }

void gtp_init(const size_t threads = 1, const bool heap = false, 
	      const size_t buffer = 0){ 
  if(0 != gtp){ gtp_stop(); } 
  if(0 == (gtp = new ThreadPool(threads,heap,buffer))){ /* do error */ return; }
}

void gtp_add(spj & job){ 
  if(0 != gtp && 0 != job){ gtp->add(job); } 
}

void gtp_sync(spj & job){ if(0 != gtp){ gtp->sync(job); } }

void gtp_sync_all(){ if(0 != gtp){ gtp->sync_all(); } }

const bool gtp_any_sleeping(){ return gtp->any_sleeping(); }

template<typename T, typename F, typename P>
spj gtp_add_optimized_args_one(F function, P param){
  return gtp->add_optimized_args_one<T>(function, param);
}

template<typename T, typename F, typename P1, typename P2>
spj gtp_add_optimized_args_two(F function, P1 param_1, P2 param_2){
  return gtp->add_optimized_args_two<T>(function, param_1, param_2);
}

template<typename T, typename F, typename P1, typename P2, typename P3>
spj gtp_add_optimized_args_three(F function, P1 param_1, P2 param_2, 
				 P3 param_3){
  return gtp->add_optimized_args_three<T>(function, param_1, param_2, param_3);
}

#ifdef __GXX_EXPERIMENTAL_CXX0X__
template<typename T, typename F, typename... PS>
spj gtp_add_optimized(F funtion, PS... parameters){
  return gtp->add_optimized<T>(function, params...);
}
#endif
/******************************************************************************/

#endif //EAD_HPP

