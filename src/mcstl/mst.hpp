/*******************************************************************************
 * Open Source License
 *
 * Copyleft (l) 2010 Ramon Salvador Soto Mathiesen (http://www.stermon.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyleft notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * The Software shall be used for Good, not Evil.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYLEFT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * License based on Douglas Crockford modified MIT Open Source License:
 * http://www.opensource.org/licenses/mit-license.html
 * 
 ******************************************************************************/
#ifndef MCSTL_MST_HPP
#define MCSTL_MST_HPP

#include <functional>
#include <set>
#include <vector>
using std::binary_function;
using std::set;
using std::swap;
using std::vector;

//stdc++
#include <parallel/algorithm>
#include <parallel/settings.h>
//openmp
#include <omp.h>
using std::__parallel::sort;
using __gnu_parallel::force_parallel;
using __gnu_parallel::multiway_mergesort_tag;

typedef __gnu_parallel::_Settings settings;

#include "../utilities/graph.hpp"

namespace mcstl{
  struct less_edge : binary_function <Edge,Edge,bool> {
    bool operator() (const Edge & x, const Edge & y) const { 
      return (x.weight < y.weight);
    }
  };

  template<typename RandomAccessIterator1, typename RandomAccessIterator2>
  vector<Edge>
  mst_kruskal(const Graph<RandomAccessIterator1,RandomAccessIterator2> & g,
	      const size_t & nr_threads = 1){
    const size_t v(g.vertex_end - g.vertex_begin);
    vector<Edge> t;
    //1) create a set for each vertex
    vector<set<Vertex*> > s(v);
    {
      size_t counter(0);
      for(RandomAccessIterator1 it(g.vertex_begin); it != g.vertex_end; ++it){
        s.at(counter).insert(it);
        ++counter;
      }
    }
    //2) sort edges nondecreasing by weight
    sort(g.edge_begin,g.edge_end,less_edge(),multiway_mergesort_tag(nr_threads));
    //3) for each edge (u,v) if vertex in diferent sets, add edge to a and union
    //   sets
    {
      for(RandomAccessIterator2 it(g.edge_begin); it != g.edge_end; ++it){
        // then check for for u v in set and union and insert
        size_t i1(0),i2(0);
        //we look in all set if we can find "v"
        {
          for(size_t i(0); i < s.size(); ++i){
            if(s[i].find(it->u) != s[i].end()){
              i1 = i;
              break;
            }
          }
        }
        //we look in all set if we can find "u"
        {
          for(size_t i(0); i < s.size(); ++i){
            if(s[i].find(it->v) != s[i].end()){
              i2 = i;
              break;
            }
          }
        }
        //if the sets where we found the vertex are different, add edge and
        //union the sets
        if(i1 != i2){
          t.push_back(*it);
          s[i1].insert(s[i2].begin(),s[i2].end()); s[i2].clear(); 
          swap(s[i2],s[s.size()-1]); s.pop_back();
	  if(1 == s.size()){ break; }
        }
      }
    }
    return t;
  }
}


#endif //MCSTL_MST_HPP
