set xlabel "2 to the power of n integers to be sorted"
set ylabel "Scalability in relation to parallel algorithm executed with 1 thread"
set xr [26:31]
set yr [1:30]

plot \
     "sort_par_mcstl_mergesort_scalability.data" u ($2/$3) t 'MCSTL mergesort 2 threads' w linespoints lt -1, \
     "sort_par_mcstl_mergesort_scalability.data" u ($2/$4) t 'MCSTL mergesort 4 threads' w linespoints lt -1, \
     "sort_par_mcstl_mergesort_scalability.data" u ($2/$5) t 'MCSTL mergesort 8 threads' w linespoints lt -1, \
     "sort_par_mcstl_mergesort_scalability.data" u ($2/$6) t 'MCSTL mergesort 16 threads' w linespoints lt -1, \
     "sort_par_mcstl_mergesort_scalability.data" u ($2/$7) t 'MCSTL mergesort 32 threads' w linespoints lt -1
