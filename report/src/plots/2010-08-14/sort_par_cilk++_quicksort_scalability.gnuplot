set xlabel "2 to the power of n integers to be sorted"
set ylabel "Scalability in relation to parallel algorithm executed with 1 thread"
set xr [26:31]
set yr [1:13]

plot \
     "sort_par_cilk++_quicksort_scalability.data" u ($2/$3) t 'Cilk++ quicksort with 2 threads' w linespoints lt -1, \
     "sort_par_cilk++_quicksort_scalability.data" u ($2/$4) t 'Cilk++ quicksort with 4 threads' w linespoints lt -1, \
     "sort_par_cilk++_quicksort_scalability.data" u ($2/$5) t 'Cilk++ quicksort with 8 threads' w linespoints lt -1, \
     "sort_par_cilk++_quicksort_scalability.data" u ($2/$6) t 'Cilk++ quicksort with 16 threads' w linespoints lt -1, \
     "sort_par_cilk++_quicksort_scalability.data" u ($2/$7) t 'Cilk++ quicksort with 32 threads' w linespoints lt -1
