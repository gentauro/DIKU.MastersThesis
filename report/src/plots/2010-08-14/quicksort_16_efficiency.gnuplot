set xlabel "2 to the power of n integers to be sorted"
set ylabel "Efficiency in relation to speedup"
set xr [26:31]
set yr [0:1.25]

plot \
     "quicksort_16.data" u ($2/$3)/16 t 'STL parallel sort' w linespoints lt -1, \
     "quicksort_16.data" u ($2/$5)/16 t 'Cilk++ quicksort' w linespoints lt -1, \
     "quicksort_16.data" u ($2/$6)/16 t 'MCSTL quicksort' w linespoints lt -1, \
     "quicksort_16.data" u ($2/$7)/16 t 'MCSTL quicksort balanced' w linespoints lt -1, \
     "quicksort_16.data" u ($2/$8)/16 t 'EAD++ quicksort' w linespoints lt -1
