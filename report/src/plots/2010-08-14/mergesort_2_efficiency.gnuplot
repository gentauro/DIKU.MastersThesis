set xlabel "2 to the power of n integers to be sorted"
set ylabel "Efficiency in relation to speedup"
set xr [26:31]
set yr [0:1.5]

plot \
     "mergesort_02.data" u ($2/$3)/2 t 'STL parallel sort' w linespoints lt -1, \
     "mergesort_02.data" u ($2/$5)/2 t 'Cilk++ mergesort' w linespoints lt -1, \
     "mergesort_02.data" u ($2/(($6+$7+$8)/3))/2 t 'MCSTL mergesort/exact/sampling' w linespoints lt -1, \
     "mergesort_02.data" u ($2/$9)/2 t 'EAD++ mergesort' w linespoints lt -1
