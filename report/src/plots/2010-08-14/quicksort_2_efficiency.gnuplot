set xlabel "2 to the power of n integers to be sorted"
set ylabel "Efficiency in relation to speedup"
set xr [26:31]
set yr [0:1.5]

plot \
     "quicksort_02.data" u ($2/$3)/2 t 'STL parallel sort' w linespoints lt -1, \
     "quicksort_02.data" u ($2/$5)/2 t 'Cilk++ quicksort' w linespoints lt -1, \
     "quicksort_02.data" u ($2/$6)/2 t 'MCSTL quicksort' w linespoints lt -1, \
     "quicksort_02.data" u ($2/$7)/2 t 'MCSTL quicksort balanced' w linespoints lt -1, \
     "quicksort_02.data" u ($2/$8)/2 t 'EAD++ quicksort' w linespoints lt -1
