set xlabel "2 to the power of n integers to be sorted"
set ylabel "Efficiency in relation to speedup"
set xr [26:31]
set yr [0:1]

plot \
     "quicksort_8.data" u ($2/$3)/8 t 'STL parallel sort' w linespoints lt -1, \
     "quicksort_8.data" u ($2/$5)/8 t 'Cilk++ quicksort' w linespoints lt -1, \
     "quicksort_8.data" u ($2/$6)/8 t 'MCSTL quicksort' w linespoints lt -1, \
     "quicksort_8.data" u ($2/$7)/8 t 'MCSTL quicksort balanced' w linespoints lt -1, \
     "quicksort_8.data" u ($2/$8)/8 t 'EAD++ quicksort' w linespoints lt -1
