set xlabel "2 to the power of n integers to be sorted"
set ylabel "Scalability in relation to parallel algorithm executed with 1 thread"
set xr [26:31]
set yr [1.5:6]

plot \
     "sort_par_mcstl_mergesort_scalability.data" \
     u ((($2+$10+$18)/3)/(($3+$11+$19)/3)) \
     t 'MCSTL mergesort/exact/sampling with 2 threads' w linespoints lt -1, \
     "sort_par_mcstl_mergesort_scalability.data" \
     u ((($2+$10+$18)/3)/(($5+$13+$21)/3)) \
     t 'MCSTL mergesort/exact/sampling with 4 threads' w linespoints lt -1, \
     "sort_par_mcstl_mergesort_scalability.data" \
     u ((($2+$10+$18)/3)/(($9+$17+$25)/3)) \
     t 'MCSTL mergesort/exact/sampling with 8 threads' w linespoints lt -1
