set xlabel "2 to the power of n vertices with a max vertex degree of 2 to the power of n/2"
set ylabel "time in seconds"
set xr [0:15]
set yr [0:200]

plot \
     "time.data" u ($8) t 'Prim MST sequential ' w linespoints lt -1, \
     "time.data" u ($6) t 'Kruskal sequential' w linespoints lt -1, \
     "time.data" u ($7) t 'Kruskal-filter sequential' w linespoints lt -1, \
     "time.data" u ($5) t 'Kruskal parallel (MCSTL mergesort) with 8 threads' w linespoints lt -1
