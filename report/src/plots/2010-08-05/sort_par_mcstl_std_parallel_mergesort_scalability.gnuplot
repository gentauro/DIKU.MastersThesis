set xlabel "2 to the power of n integers to be sorted"
set ylabel "Scalability in relation to parallel algorithm executed with 1 thread"
set xr [0:30]
set yr [0:4]

plot \
     "sort_par_mcstl_std_parallel_mergesort_scalability.data" \
     u ((($2+$6+$10)/3)/(($3+$7+$11)/3)) \
     t 'MCSTL mergesort/exact/sampling with 2 threads' w linespoints lt -1, \
     "sort_par_mcstl_std_parallel_mergesort_scalability.data" \
     u ((($2+$6+$10)/3)/(($4+$8+$12)/3)) \
     t 'MCSTL mergesort/exact/sampling with 3 threads' w linespoints lt -1, \
     "sort_par_mcstl_std_parallel_mergesort_scalability.data" \
     u ((($2+$6+$10)/3)/(($5+$9+$13)/3)) \
     t 'MCSTL mergesort/exact/sampling with 4 threads' w linespoints lt -1
