set xlabel "2 to the power of n integers to be sorted"
set ylabel "Scalability in relation to parallel algorithm executed with 1 thread"
set xr [0:30]
set yr [0:3]

plot \
     "sort_par_tp_quicksort_scalability.data" u ($2/$3) t 'EAD++ quicksort with 2 threads' w linespoints lt -1, \
     "sort_par_tp_quicksort_scalability.data" u ($2/$4) t 'EAD++ quicksort with 3 threads' w linespoints lt -1, \
     "sort_par_tp_quicksort_scalability.data" u ($2/$5) t 'EAD++ quicksort with 4 threads' w linespoints lt -1
