set xlabel "2 to the power of n integers to be sorted"
set ylabel "Scalability in relation to parallel algorithm executed with 1 thread"
set xr [0:30]
set yr [0:3]

plot \
     "sort_par_cilk++_mergesort_scalability.data" u ($2/$3) t 'Cilk++ mergesort with 2 threads' w linespoints lt -1, \
     "sort_par_cilk++_mergesort_scalability.data" u ($2/$4) t 'Cilk++ mergesort with 3 threads' w linespoints lt -1, \
     "sort_par_cilk++_mergesort_scalability.data" u ($2/$5) t 'Cilk++ mergesort with 4 threads' w linespoints lt -1
