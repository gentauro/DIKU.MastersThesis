set xlabel "2 to the power of n integers to be sorted"
set ylabel "Speedup in relation to best sequential algorithm"
set xr [0:30]
set yr [0:2.5]

plot \
     "quicksort_2.data" u ($2/$2) t 'STL sort' w lines lt -1, \
     "quicksort_2.data" u ($2/$3) t 'STL parallel sort' w linespoints lt -1, \
     "quicksort_2.data" u ($2/$4) t 'Sequential quicksort' w linespoints lt -1, \
     "quicksort_2.data" u ($2/$5) t 'Cilk++ quicksort' w linespoints lt -1, \
     "quicksort_2.data" u ($2/$6) t 'MCSTL quicksort' w linespoints lt -1, \
     "quicksort_2.data" u ($2/$7) t 'MCSTL quicksort balanced' w linespoints lt -1, \
     "quicksort_2.data" u ($2/$8) t 'EAD++ quicksort' w linespoints lt -1
