set xlabel "2 to the power of n integers to be sorted"
set ylabel "Efficiency in relation to speedup"
set xr [0:30]
set yr [0:0.7]

plot \
     "quicksort_4.data" u ($2/$3)/4 t 'STL parallel sort' w linespoints lt -1, \
     "quicksort_4.data" u ($2/$5)/4 t 'Cilk++ quicksort' w linespoints lt -1, \
     "quicksort_4.data" u ($2/$6)/4 t 'MCSTL quicksort' w linespoints lt -1, \
     "quicksort_4.data" u ($2/$7)/4 t 'MCSTL quicksort balanced' w linespoints lt -1, \
     "quicksort_4.data" u ($2/$8)/4 t 'EAD++ quicksort' w linespoints lt -1
