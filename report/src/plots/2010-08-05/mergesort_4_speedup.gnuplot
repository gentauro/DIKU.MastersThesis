set xlabel "2 to the power of n integers to be sorted"
set ylabel "Speedup in relation to sequential algorithm"
set xr [0:30]
set yr [0:3]

plot \
     "mergesort_4.data" u ($2/$2) t 'STL sort' w lines lt -1, \
     "mergesort_4.data" u ($2/$3) t 'STL parallel sort' w linespoints lt -1, \
     "mergesort_4.data" u ($2/$4) t 'Sequential mergesort' w linespoints lt -1, \
     "mergesort_4.data" u ($2/$5) t 'Cilk++ mergesort' w linespoints lt -1, \
     "mergesort_4.data" u ($2/(($6+$7+$8)/3)) t 'MCSTL mergesort/exact/sampling' w linespoints lt -1, \
     "mergesort_4.data" u ($2/$9) t 'EAD++ mergesort' w linespoints lt -1
