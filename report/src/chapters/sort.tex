\chapter{Sorting in parallel}
\label{parallelsort}

In this chapter the following sorting algorithms: \algo{Bitonicsort},
\algo{Mergesort} and \algo{Quicksort} will be studied with emphasis on how they
can be parallelized. The sorting algorithms were chosen primary based on two
criteria: the \emph{worst-case} asymptotic time and minimal extra space
utilization, in this order. A minimal requirement of being at least as fast as
the best sequential algorithm, \emph{C++ STL sort}, implemented by David
R. Musser\footnote{http://www.cs.rpi.edu/$\texttildelow$musser/}, a
visualization of the sorting algorithm can be seen in
\ref{fig:parallelsort:introsort}. If the initial experiments show that the
algorithm does not fulfill the minimal requirement, then the algorithm will be
rejected and no \emph{practical efficiency measures} will be made. The three
chosen algorithms have in common that they can be implemented on a
\emph{Exclusive-Read Exclusive-Write (EREW)} \emph{parallel random access
  machine (PRAM)}.

A requirement for the implementation in \emph{C++} of the sorting algorithms is
that they comply with the \emph{C++ STL container} policy \cite{B:2000:USA}. A
function, in this case a sorting algorithm, will only take two \emph{iterators}
as input. The first \emph{iterator} will point to the first element in the
\emph{container} and the second \emph{iterator} will point to the last element
in the \emph{container}. This will allow the function to take different
containers as input, disregarding of their \emph{type}. This can be achieved
with the use of \emph{templates} \cite{VJ:2002:USA}. No implementation details
will be added to the pseudocode.

\begin{figure}[h!]
  \centerline{
    \includegraphics[width=13cm,keepaspectratio=true]
    {figures/sort_introsort_dag.pdf}
  }
  \caption{\emph{C++ STL sort} also known as \emph{introsort} is a variation of
    quicksort were the pivot to the partition is chosen with the
    \emph{median-of-three} technique (white vertices), the large recursive calls
    are eliminated with help of \emph{heap sort} (black vertices) and small
    subsequences, of at most size \emph{16} are left unsorted (grey
    vertices). When the recursion returns, insertion sort is performed on the
    whole sequence in \emph{chunks} of \emph{16} elements. Worst-Case time
    complexity is in \emph{log-linear time} $O(n \lg n)$.}
  \label{fig:parallelsort:introsort}
\end{figure}

\section{Bitonicsort}
\label{psa:bitonicsort}

\algo{Bitonicsort} is a parallel sorting network algorithm, that sorts all
input sequences in a constant number of comparisons which makes it data
independent. The \emph{work} is $\Theta(n \lg^2{n})$ and the \emph{span} is
$O(\lg{n} \cdot \lg{n}) = O(\lg^2{n})$. The \emph{critical path} is calculated
on the basis that it takes $\lg(n)$ stages, where $n= 2^{m},\; m \in \NN$, to to
sort a sequence of $n$ elements with \emph{k processors}:
\begin{align}
  T_k(n) = \sum_{i = 1}^{m}{i} &= \frac{m^2 + m}{2} = \frac{\lg^2{n} +
    \lg{n}}{2} = O(\lg^2{n})
\end{align}
Sorting the sequence can be done without using any extra space. A visual
representation in form of a \emph{directed acyclic graph} can be seen in Figure
\ref{fig:parallelsort:bitonic}.

\begin{figure}[h!]
  \centerline{
    \includegraphics[height=6.5cm,keepaspectratio=true]
    {figures/sort_bitonic_dag.pdf}
  }
  \caption{Representation of \algo{Bitonicsort} computation as a
    DAG. Directed edges go from left to right.}
  \label{fig:parallelsort:bitonic}
\end{figure}

The algorithms is built on a set of network components:
\begin{description}
\item \algo{Comparator}. The network component, which has two input gates and
  two output gates, ensures that the minimal value of the two input values is
  set to the first output gate and the maximum value is set to the second
  gate. Pseudocode for the component can be seen in Algorithm
  \ref{algorithm:comparator}.
\begin{algorithm}[h!]
  \caption{\FuncSty{Comparator(}$x,y$\FuncSty{)}}
  \label{algorithm:comparator}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $x$ and $y$ be two arbitrary numbers.}
  \SetArgSty{}
  $x' \leftarrow \;$ \FuncSty{min(}$x,y$\FuncSty{)}\;
  $y' \leftarrow \;$ \FuncSty{max(}$x,y$\FuncSty{)}\;
  \Return{$(x',y')$}\;
  \SetArgSty{}
\end{algorithm}
\item \algo{Half-cleaner}. The network component, which has $2^m,\; m \in \NN$
  input and output gates respectively, takes a \emph{bitonic sequence} of
  numbers as input. A bitonic sequence is defined as a sequence that
  monotonically increases and then monotonically decreases, $\langle s_0 \leq
  \dots \leq s_i \geq \dots \geq s_n \rangle$ where $0 \leq i \leq n$, or
  monotonically decreases and then monotonically increases, $\langle s_0 \geq
  \dots \geq s_i \leq \dots \leq s_n \rangle$ where $0 \leq i \leq n$. The
  component ensures that all the elements in the top half of the outputted
  elements are less or equal to all the elements in the half bottom of the
  outputted elements. Both outputted halves are still bitonic sequences and the
  bottom is clean bitonic, every element is greater or equal to the first half
  of the output. Pseudocode for the component can be seen in Algorithm
  \ref{algorithm:half-cleaner}.
\begin{algorithm}[h!]
  \caption{\FuncSty{Half-cleaner(}$B$\FuncSty{)}}
  \label{algorithm:half-cleaner}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $B$ be a bitonic array.}
  \KwData{Let $n$ be the length of $B$. We assume that $n = 2^k$ where $k\geq 1$.}
  \KwData{Let $m$ be half the length of $B$, $\left(m = \frac{n}{2}\right)$}
  \SetArgSty{}
  \Parallel{}{
    \For{$i \leftarrow 1$ \KwTo $\frac{n}{2}$}{
      $(B[i],B[m]) \leftarrow \;$ \FuncSty{Comparator(}$B[i],B[m]$\FuncSty{)}\;
      $i \leftarrow i + 1$\;
      $m \leftarrow m + 1$\;
    }
  }
  \Sync{}\;
  \SetArgSty{}
\end{algorithm}
\item \algo{Bitonic-sorter}. The network component, which has $2^m,\; m \in \NN$
  input and output gates respectively, takes a \emph{bitonic sequence} of
  numbers as input and produces a sorted sequence of numbers. The
  \algo{Half-cleaner} network component is part of this component which ensures
  that the received bitonic sequence will be transformed into two bitonic
  sequences and from these two sequences the component calls itself
  recursively. Pseudocode for the component can be seen in Algorithm
  \ref{algorithm:bitonic-sorter}.
\begin{algorithm}[h!]
  \caption{\FuncSty{Bitonic-sorter(}$B$\FuncSty{)}}
  \label{algorithm:bitonic-sorter}
  \KwData{Let $B$ be a bitonic array.}
  \KwData{Let $n$ be the length of $B$. We assume that $n = 2^k$ where $k\geq 1$.}
  \KwData{Let $m$ be half the length of $B$, $\left(m = \frac{n}{2}\right)$}
  \KwData{Let $B_1 = B[1,\dots, m]$ be the first half of the bitonic array $B$ and let $B_2 = B[m+1,\dots, n]$ be the second half.}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \FuncSty{Half-cleaner(}$B$\FuncSty{)}\;
  \If{$2 < n$}{
    \Parallel{}{
      \FuncSty{Bitonic-sorter(}$B_1$\FuncSty{)}\;
      \FuncSty{Bitonic-sorter(}$B_2$\FuncSty{)}\; 
    }
    \Sync{}\;
  }
  \SetArgSty{}
\end{algorithm}
\item \algo{Merger}. The network component, which has $2^m,\; m \in \NN$ input
  and output gates respectively, takes two sorted sequences of numbers as input
  and produces a sorted sequence as output. \algo{Merger} is a recursive
  component that in its first stage produces to bitonic sequences. These
  sequences are sorted by calling the \algo{Bitonic-sorter}
  afterwards. Pseudocode for the component can be seen in Algorithm
  \ref{algorithm:merger}.
\begin{algorithm}[h!]
  \caption{\FuncSty{Merger(}$S_1,S_2$\FuncSty{)}}
  \label{algorithm:merger}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $S_1$ and $S_2$ be sorted arrays.}
  \KwData{Let $m_1$ be the length of $S_1$ and let $m_2$ be the length of $S_2$.}
  \KwData{Let $n$ be the length of $S_1$ and $S_2$, $(n = m_1 + m_2)$. We assume that $m_1$ and $m_2$ have the same length, $(m = m_1 = m_2)$ and that $n = 2^k$ where $k\geq 1$.}
  \SetArgSty{}
  \Parallel{}{
    \For{$i \leftarrow 1$ \KwTo $m$}{
      $(S_1[i],S_2[n]) \leftarrow \;$ \FuncSty{Comparator(}$S_1[i],S_2[n]$\FuncSty{)}\;
      $i \leftarrow i + 1$\;
      $n \leftarrow n - 1$\;
    }
  }
  \Sync{}\;
  \If{$2 < n$}{
    \Parallel{}{
      \FuncSty{Bitonic-sorter(}$S_1$\FuncSty{)}\;
      \FuncSty{Bitonic-sorter(}$S_2$\FuncSty{)}\; 
    }
    \Sync{}\;
  }
  \SetArgSty{}
\end{algorithm}
\item \algo{Sorter}. The network component, which has $2^m,\; m \in \NN$ input and
  output gates respectively, takes an arbitrary sequence of numbers as input and
  produces a sorted sequence of numbers. The component is recursive where in its
  first stage produces two sorted sequences that are given as input to the
  \algo{Merger} that will produce one sorted sequence with all the
  numbers. Pseudocode for the component can be seen in Algorithm
  \ref{algorithm:merger}.
\begin{algorithm}[h!]
  \caption{\FuncSty{Sorter(}$A$\FuncSty{)}}
  \label{algorithm:sorter}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $n$ be the length of $A$. We assume that $n = 2^k$ where $k\geq 1$.}
  \KwData{Let $m$ be half the length of $A$, $\left(m = \frac{n}{2}\right)$}
  \KwData{Let $A_1 = A[1,\dots, m]$ be the first half of the array $A$ and let $A_2 = A[m+1,\dots, n]$ be the second half.}
  \SetArgSty{}
  \If{$2 < n$}{
    \Parallel{}{
      \FuncSty{Sorter(}$A_1$\FuncSty{)}\;
      \FuncSty{Sorter(}$A_2$\FuncSty{)}\; 
    }
    \Sync{}\;
  }
  \FuncSty{Merger(}$A$\FuncSty{)}\;
  \SetArgSty{}
\end{algorithm}
\end{description}

The only limitation to the algorithm is that it can only sort sequences of
numbers with size $2^m,\; m \in \NN$. This size limitation is solved by adding
three more components to the network. This does not change the worst-case
asymptotic time complexity but it adds at most $n$ in extra space usage:
\begin{description}
\item \algo{Find-max}. Given a sequence of size $n$, finding the maximum number
  can be achieved in $O(\lg n)$ time assuming that all non-overlapping
  operations are done in parallel. Pseudocode for the component can be seen in
  Algorithm \ref{algorithm:find-max}.
\begin{algorithm}[h!]
  \caption{\FuncSty{Find-max(}$A$\FuncSty{)}}
  \label{algorithm:find-max}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $n$ be the length of $A$.}
  \KwData{Let $A_1 = A[1,\dots, m]$ be the first half of the array $A$ and let $A_2 = A[m+1,\dots, n]$ be the second half.}
  \SetArgSty{}
  \If{$1 \leq n$}{
    \Return{$A[1]$}\;
  }
  \Parallel{}{
    $x \leftarrow \;$ \FuncSty{Find-max(}$A_1$\FuncSty{)}\;
    $y \leftarrow \;$ \FuncSty{Find-max(}$A_2$\FuncSty{)}\;
  }
  \Sync{}\;
  \Return{\FuncSty{max(}$x,y$\FuncSty{)}}\;
  \SetArgSty{}
\end{algorithm}
\item \algo{Populate}. By taking two sequences as input, $A$ and $B$, it
  populates all the possible elements of $A$ in $B$ when $A \geq B$, otherwise
  it fills the remaining empty places with the number $x$, which is given as a
  parameter. The numbers can be populated in $O(\lg n)$ assuming that all
  overlapping operations are done in parallel. Pseudocode for the component can
  be seen in Algorithm \ref{algorithm:populate}.
\begin{algorithm}[h!]
  \caption{\FuncSty{Populate(}$A,B,b,e,x$\FuncSty{)}}
  \label{algorithm:populate}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ and $B$ be two arrays.}
  \KwData{Let $n$ be the length of $A$. We assume that $n = 2^k$ where $k\geq 1$.}
  \KwData{Let $x$ be an arbitrary number.}
  \KwData{Let $b$ and $e$ be indexes in the array $B$ such that $b \leq e$.}
  \KwData{Let $d$ be the difference between the beginning and ending indexes,
    $(d = e-b)$. We assume that $d = 2^k$ where $k\geq 1$.}
  \KwData{Let $m$ be half the length of $d$, $\left(m = \frac{d}{2}\right)$}
  \SetArgSty{}
  \If{$1 = d$}{
    $B[b] \leftarrow x$\;
    \Return{}\;
    \If{$b < n$}{
      $B[b] \leftarrow A[b]$\;
      \Return{}\;
    }
  }
  \Parallel{}{
    \FuncSty{Populate(}$A,B,b,b+m,x$\FuncSty{)}\;
    \FuncSty{Populate(}$A,B,b+m,e,x$\FuncSty{)}\;
  }
  \Sync{}\;
  \SetArgSty{}
\end{algorithm}
\item \algo{Sorter'}. Given a sequence $A$ of size $n$ where $n \neq 2^m,\; m \in
  \NN$. Can be sorted with a bitonic sorter network by moving all the numbers
  into a new sequence $B$ of size $n'$ where $n' = 2^{\lceil \lg{n} \rceil}$ and
  populating the empty spaces in $B$ with the maximum value from $A$. With the
  previous two components, this can be done in $O(\lg n)$ time. The sequence $B$
  can now inserted into the \emph{bitonic sorting network}. Once the sequence
  $B$ is sorted all the sorted values are moved back to the initial sequence
  $A$, in $O(\lg n)$ time by using the \algo{Populate} component. Because none
  of the used components execution time are greater than $O(\lg^2{n})$,
  therefore it will still dominate the asymptotic time complexity of the
  algorithm and \algo{Sorter'} will still be in $O(\lg^2{n})$. Pseudocode for
  the component can be seen in Algorithm \ref{algorithm:sorter_}.
\begin{algorithm}[h!]
  \caption{\FuncSty{Sorter'(}$A$\FuncSty{)}}
  \label{algorithm:sorter_}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $n$ be the length of $A$.}
  \KwData{Let $k = \lceil \lg{n} \rceil$.}
  \KwData{Let $A'$ be a new array of length $n'=2^k$.}
  \SetArgSty{}
  $x \leftarrow \;$ \FuncSty{Find-max(}$A$\FuncSty{)}\;
  \FuncSty{Populate(}$A,A',0,n',x$\FuncSty{)}\;
  \FuncSty{Sorter(}$A'$\FuncSty{)}\;
  \FuncSty{Populate(}$A',A,0,n,x$\FuncSty{)}\;
  \SetArgSty{}
\end{algorithm}
\end{description}

\subsection*{Implementation details}

Implementing the algorithm as a sequential version was a straightforward task,
except when transforming the parallelizable \emph{for loops} into recursive
calls. Implementing a parallel version based on the sequential code for both
\emph{Cilk++} and \emph{EAD++} was an easy task. The sequential and parallel
code can be seen in Appendix \ref{appendixcdlibrary}.

The initial experiments showed that the parallel algorithm did not fulfill the
minimal requirement no matter the size of the input or the amount of
processors. The algorithm was rejected and there will not be performed
\emph{practical efficiency measures} with real data.

%\todo{move to conclusion?}
Even though in this study the implemented parallel algorithm did not perform as
desired, in \cite{K:2005:USA,CZQXH:2009:China} they show that by implementing
the algorithm with \emph{CUDA} and executing it on a \emph{NVIDIA GeForce GTX
  280}\footnote{http://www.nvidia.com/object/product\_geforce\_gtx\_280\_us.html}
with \emph{240 multi-processor cores}, the performance exceeds the best
sequential algorithm. A conclusion can be made that until the number of
processors available is in the same range as \emph{NVIDIA GPUs}, this algorithm
will not be useful.

\section{Mergesort}
\label{psa:mergesort}

\begin{figure}[h!]
  \centerline{
    \includegraphics[height=6cm,keepaspectratio=true]
    {figures/sort_mergesort_dag.pdf}
  }
  \caption{Representation of \algo{Mergesort} computation as a DAG.}
  \label{fig:parallelsort:mergesort}
\end{figure}

\algo{Mergesort}, in Algorithm \ref{algorithm:mergesort}, is a comparison
sorting algorithm, that sorts input sequences of $n$ elements with $O(n \lg n)$
comparisons. The algorithm is based on the \emph{divide-and-conquer} paradigm
where a main problem is divided into subproblems of the same type until they are
simple enough to be solved. In this particular algorithm the input sequence is
split in half and the algorithm is called recursively on each half. This step is
done until the subproblems are of size one. The two subsequences are merged
together, in Algorithm \ref{algorithm:merge}, forming one single sorted
sequence. The time complexity of the algorithm is in $\Theta(n \lg n)$ with $n$
extra space. The extra space used is to allocate the temporary arrays in the
\algo{Merge} process, in Algorithm \ref{algorithm:merge}. Alternatives to
minimize the use of extra space would be to compute the merge process \emph{in
  place}. A naive implementation is presented in Algorithm
\ref{algorithm:inplacemerge}. The problem with this algorithm is that the time
complexity is in $O(n^2)$. This term would dominate changing the time complexity
of \algo{Mergesort}. This is not acceptable. In \cite{KPT:1996:Finland} an
\emph{in-place mergesort} algorithm is presented that runs in $O(n \lg n)$.

\begin{algorithm}[h!]
  \caption{\FuncSty{Merge(}$A,p,q,r$\FuncSty{)}}
  \label{algorithm:merge}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$, $q$ and $r$ be indexes in the array $A$ such that $p \leq q
    \leq r$.}
  \KwData{Let $A_1$ and $A_2$ be two arrays of size $n_1 = q - p$ and $n_2 =
    r - q - 1$.}
  \SetArgSty{}
  \For{$i \leftarrow 0$ \textbf{to} $n_1$}{
    $A_1[i] \leftarrow A[p + i]$\;
  }
  \For{$i \leftarrow 0$ \textbf{to} $n_2$}{
    $A_2[i] \leftarrow A[q + 1 + i]$\;
  }
  $i \leftarrow 0,\; j \leftarrow 0,\; k \leftarrow p$\;
  \While{$k < r$}{
    \eIf{$A_1[i] \leq A_2[j]$}{
      $A[k] \leftarrow A_1[i]$\;
      $i \leftarrow i + 1$\;
    }{
      $A[k] \leftarrow A_2[j]$\;
      $j \leftarrow j + 1$\;
    }
    $k \leftarrow k + 1$\;
    
  }
  \SetArgSty{}
\end{algorithm}

\begin{algorithm}[h!]
  \caption{\FuncSty{Inplacemerge(}$A,p,q,r$\FuncSty{)}}
  \label{algorithm:inplacemerge}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$, $q$ and $r$ be indexes in the array $A$ such that $p \leq q \leq r$.}
  \SetArgSty{}
  \While{$p < q$}{
    \If{$A[p] > A[q]$}{
      $A[p] \leftrightarrow A[q]$\;
      $q' \leftarrow q$\;
      \While{$q' < r$ \textbf{and} $A[q'] > A[q' + 1]$}{
        $A[q'] \leftrightarrow A[q' + 1]$\;
        $q' \leftarrow q' + 1$\;
      }
    }
    $p \leftarrow p + 1$\;    
  }
  \SetArgSty{}
\end{algorithm}

\begin{algorithm}[h!]
  \caption{\FuncSty{Mergesort(}$A,p,r$\FuncSty{)}}
  \label{algorithm:mergesort}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$ and $r$ be indexes in the array $A$ such that $p \leq r$.}
  \SetArgSty{}
  \If{$p < r$}{
    $q \leftarrow \frac{\lfloor p + r \rfloor}{2}$\;
    \FuncSty{Mergesort(}$A,p,q$\FuncSty{)}\;
    \FuncSty{Mergesort(}$A,q + 1,r$\FuncSty{)}\;
    \FuncSty{Merge(}$A,p,q,r$\FuncSty{)}\;
  }
  \SetArgSty{}
\end{algorithm}

The algorithm can be parallelized because of its nature,
\emph{divide-and-conquer}. Every subproblem can be executed on a separate
process. Using a barrier to ensure that both the subproblems are done before the
two sequences are merged. The \emph{work} is $\Theta(n \lg n)$ and the
\emph{span} is $O(n)$. The \emph{critical path} is bounded to last merge because
the cost of the partitions are in constant time:
\begin{align}
  T_k(n) &= \sum_{i = 1}^{\lg n}{i} + \sum_{i = 1}^{\lg n}{2^i} = \\
  &= O(1) + ... + O(1) + 2 + 4 + ... + \frac{n}{2} + n = O(n)
\end{align}
A visual representation of the algorithm in form of a \emph{directed acyclic
  graph} and pseudocode can be seen in Figure \ref{fig:parallelsort:mergesort}
and Algorithm \ref{algorithm:parallelmergesort} respectively.

\subsection*{Implementation details}
Implementing the algorithm as a sequential and parallel versions for both
\emph{Cilk++} and \emph{EAD++} was once again straightforward task. The
sequential an parallel version of the code can be seen in Appendix
\ref{appendixcdlibrary}.

\begin{algorithm}[h!]
  \caption{\FuncSty{ParallelMergesort(}$A,p,r$\FuncSty{)}}
  \label{algorithm:parallelmergesort}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$ and $r$ be indexes in the array $A$ such that $p \leq r$.}
  \SetArgSty{}
  \If{$p < r$}{
    $q \leftarrow \frac{\lfloor p + r \rfloor}{2}$\;
    \Parallel{}{
      \FuncSty{ParallelMergesort(}$A,p,q$\FuncSty{)}\;
      \FuncSty{ParallelMergesort(}$A,q + 1,r$\FuncSty{)}\;
    }
    \Sync{}\;
    \FuncSty{Merge(}$A,p,q,r$\FuncSty{)}\;
  }
  \SetArgSty{}
\end{algorithm}

An \emph{in place} merge algorithm is already implemented in \emph{C++ STL} that
ensures a worst-case time complexity of $O(n \lg n)$. In the initial experiments
showed that the \emph{C++ STL inplace merge} outperformed the implementation of
the \algo{Merge} algorithm. The experiments also showed that the parallel
algorithm did fulfil the minimal requirement of being faster than the best
sequential algorithm, \emph{C++ STL sort}. The result of the \emph{practical
  efficiency measures} based on \emph{speedup}, \emph{efficiency}, and
\emph{scalability} can be seen in Chapter \ref{results}.

\section{Quicksort}
\label{psa:quicksort}

\begin{figure}[h!]
  \centerline{
    \includegraphics[width=14cm,keepaspectratio=true]
    {figures/sort_quicksort_dag.pdf}
  }
  \caption{Representation of \algo{Quicksort} computation as a DAG.}
  \label{fig:parallelsort:quicksort}
\end{figure}

\begin{algorithm}[h!]
  \caption{\FuncSty{Pivot(}$A,p,r$\FuncSty{)}}
  \label{algorithm:pivot}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$ and $r$ be indexes in the array $A$ such that $p \leq r$.}
  \SetArgSty{}
  \Return{\FuncSty{Random(}$p,r$\FuncSty{)}}\;
  \SetArgSty{}
\end{algorithm}

\begin{algorithm}[h!]
  \caption{\FuncSty{Partition(}$A,p,q,r$\FuncSty{)}}
  \label{algorithm:partition}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$ and $r$ be indexes in the array $A$ such that $p \leq q \leq r$.}
  \SetArgSty{}
  $x \leftarrow A[q]$\;
  $A[p] \leftrightarrow A[q]$\;
  $i \leftarrow p$\;
  \For{$j \leftarrow p + 1$ \textbf{to} $r$}{
    \If{$A[j] \leq x$}{
      $i \leftarrow i + 1$\; 
      $A[i] \leftrightarrow A[j]$\;
    }
  }
  $A[p] \leftrightarrow A[i + 1]$\;
  \Return{$i + 1$}\;
  \SetArgSty{}
\end{algorithm}

\algo{Quicksort}, described in Algorithm \ref{algorithm:quicksort}, is also
a comparison-based sorting algorithm, that sorts input sequences of $n$ elements
with a worst-case of $O(n^2)$ comparisons but an average of $O(n \lg n)$
comparisons. This algorithm is based on the \emph{divide-and-conquer} paradigm
as \algo{Mergesort}. The algorithm is split up into two subsequences. The
\algo{Pivot} is chosen randomly, in Algorithm \ref{algorithm:pivot}, and based
on this the \algo{Partition}, in Algorithm \ref{algorithm:partition}, can create
subsequences of different size unbalancing the recursion tree. The algorithm
calls itself until the size of the subsequence is one. Compared to
\algo{Mergesort}, no bottom-up merging needs to be done. The time complexity of
the algorithm is in $O(n^2)$, \emph{worst-case} scenario with $O(n^2)$
comparisons. The average time complexity is in $O(n \lg n)$ with $O(n \lg n)$
comparisons. Minimizing the \emph{worst-case} time complexity can be achieved by
always setting the \algo{Pivot} to the \emph{median} element in the sequence as
in \cite{CLRS:2009:USA}. The median can be found in \emph{linear time}. This
will ensure that \algo{Partition} will always split up in two equal sized
sequences and the depth of the recursive tree would be bounded to $O(\lg n)$.

\begin{algorithm}[h!]
  \caption{\FuncSty{Quicksort(}$A,p,r$\FuncSty{)}}
  \label{algorithm:quicksort}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$ and $r$ be indexes in the array $A$ such that $p \leq r$.}
  \SetArgSty{}
  \If{$p < r$}{
    $q \leftarrow \;$ \FuncSty{Pivot(}$A,p,r$\FuncSty{)}\;
    $q' \leftarrow \;$ \FuncSty{Partition(}$A,p,q,r$\FuncSty{)}\;
    \FuncSty{Quicksort(}$A,p,q'-1$\FuncSty{)}\;
    \FuncSty{Quicksort(}$A,q' + 1,r$\FuncSty{)}\;
  }
  \SetArgSty{}
\end{algorithm}

The nature of \algo{Quicksort} is also a \emph{divide-and-conquer} algorithm, as
\algo{Mergesort}. The same approach can be used in order to parallelize the
algorithm. An advantage over \algo{Mergesort} is that \algo{Quicksort} do not
need barriers. Once a partition have split the sequence up in two halves there
would be no need for merging them back together. 

The \emph{work} is $O(n^2)$ for the \emph{worst-case} and $O(n \lg{n})$ for the
\emph{average-case}. Respectively the \emph{span} is $O(n^2)$ for the
\emph{worst-case} and $O(n)$ for the \emph{average-case}. The \emph{critical
  path} for the \emph{worst-case} is bounded to the partition and the unbalanced
recursion tree:
\begin{align}
  T_k(n) &= \sum_{i = 0}^{n-1}{n-i} = \\
  &= n + (n-1) + (n-2) + ... + 2 = O(n^2)
\end{align}
The \emph{critical path} for the \emph{average-case} is bounded to the first
partition:
\begin{align}
  T_k(n) &= \sum_{i = 1}^{\lg{n}}{\frac{n}{i}} = \\
  &= n + \frac{n}{2} + \frac{n}{4} + ... + 2 = O(n)
\end{align}
A visual representation of the algorithm in form of a \emph{directed acyclic
  graph} and pseudocode can be seen in Figure \ref{fig:parallelsort:quicksort}
and Algorithm \ref{algorithm:parallelquicksort} respectively.

\begin{algorithm}[h!]
  \caption{\FuncSty{ParallelQuicksort(}$A,p,r$\FuncSty{)}}
  \label{algorithm:parallelquicksort}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$ and $r$ be indexes in the array $A$ such that $p \leq r$.}
  \SetArgSty{}
  \If{$p < r$}{
    $q \leftarrow \;$ \FuncSty{Pivot(}$A,p,r$\FuncSty{)}\;
    $q' \leftarrow \;$ \FuncSty{Partition(}$A,p,q,r$\FuncSty{)}\;
    \Parallel{}{
      \FuncSty{ParallelQuicksort(}$A,p,q'-1$\FuncSty{)}\;
      \FuncSty{ParallelQuicksort(}$A,q' + 1,r$\FuncSty{)}\;
    }
  }
  \SetArgSty{}
\end{algorithm}

\subsection*{Implementation details}

Implementing the algorithm as a sequential and parallel versions for both
\emph{Cilk++} and \emph{EAD++} was once again straightforward task. The
sequential and parallel version of the code can be seen in Appendix
\ref{appendixcdlibrary}.

The \emph{C++ standard template library} offers a function, \emph{$n^{th}$
  element}, that ensures that a chosen element in a sequence will be in the same
position as if the sequence was sorted, $\langle s_0 \leq \dots \leq s_n \geq
\dots \geq s_m \rangle$ where $0 \leq n \leq m$. The function can be used to
select the \emph{median} of a sequence as the pivot and ensuring that partition
always will split the sequence in equal sized subsequences. This will ensure
that the recursion tree will be balanced. The time complexity of the function is
$O(n)$.

The initial experiments showed that the \emph{C++ STL $n^{th}$ element} was
slower than the \algo{pivot}-\algo{partition} combined with the
\emph{load-balancing} from \emph{Cilk++} or \emph{EAD++}. The experiments also
showed that the parallel algorithm fulfilled the minimal requirement of being
faster than the best sequential algorithm. Hereby there will be made
\emph{practical efficiency measures} based on \emph{speedup}, \emph{efficiency},
and \emph{scalability}. The results can be seen in Chapter \ref{results}.
