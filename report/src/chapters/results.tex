\chapter{Results and discussion}
\label{results}

In this chapter the results of the benchmarks for executing the \emph{sorting}
and \emph{minimum spanning tree} algorithms on different computer architectures
are presented. Only one parallel version of a \emph{minimum-spanning-tree}
algorithm was implemented, therefore no \emph{practical efficiency measures} are
made for those algorithms. The \emph{practical efficiency measures} for the
sorting algorithms are visualized as \emph{plots}. The presented plots are
limited to the results of using $2^k$, where $k = 2^i,\; i \in \NN$,
processors. Finally based on the hypothesis and benchmark results, algorithms
will be accepted or rejected for the library.

\section{Sorting}
Only \algo{MergesortParallel} and \algo{QuicksortParallel} in their
\emph{Cilk++} and \emph{EAD++} implementations were benchmarked. As mentioned
in Section \ref{psa:bitonicsort}, \algo{Bitonicsort} did not fulfill the minimal
requirement of being as fast as the best sequential algorithm. The two
algorithms chosen to measure the parallel implementations are the \emph{C++ STL
  sort}, which is the best sequential sorting algorithm, and \emph{C++ STL
  parallel sort}, which is \emph{MCSTL's} implementation of \emph{mergesort}.

\subsection*{Dual-hyperthreading server}

The initial experiments were executed on this server. The result showed that the
\emph{MCSTL} implementation of \emph{mergesort} was much faster than the
versions implemented in \emph{Cilk++} and \emph{EAD++}. The \emph{speedup} and
\emph{efficiency} is similar to the others implementations until the size of the
sequence exceeds $2^{10}$ as in Figure \ref{fig:result:speedupmergehyper24} and
\ref{fig:result:efficiencymergehyper24}. Note that when the sequence is of size
$2^{28}$ the \emph{speedup} and \emph{efficiency} of the \emph{MCSTL mergesort}
is similar to the other two implemented algorithms. This is due to the memory
usage of the algorithm. The server only have \emph{2} GB of RAM and it was
useful to show what happens when each of the algorithms reach the memory
limit. In case of \emph{MCSTL mergesort} the practical performance falls
drastically. In order to compare what happened when the algorithms run out of
memory the following test case was run by setting the size of the sequence to
sort to $2^{29}$:

\scriptsize
\begin{verbatim}
ramon@benzbox:~/$ ./sort_par_tp_mergesort-g++ --n=29 --k=4
Segmentation fault
ramon@benzbox:~/$ ./sort_par_cilk++_mergesort-cilk++ --n=29 -cilk_set_worker_count=4
Bus error
ramon@benzbox:~/$ ./sort_par_mcstl_std_parallel_mergesort-g++ --n=29 --k=4
0.000093411
\end{verbatim}
\normalsize

\emph{EAD++} implementation crashed with a \emph{segmentation fault},
\emph{Cilk++} implementation also crashed with a \emph{bus error}. The only
implementation that did not crash was \emph{MCSTL}. Guessing that no algorithm
is able to sort a sequence of $2^{29}$ numbers in less than $0.1$ milliseconds,
an error message is expected. Specially because this implementation is part of
the \emph{C++ STL parallel library}.

The implementations of the \algo{Quicksort} algorithm showed that once again the
\emph{MCSTL} version was the fastest. The practical performance decreases
drastically when a sequences are greater than $2^{25}$. An explanation can be
that because the algorithm calls the \emph{C++ STL sort}, a sequential
algorithm, whenever there are no more threads available. In case the recursion
tree is unbalanced and some threads finish before others. The finished threads
cannot help due to the algorithm now runs sequentially. The \emph{MCSTL
  load-balanced} version of \emph{quicksort}, based on the \emph{work-stealing}
technique, is the fastest when \emph{2} threads are used. When \emph{4} threads
are used then it is \emph{EAD++} which is the fastest, outperforming even the
\emph{C++ STL parallel sort} for $2^{28}$. The \emph{Cilk++} implementation has
some discrete results. The results can be seen in Figure
\ref{fig:result:speedupquickhyper24} and
\ref{fig:result:efficiencyquickhyper24}.

The \emph{scalability} plots, from Figure \ref{fig:result:scalabilityeadhyper}
to \ref{fig:result:scalabilitymcstlquickhyper}, show that the performance for
algorithms implemented in \emph{Cilk++} and \emph{EAD++} grow whenever extra
processors are added. On the other hand, the performance of the algorithms
implemented in \emph{MCSTL} grow fast, \emph{peak}, and then falls. Note on the
strange behaviour of the \emph{MCSTL mergesort}. This is produced by the
algorithm when reaching the memory limit. The problem is that when executing the
algorithm with only one processor, sequentially, on a sequence of of size
$2^{28}$ the execution time is \emph{318.01} seconds. Comparing this time to
when the algorithm sorts a sequence of size $2^{27}$ with one processor in only
\emph{23.55} seconds. This would move the time complexity of the algorithm from
$O(n \lg n)$ to $O(n^2)$.

All the \emph{speedup} plots (Figures \ref{fig:result:speedupmergehyper24} to
\ref{fig:result:speedupquickhyper24}), \emph{efficiency} plots (Figures
\ref{fig:result:efficiencymergehyper24} to
\ref{fig:result:efficiencyquickhyper24}) and \emph{scalability} plots (Figures
\ref{fig:result:scalabilityeadhyper} to
\ref{fig:result:scalabilitymcstlquickhyper}) can be seen in Appendix
\ref{measuressorthyper}.

\subsection*{Dual-quadcore server}
The limitation for the usage of this server only allowed to perform
\emph{practical efficiency measures} on sequences in the range of $2^{26}$ to
$2^{30}$. The results on this server are very similar to the
previous. \emph{MCSTL mergesort} is still the fastest and without the memory
barrier there are no drastic falls in the performance. The implementation of
\algo{Mergesort} on the other two frameworks, \emph{Cilk++} and \emph{EAD++} are
almost equal.

The results on the implementation of \algo{Quicksort} shows that it is now the
\emph{Cilk++} version that is the best followed closely by the \emph{EAD++}
implementation.

All the \emph{speedup} plots (Figures \ref{fig:result:speedupmergequad24} to
\ref{fig:result:speedupquickquad8}), \emph{efficiency} plots (Figures
\ref{fig:result:efficiencymerge24} to \ref{fig:result:efficiencyquickquad8}) and
\emph{scalability} plots (Figures \ref{fig:result:scalabilityeadquad} to
\ref{fig:result:scalabilitymcstlquickquad}) can be seen in Appendix
\ref{measuressorthyper}.

\subsection*{Tetra-octacore server}
The result on this server confirms what have been seen until now. The best
parallel sorting algorithm implementation is \emph{MCSTL mergesort}. The best
\algo{Quicksort} is the version implemented in \emph{Cilk++}, followed very
closely by the version from \emph{EAD++}.


All the \emph{speedup} plots (Figures \ref{fig:result:speedupmergeocta24} to
\ref{fig:result:speedupquickocta32}), \emph{efficiency} plots (Figures
\ref{fig:result:efficiencymergeocta24} to
\ref{fig:result:efficiencyquickocta32}) and \emph{scalability} plots (Figures
\ref{fig:result:scalabilityeadocta} to
\ref{fig:result:scalabilitymcstlquickocta}) can be seen in Appendix
\ref{measuressorthyper}.

\section{Finding minimum spanning trees}
There was only made benchmark test on the \emph{dual-hyperthreading server} and
\emph{dual-quadcore server}. Access to the \emph{tetra-octacore server} was no
longer available. No \emph{practical efficiency measures} were made because only
one parallel algorithm was implemented. The implementation of \algo{Kruskal}
showed to be the fastest sequential algorithm. In order to execute the benchmark
test and because the server usage was limited by time, the \algo{Bor\r{u}vka}
algorithm was excluded. The initial test showed that the implementation of
\algo{Bor\r{u}vka} was much slower than the others.

\begin{figure}[h!]
  \includegraphics[width=14cm,keepaspectratio=true]
  {figures/results_hyperthreading_mst_time.pdf}
  \caption{MST time on dual-hyperthreading.}
  \label{fig:result:msthyper}
\end{figure}

The test showed that \algo{Prim} was the slowest sequential algorithm and that
\algo{Kruskal} was the fastest. The problem with \algo{Kruskal-filter} is that
is is now bounded to the \emph{worst-case} time complexity of \algo{Quicksort},
which is $|E|^2$, while \algo{Kruskal} is still bounded to time complexity of
\emph{C++ STL sorts}, $|E| \lg |V|$. The best sequential algorithm to measure
against the \algo{Kruskal} parallel implementation based on the \emph{C++ STL
  parallel sort} is \algo{Kruskal}. As can be seen in Figure
\ref{fig:result:msthyper} and \ref{fig:result:mstquad}, the results are very
close. The fastest is the parallel implementation of \algo{Kruskal} based on the
\emph{C++ STL parallel sort}.

\begin{figure}[h!]
  \includegraphics[width=14cm,keepaspectratio=true]
  {figures/results_dualquad_mst_time.pdf}
  \caption{MST time on dual-quadcore.}
  \label{fig:result:mstquad}
\end{figure}

\section{Discussion}
Based on our hypothesis where the minimum requirement is that the algorithm has
to be faster than the best sequential algorithm. Both the implementations of
\algo{Mergesort} and \algo{Quicksort} for \emph{Cilk++} and \emph{EAD++} are
accepted. Likewise is the parallel \algo{Kruskal} implementation based on
\emph{C++ STL parallel sort}.

Out from these results what stands out is the implementation of \emph{MCSTL
  mergesort}. The test performed on the \emph{tetra-octacore} server showed a
\emph{speedup} superior to \emph{20}. How is this possible? The answer is that
the \algo{Mergesort} algorithm implemented in \emph{MCSTL} is not based on a
\emph{binary merge} as the versions implemented for \emph{Cilk++} and
\emph{EAD++} but its based on a \emph{multiway merge}. The \emph{multiway
  mergesort} in \cite{MS:2010:Germany}, is an \emph{external sorting}
algorithm. An \emph{external sorting} algorithm is based on a memory model where
there exist a fast internal memory of size \emph{M} and a large external
memory. The data is moved between memories in block of size \emph{B}. How the
\emph{multiway mergesort} takes advantage of the internal memory is that it can
hold more than one block. Assuming that there can be \emph{k} blocks, an output
block and a small amount of extra storage in the internal memory. Then \emph{k}
blocks containing sorted sequences can be merged in a \emph{k-way}. The merging
of the \emph{k} sequences is done with help of a priority queue containing the
smallest element from each sequence. Insertion of elements and extraction of the
minimum element of the priority queue can be done in \emph{logarithmic time}
because its built on a \emph{minimum-heap}. The smallest element is written to
the output block. Once the output block is full. The sequence of sorted elements
is written to the \emph{external memory}. If one of the \emph{k} blocks is empty
in the \emph{internal memory} a new block is retrieved from the \emph{external
  memory}. The bound set in order to determine the size of the \emph{k} blocks
is set to:
\begin{align}
  M &\geq (k)B + B + k
  \intertext{Where the terms are for the \emph{k} blocks, the output buffer and
    the priority queue. By doing this the number of merging phases will then be
    reduced to $\left \lceil \log_{k}\left(\frac{n}{M}\right) \right \rceil$ and
    the time complexity of the algorithm becomes:} 
  T(n) &= O\left(\frac{n}{B} \left(1 + \left \lceil
        \log_{\frac{M}{B}}\left(\frac{n}{M}\right) \right \rceil \right)\right)
\end{align}

In the \emph{MCSTL multiway mergesort}, the \emph{external memory} is the shared
memory and the \emph{internal memory} is the cache of the \emph{k
  processors}. The more processors added the more internal memory will be
available. By running the algorithm against the sequential version of
\emph{mergesort} a \emph{speedup} of \emph{50} was achieved when using \emph{32}
processors getting \emph{superlinear speedup}. That is why \emph{multiway
  mergesort} is chosen to be the \emph{C++ STL parallel sort} as
\emph{introsort} was chosen to be the \emph{C++ STL sort}.
