\chapter{Existing frameworks and libraries}
\label{libraryreview}

In this chapter several \emph{C++ parallel frameworks and libraries} will be
described with emphasis on how they allow parallelism, handling of shared data
structures and the programming language syntax. As in \cite{MHTKE:2010:Germany},
a few words describing the libraries and an example on of how to parallelize a
sequential algorithm, in this case based on the recursion example from the
previous chapter, will be presented for each library. The implementation in
\emph{C++} of the sequential version of the algorithm, in Listing
\ref{algorithm:recursion}, can be seen in Listing
\ref{libraryreview:seq:recursion}.

\begin{lstlisting}[caption=Simple recursion in a sequential algorithm,
  label=libraryreview:seq:recursion,captionpos=b,float]
template<typename RandomAccessIterator>
void foo(RandomAccessIterator first, RandomAccessIterator last){
  if(first == last){ return; }
  RandomAccessIterator middle(first+((last - first) >> 1));
  foo(first,middle);
  foo(middle,last);
}
\end{lstlisting}

\section{POSIX threads (pthreads)} % C library
\label{libraryreview:posix}

The \emph{Portable Operating System Interface for UNIX Threads} is a \emph{C/C++
  library} that offers parallelism through \emph{threads}. As mentioned in
Section \ref{introduction:multicore}, a \emph{process}, can create
\emph{threads} to execute instructions concurrently. The \emph{API} offers two
types of threads:
\begin{description}
\item [System thread:] Whenever a \emph{process} creates a \emph{system thread},
  the operating system will considered it as a peer to the creating
  \emph{process} and the thread will compete for resources with all the other
  processes running on the system. System threads within a \emph{process} or
  another \emph{system thread} can execute concurrently on several processors,
  taking advantage of multiple processors
  architectures. \emph{PTHREAD\_SCOPE\_SYSTEM} must be given as a parameter in
  order to create a thread as a system thread when calling
  \emph{pthread\_create}.
\item [User thread:] In difference to system threads, user threads within the
  same \emph{process} or \emph{system thread} can never be executed concurrently
  on other processors. This makes \emph{user threads} unsuitable for
  parallelism. In order to create a user thread, \emph{PTHREAD\_SCOPE\_PROCESS}
  must be given as a parameter when calling \emph{pthread\_create}.
\end{description}

The sequential code, in Listing \ref{libraryreview:seq:recursion}, can be
parallelized by doing a few modifications. The function and data types must be
transformed to \emph{(void *)} types because the function that creates a thread,
\emph{pthread\_create}, only allows this type to be sent as a parameter. Data
must be \emph{casted} back to the class in order to use it. The programming
syntax used to write the parallel version of the code is \emph{pure C++}.

\begin{lstlisting}[caption=Parallel implementation of the sequential
  code with POSIX,label=libraryreview:posix:recursion,captionpos=b,float]
#include <pthread.h>
template<typename RandomAccessIterator>
class Range{
  RandomAccessIterator first;
  RandomAccessIterator last;
  Range(RandomAccessIterator first_, RandomAccessIterator last_)
    : first(first_), last(last_) {}
};

template<typename RandomAccessIterator>
void * foo(void * data){
  Data<RandomAccessIterator> * 
    d(static_cast<Data<RandomAccessIterator> * >(data));
  if(d->first == d->last){ return; }
  pthread_t thread;
  RandomAccessIterator middle(d->first+((d->last - d->first) >> 1));
  pthread_create(&thread, 0, &foo<RandomAccessIterator>,
                 (void *) new Range<RandomAccessIterator>(d->first,d->middle));
  foo(d->middle,d->last);
  pthread_join(thread, 0);
}
\end{lstlisting}

The problem in the code, in Listing \ref{libraryreview:posix:recursion}, is
that if there only is a limited number of threads available. Optimal usage of a
system thread is usually \emph{1-to-1} with a core, as mentioned in Section
\ref{introduction:multicore}, but the recursive function will create one thread
for each iteration reaching very fast the operating systems limit. The minimal
value for \emph{\_POSIX\_THREAD\_THREADS\_MAX} is $64$ and it is defined in
\emph{basedefs/limits.h}. A way to avoid this is by using a thread pool. A
thread pool will create a limited number of threads and instead of destroying
the threads when tasks are done, the thread pool will reuse them to execute
other tasks. This is mentioned in \cite{SGG:2004:USA}.

The mechanism presented for handling shared data structures are \emph{mutex},
abbreviation for \emph{mutual exclusion} and \emph{condition} variables. By
using a mutex when writing to data structure, no other thread will be able to
read or write until the mutex is released. This gives \emph{atomicity} when
changing the state of a data structure and ensures no \emph{race condition} will
arise. A \emph{condition} variable allows a thread to wait until a mutex for a
shared data structure is released by the thread holding the lock. This will
result in less computational overhead. A side effect of using \emph{mutex} is
that \emph{deadlocks} can arise when a thread $t_1$ is holding a lock on a data
structure $A$ and waiting for the data structure $B$ while a thread $t_2$ is
holding a lock on the data structure $B$ and waiting for the data structure
$A$. In this library there is no mechanism to detect \emph{deadlocks}, it is the
responsibility of the developer to avoid them.

Synchronization between threads can be achieved by using \emph{barriers}, where
a thread is not allowed to pass the barrier until the condition of the barrier
is fulfilled. The main problem with \emph{POSIX} barriers is that the
implementation is optional, this means that a code relying on this form of
synchronization might not be able to compile on different systems. Another way
to implement synchronization is with the use of \emph{pthread\_join}. This
function ensures that no code will be executed until the created thread is
terminated with \emph{pthread\_exit}. It will only work if the thread is created
in a \emph{joinable} state.

\section{OpenMP} % OpenMP
\label{libraryreview:openmp}

\begin{figure}[h!]
  \centerline{
    \includegraphics[width=14.5cm,keepaspectratio=true,trim = 20mm 30mm 20mm
    20mm, clip]{figures/libraryreview_openmp.pdf}
  }
  \caption{Transformation from OpenMP compiler directives to C++ code.}
  \label{fig:libraryreview:openmp}
\end{figure}

The \emph{Open Multi-Processing}, is a \emph{C, C++ and Fortran API} for shared
memory programming. OpenMP is implemented in all major compilers\footnote{GNU
  Compiler Collection (GCC), Low Level Virtual Machine (LLVM), Intel C++
  Compiler (ICC) and Visual C++} and achieves parallelism through a set of
\emph{compiler directives (\#pragma)}. An example of how this preprocessing
compiler directives would look like if \emph{pthreads} were used can be seen in
Figure
\ref{fig:libraryreview:openmp}\footnote{http://www.cs.aau.dk/$\sim$adavid/teaching/MTP-06/11-MVP06-slides.pdf}.
The parallelism strategy of the library built on a \emph{hierarchical thread
  model} where a main thread creates a set of children treads, knows as
\emph{forks}. The main thread will wait until all the children threads are
finished with their work, named \emph{join}. The sequential code from Listing
\ref{libraryreview:seq:recursion}, can be parallelized with a few
modifications. What stands out looking in the parallelized code, in Listing
\ref{libraryreview:openmp:recursion}, is that parallelization in \emph{OpenMP}
is mostly based on \emph{for loops}. One thing to have in mind when using this
type of parallelization is that this will fork the threads in \emph{linear time}
$O(n)$. A way to achieve this in \emph{logarithmic time} is to use nested
\emph{for loops} where the iterations are limited to \emph{2}. The syntax of the
code is \emph{pure C++}.

\begin{lstlisting}[caption=Parallel implementation of the sequential code with
  OpenMP,label=libraryreview:openmp:recursion,captionpos=b,float]
template<typename RandomAccessIterator>
#include <omp.h>
void foo(RandomAccessIterator first, RandomAccessIterator last){
  if(first == last){ return; }
  RandomAccessIterator middle(first+((last - first) >> 1));
  #pragma omp parallel for
  for(typename iterator_traits<RandomAccessIterator>::value_type i(0); 
      i < 2; ++i){
    if(i == 0){ foo(first,middle); }
    else{ foo(middle,last); }
  }
}
\end{lstlisting}

The thread limit is once again an issue. By comparing the codes from Listing
\ref{libraryreview:posix:recursion} and \ref{libraryreview:openmp:recursion} and
based on Figure \ref{fig:libraryreview:openmp}. OpenMP will actually create
twice the amount of threads than POSIX reaching earlier the operating system
maximum limit of system threads. The problem could be solved in POSIX by using a
thread pool. This solution is not possible for OpenMP.

A set of compiler directives are given in order to work with shared data
structures. By adding \emph{shared(x)} to the compiler directive \emph{\#pragma
  for}, all the forked threads will then have access to the data structure
\emph{x}. An example can be seen below:

\begin{minipage}[float]{0.5\linewidth}
  \scriptsize
\begin{verbatim}
  int bar [] = { 42 };
  #pragma omp parallel for shared(x)
  for(int i(0); i < n; ++i){
    foo(bar);
  }
\end{verbatim}
  \normalsize
\end{minipage}

Synchronization is done when all forked threads are finished and the parent
threads join them. Another way to achieve synchronization between threads is to
use the compiler directive \emph{barrier}. The barrier will ensure that until
all forked threads reach that point, the execution of the parallel code will not
be resumed.

\section{Cilk++}
\label{libraryreview:cilk++}
The \emph{Cilk++} library \cite{L:2009:USA} is an extension to the \emph{C++}
programming language based on \emph{MIT's} parallel programming language,
\emph{Cilk}, created among other by \emph{Professor Charles E. Leiserson},
co-author of \cite{CLRS:2009:USA}. Even though the library is commercial
software, property of \emph{Intel}, the source code is available under an open
source licence\footnote{http://sourceforge.net/projects/cilk/}.

The library offers parallelism through three keywords: \emph{cilk\_for},
\emph{cilk\_spawn} and \emph{cilk\_sync}. This three primitives are mapped
directly to \emph{parallel}, \emph{spawn} and \emph{sync} in
\cite{CLRS:2009:USA}. With these keywords, parallelizing sequential code is
extremely easy. The code from Listing \ref{libraryreview:seq:recursion}, is
parallelized, in Listing \ref{libraryreview:cilk++:recursion}, under less than
a minute in a very intuitive way. The code is almost identical but in order to
compile it a specialized compiler must be used\footnote{Intel offers two
  compilers: One based GNU Compiler Collection (GCC) for Linux systems and the
  other based on Visual C++ for Microsoft systems.}. The syntax of the code is
not \emph{pure C++}.

\begin{lstlisting}[caption=Parallel implementation of the sequential code with
  Cilk++,label=libraryreview:cilk++:recursion,captionpos=b,float]
template<typename RandomAccessIterator>
#include <cilk.h>
void foo(RandomAccessIterator first, RandomAccessIterator last){
  if(first == last){ return; }
  RandomAccessIterator middle(first+((last - first) >> 1));
  cilk_spawn foo(first,middle);
  foo(middle,last);
  cilk_sync;
}

int cilk_main(int argc, char *argv[]){
  int bar [] = { 42 };
  foo(bar+0,bar+1);
}
\end{lstlisting}

In order to work with shared data structures, \emph{Cilk++} introduces
\emph{hyperobjects}, which allows several threads to update the data structure
maintaining the sequential order and ensuring no race conditions. The
\emph{hyperobjects} available are \emph{reducers} which offers the possibility
to append elements to a list or a string, basic logic operations and the
possibility to find the maximum or minimum over a set of values. All other types
of shared data structures are handled with \emph{mutex} as in the \emph{pthread}
library. A visual application named \emph{cilkscreen} can be used to detect
\emph{race conditions}.

As mentioned before, the created applications will only compile with the
specialized compilers and only if the main function in the applications is
renamed to \emph{cilk\_main}. This will restrict portability, if only a minor
part of an application needs to be parallelized. Because of this, the whole
application must be made parallelizable. Looking at the
manual\footnote{http://software.intel.com/en-us/articles/download-intel-cilk-sdk},
merging \emph{C++} and \emph{Cilk++} libraries is not a straightforward task.

In \cite{BL:1999:USA,L:2009:USA} the scheduler for multi-threaded computations
used in Cilk++ is proved to execute an application that is suited for
parallelization with $T(n)$ work and $T_\infty(n)$ span in an expected running
time of:
\begin{align}
  T_k(n) &\leq \frac{T(n)}{k} +
  O(T_\infty(n))\label{libraryreview:cilk++:worksteal}
\end{align}

The utilized scheduler, \emph{work stealing}, is based on a greedy scheduling
technique where it is the inactive processors that try to steal work from the
active processors. This differ from the conventional schedulers, \emph{work
  sharing}, whenever a processor creates new threads, the scheduler tries to
migrate them to other inactive processors.

By recalling the \emph{work law} (\ref{introduction:worklaw}) and \emph{span
  law} (\ref{introduction:spanlaw}) the optimal execution time is:
\begin{align}
  T_k(n) \geq \frac{T(n)}{k} &\implies T_k(n) = \frac{T(n)}{k}\label{libraryreview:cilk++:idealwork}
  \intertext{The optimal span is:}
  T_k(n) \geq T_\infty(n) &\implies T_k(n) = T_\infty(n)\label{libraryreview:cilk++:idealspan}
  \intertext{And the ratio of \emph{parallelism} between \emph{work} and
    \emph{span} is:} 
  &\frac{T(n)}{T_\infty(n)}\label{libraryreview:cilk++:ratioparallelism}
\end{align}
The result of using (\ref{libraryreview:cilk++:idealwork}) and
(\ref{libraryreview:cilk++:idealspan}) as upper bounds for an optimal greedy
scheduler to execute a computation with $T(n)$ work and $T_\infty(n)$ span on a
computer with \emph{k processors}, as in (\ref{libraryreview:cilk++:worksteal}),
is:
\begin{align}
  T_k(n) \leq \frac{T(n)}{k} + O(T_\infty(n))\label{libraryreview:cilk++:workstealproved}
\end{align}
With (\ref{libraryreview:cilk++:workstealproved}) and if the ratio of
\emph{parallelism} (\ref{libraryreview:cilk++:ratioparallelism}) exceeds the
number of $k$ processors with a sufficient margin, at least 10 times more
parallelism than processors, then a guarantee of a nearly \emph{perfect linear
  speedup} is ensured. By rewriting the \emph{work law}
(\ref{introduction:worklaw}) as:
\begin{align}
  T_k(n) \geq \frac{T(n)}{k} &\implies k \geq
  \frac{T(n)}{T_k(n)}\label{libraryreview:cilk++:worklaw2} 
  \intertext{And combining (\ref{libraryreview:cilk++:worklaw2}) with
    (\ref{libraryreview:cilk++:ratioparallelism}). No \emph{perfect linear
      speedup} can be achieved when the number of processors are greater than
    the ratio of \emph{parallelism}}
  \frac{T(n)}{T_k(n)} &\leq \frac{T(n)}{T_\infty(n)} < k
  \intertext{We assumed that the ratio of \emph{parallelism}
    (\ref{libraryreview:cilk++:ratioparallelism}) exceeds the number of $k$
    processors with a sufficient margin}
  \frac{T(n)}{T_\infty(n)} \gg k &\implies T_\infty(n) \ll
  \frac{T(n)}{k}\label{libraryreview:cilk++:speedupdominates}
  \intertext{By combining (\ref{libraryreview:cilk++:workstealproved}) with
    (\ref{libraryreview:cilk++:speedupdominates}) the term $\frac{T(n)}{k}$
    dominates. The running time is:}
  T_k(n) &\approx \frac{T(n)}{k}
  \intertext{And a nearly perfect linear speedup is achieved:}
  k &\approx \frac{T(n)}{T_k(n)}
\end{align}

\section{C++CSP2} % distributed model and not shared-memory, you can't change

\label{libraryreview:c++csp2}
The \emph{C++ Communicating Sequential Processes 2} library is the only reviewed
library that has a \emph{network} model and not a \emph{shared-memory} model
like the other mentioned frameworks. The library is based on \emph{CSP}
\cite{H:1978:USA} as the name indicates. The basics of \emph{CSP} are that each
process must have its own encapsulated data and algorithms and the only way to
communicate between processes is by sending synchronized messages over
channels. In order to share readable and writable data with this library,
channels usage must be avoided. As channels work, it is only possible to send
data structures as readable copies, \emph{const T \&}, which ensures that the
data can not be modified by another process. A parallelization of the sequential
code, in Listing \ref{libraryreview:seq:recursion}, can be seen in Listing
\ref{libraryreview:c++csp2:recursion}. The syntax of the code is \emph{pure
  C++}. Two main problems in this way of implementation is that as in
\emph{pthreads} and \emph{OpenMP} the limit of available system threads are
reached very soon. Even though the library allows to create \emph{user threads},
\emph{RunInThisThread}, this will restrict the created threads only to run on a
single processor. The other main problem is that, if channels are not used, then
there is no longer any mechanism to avoid \emph{race conditions}, which will
make this type of implementation useless.

\begin{lstlisting}[caption=Parallel implementation of the sequential code with
  C++CSP2,label=libraryreview:c++csp2:recursion,captionpos=b,float]
#include <cppcsp/cppcsp.h>
template<typename RandomAccessIterator>
class Foo : public CSProcess{
private:
  RandomAccessIterator first;
  RandomAccessIterator last;
protected:
  void run(){
    if(first == last){ return; }
    RandomAccessIterator middle(first+((last - first) >> 1));
    Run( InParallel
         ( new Foo<RandomAccessIterator>(first,middle) )
         ( new Foo<RandomAccessIterator>(middle,last) ) );
  }
  public:
    Foo(RandomAccessIterator first_, RandomAccessIterator last_) 
      : first(first_), last(last_) {}
  };
template<typename RandomAccessIterator>
void foo(RandomAccessIterator first, RandomAccessIterator last){
  Start_CPPCSP();
  Run( new Foo<RandomAccessIterator> (first,last) );
  End_CPPCSP();
}
\end{lstlisting}

\section{MCSTL} % (OpenMP)
\label{libraryreview:mcstl}
The \emph{Multi-Core Standard Template Library} \cite{SSP:2007:USA} is a
parallel implementation of the \emph{C++ Standard Template Library (STL)}. It
makes use of multiple processors with shared memory and it is based on the
previously described \emph{OpenMP} API. Most of the algorithms from the
\emph{STL} are implemented. Data Structures \emph{heaps} and the \emph{STL
  containers} are not implemented. Since 2008 the library has been part of the
\emph{GNU Compiler Collection (GCC 4.3)}. The library is very
straightforward. For example an application that uses the \emph{STL sort}
function can be refactored by changing only a few things, outcommenting
\emph{using std::sort} and adding \emph{using std::\_\_parallel::sort} and some
extra parameters to ensure parallelism. An example of this can be seen in
Listing \ref{libraryreview:mcstl:parallelsort}.

As mentioned before, \emph{MCSTL} is based on \emph{OpenMP} and hereby inherits
the problem of the operating system thread limit. In the implementation of their
\emph{quicksort} algorithms two solutions are presented. The first solution will
call the best sequential sort algorithm whenever the limit is reached, in
Listing \ref{libraryreview:mcstl:quicksort}. Second a \emph{load-balanced}
version of the algorithm is implemented on the \emph{work-stealing} technique
\cite{BL:1999:USA}, described in Section \ref{libraryreview:cilk++}, where if
one threads finish all its tasks it looks for more work randomly on other
threads. If there is work to do, the thread will steal \emph{half} of the
\emph{victim threads} work.

\begin{lstlisting}[caption=MCSTL Parallel sorting application,
  label=libraryreview:mcstl:parallelsort,captionpos=b,float]
#include <algorithm>
#include <parallel/algorithm>
#include <parallel/settings.h>
#include <omp.h>
#include <algorithm>
//using std::sort;
using std::__parallel::sort;
using __gnu_parallel::force_parallel;

typedef __gnu_parallel::_Settings settings;

int main(int argc, char *argv[]){
  const int k(2);
  const int n(1 << 20);
  int * a(new size_t[n]);
  srand(time(0));
  for(size_t i(n); i--;){
    a[i] = (rand() % (n >> 1) + 1);
  }

  omp_set_dynamic(false);
  omp_set_num_threads(k); //available processors

  settings s;
  s.algorithm_strategy = force_parallel;
  settings::set(s);

  sort(a,a+n);

  return EXIT_SUCCESS;
}
\end{lstlisting}

\begin{lstlisting}[caption=MCSTL parallel quicksort calls sequential sort when
  limit of threads are reached,
  label=libraryreview:mcstl:quicksort,captionpos=b,float]
.../gcc44_c++/parallel/quicksort.h

template<typename RandomAccessIterator, typename Comparator>
  void
  parallel_sort_qs_conquer(RandomAccessIterator begin,
                           RandomAccessIterator end,
                           Comparator comp,
                           thread_index_t num_threads)
  {
    ...

    if (num_threads <= 1){
      __gnu_sequential::sort(begin, end, comp);
      return;
    }

    ...
  }
\end{lstlisting}
