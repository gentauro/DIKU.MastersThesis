\chapter{EAD++ framework}
\label{library}

In this chapter the framework developed under this Master's thesis will be
described briefly in the introduction. Afterwards an analysis reasoning why the
framework was created followed by the design and implementation in
\emph{C++}. Finally a detailed description of the \emph{API} is presented and
the sequential code example from Listing \ref{libraryreview:seq:recursion} will
be parallelized.

The \emph{Efficient Algorithm and Data Structures framework for C++ (EAD++)} is
a \emph{multi-threaded} based on a \emph{shared-memory} model that offers:
\begin{itemize}
\item a limit on the usage of threads with the help of a \emph{thread pool};
\item a mechanism that ensures that jobs with higher priorities are executed
  first. This is achieved with help of a \emph{job pool};
\item automatic load-balance of jobs based on a \emph{helping technique};
\item \emph{atomicity} when changing the state of a shared data structure;
\item a \emph{pure and simple C++} application programming interface (API);
\item no dependencies to certain compilers;
\item a synchronization mechanism based on \emph{global thread} and \emph{job}
  barriers;
\item little memory overhead, a \emph{constant factor}, defined on
  initialization.
\end{itemize}
The framework is around \emph{400} lines of code, including the \emph{open
  source} licence. The implementation can be seen in Appendix
\ref{appendixcdead++}.

The practical efficiency measures, \emph{speedup}, \emph{efficiency} and
\emph{scalability}, from the benchmark test matched up with \emph{Cilk++} and
\emph{MCSTL}. In some cases \emph{EAD++} even outperformed the other
frameworks. For more information on the benchmark tests, look at Chapter
\ref{results}.

\section{Analysis}
\label{library:analysis}
Why create a new framework instead of using some of the previously described in
Chapter \ref{libraryreview}? 

The main problem is that frameworks such as \emph{pthread}, \emph{OpenMP} and
\emph{CPPCSP2} have no built in mechanism to limit the creation of threads. As
showed with the recursion example, the limit of the operating system is reached
very fast. \emph{MCSTL} which is based on \emph{OpenMP} solves this problem by
calling a sequential version of the algorithm. This approach is not
acceptable. If there are no available threads at the time when the sequential
algorithm is called there might be some later on but because the algorithm now
is sequential it cannot make use of the available threads. A framework should
have a mechanism that can limit the usage of threads.

The way that \emph{pthread} handles the sharing of data structures is by casting
data of different types to \emph{void pointers} and back to the given type. This
approach removes the responsibility from the compiler, \emph{C++} is a
\emph{strongly typed} language, and gives it to the developer. The errors will
no longer be found at compiling time but at run time. This is not
acceptable.

The synchronization mechanism must not be built on an optional specification of
a library, as \emph{barriers} in \emph{pthread}. A clear definition must be made
so executed code have the same behavior independent on the platform on which it
is executed.

The presented \emph{application programming interface (API)} must be simple,
intuitive and \emph{pure C++}. Writing code with \emph{compiler directives} and
performing all parallelism with \emph{for loops} it is not very easy or
intuitive. Even though \emph{Cilk++} achieves parallelism with the help of only
the following primitives: \emph{cilk\_for}, \emph{cilk\_spawn},
\emph{cilk\_sync} and \emph{cilk\_main}. The problem is that these primitives
are not considered \emph{pure C++} and are specific to \emph{Intel's} version
of the \emph{GNU Compiler Collection (GCC)}.

Finally the presented framework must not be compiler specific as \emph{MCSTL}
and \emph{Cilk++} are to the \emph{GNU Compiler Collection (GCC)}.

\section{Design and implementation}
\label{library:designimplementation}
Based on the previous analysis, a \emph{pure C++} framework needed to be
created. The backbone of the framework is built on the \emph{pthreads} library
because it:
\begin{itemize}
\item allows to create \emph{system threads} and hereby take advantage of a
  multi-core architecture;
\item is implemented on most platforms such as \emph{Unix}, \emph{Linux} and
  even \emph{Microsoft Windows};
\item has a mechanism to handle shared data structures;
\end{itemize}
The limitations of the library, as previously mentioned, are solved by
introducing:
\begin{itemize}
\item a thread pool that limits the creation of threads;
\item an abstract class, \emph{Job}, that must be inherited in order to add work
  to the thread pool. Because there is only one \emph{type} of work accepted by
  the thread pool, the responsibility of casting void pointers back to data is
  removed from the developers;
\item \emph{global thread} and \emph{job} barriers in order to allow
  synchronization between threads;
\end{itemize}

\subsection*{Thread pool}
The amount of threads in the thread pool are set on initialization. It is not
possible to add or remove threads once the thread pool is running. All the
threads created by the thread pool are \emph{system threads}. Threads in the
thread pool have three states: \emph{active} when executing a job,
\emph{sleeping} when no job is available and \emph{waiting} at a synchronization
barrier. A thread sleeps by waiting on the job pool \emph{condition variable}. A
thread will be awaken when another thread sends a signal, \emph{condition
  broadcast}, after adding a job to the job pool.

The way the thread pool \emph{load-balances} the work between the threads is
based on a \emph{helping technique}. The \emph{helping technique} is introduced
in this thesis and it is based on that a thread will only create a job if there
is at least one inactive thread. The created job is placed in the job pool and
the inactive threads are notified. It is now the responsibility of the inactive
threads to remove the job from the job pool and execute it. By using this
technique, work can be distributed to \emph{k inactive processors} in
\emph{logarithmic time}. Once all the processors are active, no more jobs will
be created. This will reduce the amount of created jobs based on work. On other
techniques such as \emph{work stealing}, previously described, even though there
are no inactive threads, the active threads will still create jobs because when
other threads are finished with their tasks and they become inactive they need
to be able to steal work from other threads. Assuming that all threads finish at
the same time, all the resources and time used to create the stealable jobs are
wasted.

\begin{figure}[h!]
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_legend.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_1.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_2.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_3.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_4.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_5.pdf}
    }
  \end{minipage}
  \caption{EAD++ helping technique.}
  \label{fig:library:threadpool}
\end{figure}

A visual representation on how the thread pool works, can be seen in Figures
\ref{fig:library:threadpool} and \ref{fig:library:threadpool2}. The thread pool
is started with four threads and a job pool of size eight (Figure
\ref{fig:library:threadpool}.b). All threads are active, they notice that the
job pool is empty and go to \emph{sleep} (Figure
\ref{fig:library:threadpool}.c). The main thread adds a job and notifies all the
threads from the thread pool. One of them will take the job and execute it
(Figure \ref{fig:library:threadpool}.d). The thread will now check if other
threads in the thread pool are \emph{sleeping}. In this case, three threads are
\emph{sleeping}. The thread adds a job from the left branch of the graph and
continues to work on the right side of the graph (Figure
\ref{fig:library:threadpool}.e). This will be repeated until all threads are
active (Figure \ref{fig:library:threadpool}.f). After all threads are active
there are no need to create more jobs (Figure
\ref{fig:library:threadpool2}.b). Once again, when there are \emph{sleeping}
threads, new jobs will be added to the job pool (Figure
\ref{fig:library:threadpool2}.c and \ref{fig:library:threadpool2}.e).

\begin{figure}[h!]
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_legend.pdf}
    }
  \label{fig:library:threadpool2:a}
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_6.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_7.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_8.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_9.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_threadpool_step_10.pdf}
    }
  \end{minipage}
  \caption{EAD++ helping technique.}
  \label{fig:library:threadpool2}
\end{figure}

\subsection*{Job pool}
The job pool is implemented as a \emph{stack (LIFO)}, where the last job arrived
is the first one served. The underlying container is a \emph{vector from the C++
  STL}. Because of \emph{polymorphism}, the job pool must be implemented as
vector of job pointers. The memory used for the job pool is reserved when the
thread pool is instantiated and because no dynamic memory allocation is used,
the jobs can be added and removed in \emph{constant time} $O(1)$. The job pool
can also be used as a \emph{priority queue}. This can be achieved by setting the
parameter \emph{heap} to true when instantiating the thread pool. Jobs, based on
their priority, can be added and removed from the \emph{heap} in
\emph{logarithmic time}. Because the underlying data structure is a
\emph{min-heap}, jobs with lower priorities will be served first. The behavior
of a \emph{FIFO} can also be achieved if created jobs have a higher priority
then the job creating them. If not specified the initial size of the job pool is
set to the amount of available threads in the thread pool. No dynamic memory
allocation is allowed and it is defined on initialization, \emph{constant
  factor}.

\subsection*{Shared data structures}
The change of state of a shared data structure is handled with help of
\emph{mutex} and \emph{condition} as in \emph{pthreads}. This will ensure
\emph{atomicity}. Note that \emph{Cilk++} only allows the usage of
\emph{mutex}. It is the developers responsibility not to create
\emph{deadlocks}, and as in the other mentioned frameworks, there is no
mechanism to detect them.

\subsection*{Synchronization}
\begin{figure}[h!]
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_legend.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_job_sync_step_1.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_job_sync_step_2.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_job_sync_step_3.pdf}
    }
  \end{minipage}
  \begin{minipage}[b]{0.5\linewidth}
    \subfloat[][]{
      \includegraphics[width=5cm,keepaspectratio=true]
      {figures/library_job_sync_step_4.pdf}
    }
  \end{minipage}
  \caption{EAD++ job barrier.}
  \label{fig:library:jobbarrier}
\end{figure}

Synchronization can be done in the form of two barriers. The first one is a
\emph{global thread barrier} that should only be called from the \emph{main
  thread} of the application. This barrier ensures that all threads that are
part of the thread pool will have finished their work and that the job pool is
empty before resuming the execution of the code that follows the barrier. If the
global barrier is called from a thread belonging to the thread pool, the
condition will never be fulfilled because there will always be an active thread
waiting at the barrier. Correct usage of the barrier from the main thread is the
following:

\begin{minipage}[float]{0.5\linewidth}
\scriptsize
\begin{verbatim}
{
  ...
  gtp_init(nr_threads, heap, buffer);
  shared_ptr<Job> j1(new Foo<RandomAccessIterator>(first, middle));
  gtp_add(j1);
  gtp_sync_all();
  shared_ptr<Job> j2(new Foo<RandomAccessIterator>(middle, last));
  gtp_add(j2);
  gtp_sync_all();
  gtp_stop();
  ...
}
\end{verbatim}
\normalsize
\end{minipage}

The other type of barrier is the \emph{job barrier}. This barrier ensures that a
thread will wait at that point until the job given as a parameter is
finished. If no other thread is executing the job. The waiting thread will
execute the job. This way the waiting time at \emph{job barriers} is minimal.

A visual representation can be seen in Figure \ref{fig:library:jobbarrier}. The
active threads add a job to the job pool because they notice that a thread is
sleeping. The awoken thread retrieves the a job from the job pool. There are now
three threads active and one job in the job pool (Figure
\ref{fig:library:jobbarrier}.c). The thread performing task in the left branch
of the graph reached a leaf and goes one level up waiting for the other leaf to
be done (Figure \ref{fig:library:jobbarrier}.d). The two other threads are busy
performing a task on the right side so they are not able to help. The left
thread, instead of waiting for the other threads to do the job, does it
itself. Once the job is done the threads go back to sleep, but it does not
remove the job from the job queue, that task will be performed by another thread
whenever it checks if there are more jobs available (Figure
\ref{fig:library:jobbarrier}.e).

The waiting thread can access the unfinished job in the job pool in
\emph{constant time} because the barrier has a pointer to the job. The
implemented pointers are not standard pointers but
\emph{std::tr1::shared\_ptr}\footnote{Smart Pointers (TR1 document nr. n1450):
  http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2003/n1450.html}. This type
of pointer is used because the job pointed at is guaranteed to be deleted when
the last \emph{shared\_ptr} pointing to the job is destroyed. The only compiler
that does not have implemented \emph{TR1}'s shared pointer is \emph{Intel's C++
  Compiler (ICC)}. This can be solved by adding the implementation of the
\emph{boost} library instead.

\section{API}
\label{library:api}

The \emph{application programming interface} presents two interfaces: a
\emph{global thread pool} and a \emph{local thread pool}. The \emph{global
  thread pool} is presented in order to reduce the overhead of parallelizing an
algorithm by allowing functions to call a global variable instead of having to
add an extra parameter, the thread pool, to all the functions.

\subsection*{Global thread pool}
\begin{description}
\item[gtp\_init(size\_t threads, bool heap, size\_t buffer):] The function shuts
  down an already global thread pool, if instantiated, and creates a new thread
  pool with the given parameters. If heap is set to true the job pool will be a
  \emph{min-heap}. If the buffer value is given, the size of the job pool will
  be the amount of threads plus the buffer value.
\item[gtp\_add(shared\_ptr<Job> job):] Adds a job to the job pool. The job will
  be executed by one of the threads from the thread pool.
\item[gtp\_add\_optimized(Function f, Parameters ps):] A job is only created if
  there is at least one sleeping thread. Otherwise the work will be executed by
  the thread calling the function. This function is only supported by compilers
  that have implemented the \emph{experimental} version of the upcoming
  \emph{C++0x}.
\item[gtp\_add\_optimized\_args\_\emph{number}(F f, P1 p1,... , PN pN):] The
  functionality is the same as the previous mentioned function. There are only
  implemented up to \emph{three} arguments. If more arguments are needed, the
  framework should be expanded following the guidelines of these functions.
\item[gtp\_sync\_all():] This barrier ensures that no code after the barrier
  will be executed until all threads in the thread pool have finished their work
  and no jobs are left in the job pool. \remark{This barrier must \textbf{only}
    be called by the main thread that has instantiated the thread pool.}
\item[gtp\_sync(shared\_ptr<Job> job):] This barrier ensures that no code, after
  the barrier, will be executed until the job, given as parameter, is executed
  and finished.
\item[gtp\_stop():] The global thread pool is shutdown correctly.
\end{description}

\subsubsection*{Job class}
\begin{description}
\item[Job(size\_t priority):] An abstract class that must be inherited in order
  to add jobs to the thread pool.
\item[virtual run():] This function must be implemented. It is the function that
  the threads in the thread pool will execute.
\item[priority():] Get the priority of a job.
\item[set\_priority(size\_t n):] Set the priority of a job.
\item[status():] Get the status of a job.
\item[done():] Set the status of a job to done when a job is finished.
\end{description}

\subsubsection*{Mutex and Condition}
These are \emph{C++ classes} wrapping the corresponding \emph{pthread} procedure
calls. These classes should be used when working with shared data structures to
ensure atomicity.

\subsection*{Local thread pool}
The \emph{local thread pool} shares the same interface as the \emph{global
  thread pool} but without the prefix \emph{gtp\_}. The functions
\emph{gtp\_init} are replaced by standard \emph{C++} syntax when instantiating a
class, \emph{ThreadPool ltp(42, false, 0);}. The \emph{gtp\_stop} function is
not necessary. The local thread pool will shut down correctly when the
destructor is called. This occurs whenever the instance of the class goes out of
scope.

\begin{lstlisting}[caption=Parallel implementation of the sequential code with
  EAD++,label=library:ead++:recursion,captionpos=b,float]
template<typename RandomAccessIterator>
class Foo : public Job{
  RandomAccessIterator f,l;
  Foo(RandomAccessIterator first, RandomAccessIterator last)
    : Job(), f(first), l(last) { }
  void foo(RandomAccessIterator first, RandomAccessIterator last){
    if(first == last){ return; }
    RandomAccessIterator middle((last - first) >> 1);
    gtp_add_optimized<Foo<RandomAccessIterator> >(first,middle);
    foo(middle,last);
  }
  void run(){
    foo(f,l);
    done();
  }
};
  
template<typename RandomAccessIterator>
void bar(RandomAccessIterator first, RandomAccessIterator last,
         const size_t & nr_threads = 1, const bool & heap = false,
         const size_t & buffer = 0){
  gtp_init(nr_threads, heap, buffer);
  shared_ptr<Job> j(new Foo<RandomAccessIterator>(first, last));
  gtp_add(j);
  gtp_sync_all();
  gtp_stop();
}
\end{lstlisting}

An example on parallelizing the code from Listing
\ref{libraryreview:seq:recursion}, can be seen in Listing
\ref{library:ead++:recursion}. Basically the only thing that has to be done is
to encapsulate a given function into a class that inherits from the abstract
class \emph{Job}. A virtual function, \emph{run}, must be implemented. A new
function wrapper that instantiates the \emph{thread pool}, in this case the
\emph{global thread pool} adding the first job. The rest of the jobs will be
added recursively by the treads in the thread pool. Thanks to the \emph{global
  thread} barrier no synchronization barrier needs to be added to the function
\emph{foo} as it was necessary for the framework \emph{Cilk++} with
\emph{cilk\_sync} and the \emph{pthread} library with \emph{pthread\_join}.
