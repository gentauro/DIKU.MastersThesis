\chapter{Closure}
\label{closure}

\section{Conclusion}
\label{conclusion}

The two primary goals of the thesis was: to make an experimental study of the
various programming models to implement parallelism from a practical approach
and evaluate them with emphasis on efficiency on multi-core computers; and to
design and implement a framework that could be used by researchers in future
work. Both goals are achieved.

A new terminology is introduced, $T_k(n)$, which stands for: \emph{``The running
  time of an algorithm on a computer with k processors where k is defined as a
  bijection of the k processors to the optimal amount of threads that can be
  executed concurrently on a given hardware processor.''}. The terminology helps
to avoid unexpected results when performing \emph{practical efficiency
  measures}.

In the \emph{Efficient Algorithm and Data Structures framework for C++ (EAD++)},
a new method to \emph{load-balance} work between inactive processors is
introduced. The \emph{helping technique} can distribute work between inactive
processors in \emph{logarithmic time}. Two types of barriers are also
introduced: a \emph{global thread} barrier that can be set by the main thread in
order to avoid having to add a barrier in each functions that are called, as
seen in \emph{pthread} and \emph{Cilk++}; and a \emph{job barriers} that can
chose to execute a given job which is binded to the barrier if no other process
is performing the task instead of just waiting.

The implemented framework, \emph{EAD++}, shows a similar performance on the
studied algorithms if compared to other frameworks such as \emph{Cilk++} or
\emph{MCSTL}. The usage of memory is limited to values given on
initialization. This would make the framework suitable for \emph{embedded
  devices} or upcoming \emph{smartphones} with several cores and a limited
amount of memory. 

As for the studied algorithms, the \algo{Bitonicsorter} will still need more
cores in order to perform better as shown in a recent study. The
\algo{Mergesort} and \algo{Quicksort} are bounded to \emph{Amdahl's law} by
having a computation that takes \emph{linear time}. With regard to the
\emph{minimum-spanning-tree} algorithms, a study on how to replace the
sequential data structures should be made.  

\algo{Kruskal-filter} and \algo{Bor\r{u}vka} only perform well on certain
graphs, since the first one is bounded to a sequential \algo{Quicksort} and the
other only performs well for \emph{sparse} graphs. Full results must be
made available, specifically testing on \emph{worst-case} scenarios.

A general thought for designing parallel algorithms should be to avoid as much
as possible operations that only can be run sequentially. A good example of this
is \emph{multiway mergesort} that tackles the sequential \emph{merge} operation
of the \algo{Mergesort} algorithm, which restricts the critical path, by
executing the process in parallel. Results of \emph{superlinear speedup} are
achieved when comparing to the sequential version of the algorithm.

\section{Future work}
\label{futurework}
Is it possible to make \emph{EAD++} perform better? A likely bottleneck in the
implementation could be the job pool, because it is implemented as an
\emph{array of pointers}. This is due to the use of \emph{polymorphism}. Two
functions in \emph{TR1}, \emph{std::tr1::function}\footnote{Polymorphic Function
  Wrappers (TR1 document nr. n1402):
  http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2002/n1402.html} and
\emph{std::tr1::bind}\footnote{Function Object Binders (TR1 document nr. n1455):
  http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2003/n1455.htm} can be used
to actually introduce \emph{anonymous functions} know from \emph{functional
  programming language}. This way the job pool could be defined as an array of
functions and then the use of the operator \emph{new}, which dynamically
allocates memory on the \emph{memory heap} for each object, could be avoided. A
draft of how the changes would look in the current code can be seen in Listing
\ref{closure:futurework:jobpool}. This will actually simplifies the interface,
no need to encapsulate functions in classes. A challenge would be to add
properties to the jobs like \emph{priority} and \emph{status} in order to be
used for the \emph{job barriers}. These problems are certainly not trivial. Note
that proposals for \emph{TR1} are most likely going to be part of the upcoming
version of C++ (\emph{C++0x}).

\begin{lstlisting}[caption=Proposal to avoid use of polymorphism with help of
  anonymous functions, label=closure:futurework:jobpool,captionpos=b,float]
...
#include <tr1/functional>
...

class ThreadPool{
 ...
protected:
 ...
 vector<function<void ()> > jl;  // job list, default array (LIFO)
 ...
public:
 ...
  void add(function<void ()> & job){ ... }
 ...
};

...
void gtp_add(function<void ()> & job){ ... }
...

template<typename RandomAccessIterator>
void foo(RandomAccessIterator first, RandomAccessIterator last){
  if(first == last){ return; }
  RandomAccessIterator middle(first+((last - first) >> 1));
  gtp_add(bind(foo,first,middle));
  foo(middle,last);
}

template<typename RandomAccessIterator>
void bar(RandomAccessIterator first, RandomAccessIterator last,
         const size_t & nr_threads = 1, const bool & heap = false,
         const size_t & buffer = 0){
  gtp_init(nr_threads, heap, buffer);
  gtp_add(bind(foo<RandomAccessIterator>,first,last));
  gtp_sync_all();
  gtp_stop();
}
\end{lstlisting}

As for the algorithms. It would be interesting to see how well a implementation
of the \emph{multiway mergesort} would perform in \emph{Cilk++} or \emph{EAD++}.

\section{Framework availability}
The \emph{Efficient Algorithm and Data Structures framework for C++ (EAD++)} and
source code, published under a \emph{open source license}, will be made
available through either the \emph{CPH STL} or a open source repository such as
\emph{sourceforge.com}.

