\chapter{Methodology}
\label{methodology}

In this chapter a definition of the different types of \emph{parallel random
  access machine} models of computation used for the studied algorithm will be
specified. Afterwards, the model used to \emph{design}, \emph{analyse},
\emph{implement} and \emph{test} parallel algorithms with emphasis on that they
will be executed on different computer architectures, will be presented. Finally
the criteria for accepting or rejecting the studied algorithms based on
asymptotic \emph{time} and \emph{space} complexities, \emph{asymptotic
  performance measures} and \emph{practical efficiency measures} will be
described.

\section{Parallel random access machine}

The \emph{parallel random access machine (PRAM)} is an abstract theoretical
computer model used to deal with the constraint where $k$ processors are allowed
to access the same memory address concurrently when designing algorithms for a
shared memory system. A visual representation can be seen in Figure
\ref{fig:methodology:pram}.

For each of the studied algorithms in this thesis, the type of \emph{parallel
  random access machine} model of computation will be specified. The three
models are:

\begin{figure}[h!]
  \centerline{
    \includegraphics[width=8cm,keepaspectratio=true]
    {figures/methodology_pram.pdf}
  }
  \caption{Representation of a parallel random access machine (PRAM) where
    \emph{k processors} access a single shared memory cell in a clock cycle.}
  \label{fig:methodology:pram}
\end{figure}

\begin{description}
\item [Exclusive Read Exclusive Write (EREW):] Different locations in the shared
  memory can be read by or written to exclusively by only one processor in the
  same clock cycle.
\item [Concurrent Read Exclusive Write (CREW):] Same locations in the memory can
  be read by several processors but only written to exclusively by one processor
  in the same clock cycle.
\item [Concurrent Read Concurrent Write (CRCW):] Same locations in the memory
  can be read by or written to by several processor in the same clock cycle. To
  this particular model, there are three submodels, based on the \emph{write}
  constraint:
  \begin{description}
  \item [Priority:] processors are assigned distinct priorities, where the
    one with the highest priority is allowed to perform the write to memory.
  \item [Arbitrary:] A random processor is allowed to write.
  \item [Common:] All processors are allowed to write to memory \emph{iff} the values
    are equal. It is the algorithm's responsibility to ensure this condition
    holds.
  \end{description}
\end{description}

An example could be to calculate the logical \emph{OR} from $n$ boolean values
with $k$ processors, where $k = n$. The calculation can be performed in
\emph{logarithmic} time, $O(\lg n)$, if the chosen model is either \emph{EREW}
or \emph{CREW}. A binary recursive tree needs to be done and at each level where
all the operations can be performed concurrently. If the chosen model is
\emph{common CRCW}, the calculation can be performed in \emph{constant} time,
$O(1)$. The initial value in the result slot is \emph{0} and a processor will
write the local value iff it equals \emph{1}. It is important that algorithm do
not write another number than \emph{1} to the shared memory. If this is not the
case and several processes write different values at the same time to the result
slot corrupting the final result, then a \emph{race condition} will arise.

Two things to have in mind when designing an algorithm based on these models is
that \emph{EREW} and \emph{CREW} algorithms can be executed on a \emph{CRCW}
model but not the other way around. And the other is to prioritize correctness
before efficiency.

\section{Algorithmics as Algorithm Engineering}
The chosen model to \emph{design}, \emph{analyse}, \emph{implement},
\emph{test} and measure the \emph{asymptotic performance} and \emph{practical
  efficiency} of the studied algorithm is \emph{Algorithmics as Algorithm
  Engineering}\footnote{http://algo2.iti.kit.edu/sanders/courses/bergen/bergen2.pdf}. The
approach is based on an \emph{eight step} model that is a combination of both a
\emph{theoretical} and \emph{practical} model. As can be seen in the Figure
\ref{fig:methodology:algorithmengineering} the \emph{first} step is to define a
\emph{realistic model}. In this study the two underlying models,
\emph{application} and \emph{machine}, to the \emph{realistic model} will be
defined as:
\begin{description}
\item [Application:] several \emph{C++ frameworks} that implement
parallelism.
\item [Machine:] several \emph{real computers} with different type and amount of
  processors and RAM. It is obvious to see here how the \emph{simple computer
    model}, with one processor and infinite amount of RAM, from the
  \emph{theoretical model} does not fit.
\end{description}

\begin{figure}[h!]
  \centerline{
    \includegraphics[width=14.5cm,keepaspectratio=true,trim = 20mm 20mm 20mm
    35mm, clip]{figures/introduction_algorithm_engineering.pdf}
  }
  \caption{Representation of Algorithmics as Algorithm Engineering.}
  \label{fig:methodology:algorithmengineering}
\end{figure}

The next four steps: \emph{design}, \emph{analysis}, \emph{implementation} and
\emph{experiments} are the core steps of the model. By using an \emph{iterative
  developments model} \cite{BEKPSM:USA:1998}, an algorithm is designed, analyzed
and a \emph{hypothesis} is presented. Afterwards the algorithm will be
implemented, in this case in \emph{C++}, and then initial experiments will be
made. If the initial experiments show that the hypothesis does not hold, a new
iteration can be made. If every time the \emph{fifth} step is reached and the
hypothesis is still not fulfilled, then the algorithm must be rejected. In case
that the hypothesis holds, a new iteration to the \emph{seventh} step will be
made. In this step, experiments are made on \emph{real data}. If the results are
not satisfied a new iteration in the previous four steps can be done and if they
are satisfied then the algorithm can be added to a library or to an application,
in the \emph{sixth} and \emph{eighth} steps respectively.

Hypothesis will be presented on basis of asymptotic \emph{time} and \emph{space}
complexities and \emph{asymptotic performance measures} and \emph{practical
  efficiency measures}. A minimal requirement for the asymptotic time complexity
is that if the execution of the best sequential algorithm is $T(n)$, then the
execution of the parallel algorithm with \emph{$k > 1$ processors} must at least
be as fast $T_k(n) \leq T(n)$. If this minimal requirement is not fulfilled the
hypothesis will not hold, and the algorithm will be rejected. No
\emph{asymptotic performance} and \emph{practical efficiency} will be measured.

\section{Asymptotic performance measures} 
The studied algorithms will be described in pseudocode and represented visually
as \emph{directed acyclic graph (DAG)} as in \cite{J:1992:USA,CLRS:2009:USA}. In
order to understand which parts of the algorithms are parallelizable in a
DAG the following terms must be defined:

\begin{description}
\item [DAG:] A directed acyclic graph, $G = (V,E)$, in this thesis representing
  the computations of an algorithm. The vertices, $V$, represent the
  instructions to be computed and the edges, $E$, represent the dependencies
  between the instructions. If there is a edge, $(u,v) \in E$, the instructions
  in the vertex $v$ cannot be executed before the instructions in the vertex
  $u$.
\item [Work:] The total time of computing all instructions of the algorithm
  executed on a computer with a single processor. Work can be mapped to the
  DAG as all the vertices, $V$, in the graph. The \emph{work law} states
  that if $k$ work can be done in $T_k(n)$ time on a computer with \emph{k
    processors} and the amount of work is only $T(n)$, where $k\cdot T_k(n) \geq
  T(n)$, then:
  \begin{align}
    k\cdot T_k(n) &\geq T(n) \implies \\
    \frac{k\cdot T_k(n)}{k} &\geq \frac{T(n)}{k} \implies \\
    T_k(n) &\geq \frac{T(n)}{k}\label{introduction:worklaw}
  \end{align}
  The execution time of the algorithm with \emph{k processors}, is limited to
  the amount of work divided by the amount of processors.
\item [Span:] The execution time of the longest path in a DAG, also know
  as \emph{critical path}. The asymptotic time complexity of the algorithm is
  dominated by the \emph{critical path}. The \emph{span law} is defined as
  follows: no parallel computer with a limited amount of \emph{k processors} can
  execute a parallel algorithm faster than a computer with an unlimited amount
  of cores. The computer with unlimited amount of processors can always just
  execute the parallel algorithm with \emph{k processors}:
  \begin{align}
    T_k(n) &\geq T_\infty(n)\label{introduction:spanlaw}
  \end{align}
\end{description}


\begin{algorithm}[h!]
  \caption{\FuncSty{Foo(}$A,p,r$\FuncSty{)}}
  \label{algorithm:recursion}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $A$ be an array.}
  \KwData{Let $p$ and $r$ be indexes in the array $A$ such that $p \leq r$.}
  \SetArgSty{}
  \If{$p < r$}{
    $q \leftarrow \frac{r - p}{2}$\;
    \FuncSty{Foo(}$p,q$\FuncSty{)}\;
    \FuncSty{Foo(}$q,r$\FuncSty{)}\;
  }
  \SetArgSty{}
\end{algorithm}

An example of pseudocode and a visual representation of a parallel algorithm
executed with one processor and \emph{k processors} represented as \emph{DAGs}
can be seen in Listing \ref{algorithm:recursion} and Figure
\ref{fig:methodology:dag}. By looking at the pseudocode, the only operation that
the algorithm does is to split up in half recursively. The split operation can
be done in \emph{constant time}, $O(1)$. The amount of operations that will
performed at each stage of the recursion tree is $O(1) + ... + O(\frac{n}{2}) +
O(n)$ respectively where $O(n)$ dominates. So the \emph{work} for the algorithm
is $O(n)$. The \emph{span} in the first DAG, which is executed
sequentially, is of length $n$ because no vertices can be visited at the same
time. In the second DAG, because all non overlapping operations can be
executed in the same time unit at each level of the recursion tree, the
\emph{span} will only be of length $\lg n$. Executing the algorithm sequentially
will be in $T(n) = O(n)$ and executing the algorithm in parallel with \emph{k
  processors} will be in $T_k(n) = O(\lg n)$ when $k \geq n$. We have now set a
lower bound on the running time for the algorithm on a parallel computer with
\emph{k processors}.

\begin{figure}[h!]
  \centerline{
    \includegraphics[height=6cm,keepaspectratio=true]
    {figures/methodology_work_span.pdf}
  }
  \caption{Representation of \algo{Foo} sequential and parallel computation as
    \emph{DAGs}.}
  \label{fig:methodology:dag}
\end{figure}

\section{Practical efficiency measures}
The performance measures that will be used to show that a
designated parallel algorithm is efficient are:
\begin{description}
\item [Speedup:] A Comparison with the best sequential implementation.
\begin{align}
  \text{\emph{speedup}}(k) &=
  \frac{T(n)}{T_k(n)}\label{introduction:pad:speedup}
\end{align}
where $k$ is the number of processors, and $T(n)$ is the time used to
execute the best know sequential algorithm. This algorithm does not have to be
the same as the parallel version, and $T_k(n)$ is the time used to execute the
parallel algorithm with $k$ processors.
\item [Efficiency:] Useful work performed on the available resources.
\begin{align}
  \text{\emph{efficiency}}(k) &= \frac{P_i(k)}{P_m(k)} 
  = \frac{\frac{T(n)}{k}}{T_k(n)} 
  = \frac{T(n)}{k \cdot T_k(n)}
  = \frac{\text{\emph{speedup}}(k)}{k}\label{introduction:pad:efficiency}
\end{align}
where $k$ is the number of processors, $P_i(k)$ is the ideal performance
and is defined as the best sequential time, $T(n)$, divided by the $k$
processors and $P_m(k)$ is the actual performance and equals the time used
to execute the parallel algorithm with $k$ processors, $T_k(n)$.
\item [Scalability:] How increased resources provide a better performance.
\begin{align}
  \text{\emph{scalability}}(k) &=
  \frac{T_1(n)}{T_k(n)}\label{introduction:pad:scalability}
\end{align}
where $k$ is the number of processors, and $T_1(n)$ is the time used to execute
the parallel algorithm with \emph{1} processor and $T_k(n)$ is the time used to
execute the parallel algorithm with $k$ processors.
\end{description}
From these parameters a hypothesis will be presented for measuring performance
on the given architectures. A special consideration for the calculation of
\emph{speedup} is to have in mind:
\begin{description}
\item [Amdahl's law \cite{A:1967:USA,G:1988:USA}:] The maximum speedup of a
  parallel application is bounded to $\text{\emph{speedup}}(k) =
  \frac{1}{T_s(n)}$, where $k$ is the amount of processors. Note that $k$ does
  not affect the calculation of the maximum possible speedup. $T_s(n)$ is the
  time spent computing the sequential part of the program and $T_p(n)$ is the
  time spent computing the parallelizable part of the program. Assuming that
  both the sequential time and the parallelizable time executed on a sequential
  processor is:
\begin{align}
  T(n) &= T_s(n) + T_p(n) = 1\label{introduction:pad:amdahl:sequential}
  \intertext{where the parallel time can be defined as:}
  T_p(n) &= 1 - T_s(n) \\
  T(n) &= T_s(n) + 1 - T_s(n)
  \intertext{running the parallel part from (\ref{introduction:pad:amdahl:sequential}) with $k$ processors:}
  T_k(n) &= T_s(n) + \frac{T_p(n)}{k}\\
  &= T_s(n) + \frac{1 - T_s(n)}{k}\label{introduction:pad:amdahl:parallel}
  \intertext{looking at (\ref{introduction:pad:speedup}), the calculation of
    \emph{speedup} for $k$ processors with
    (\ref{introduction:pad:amdahl:sequential}) and
    (\ref{introduction:pad:amdahl:parallel}) would be:}
  \text{\emph{speedup}}(k) &= \frac{T(n)}{T_k(n)} 
  = \frac{1}{T_s(n) + \frac{1 - T_s(n)}{k}}\label{introduction:pad:amdahl:speedup}
  \intertext{by setting $k$ to infinity:}
  &\lim_{k \to \infty}{\left(\frac{1 - T_s(n)}{k}\right)} = 0\label{introduction:pad:amdahl:limit}
  \intertext{by combining (\ref{introduction:pad:amdahl:speedup}) and
    (\ref{introduction:pad:amdahl:limit}):}
  \text{\emph{speedup}}(k) &= \frac{1}{T_s(n) + 0} = \frac{1}{T_s(n)}
\end{align}
An example, given an algorithm $A$ where $30 \%$ of the algorithm runs in
sequential time cannot be parallelized and $70 \%$ can be parallelized. Taking
into consideration \emph{Amdahl's law} a maximum speedup bound would be set to:
\begin{align}
  \text{\emph{speedup}}(k) &= \frac{1}{T_s(n)} = \frac{1}{\frac{30}{100}} 
  = \frac{1}{\frac{3}{10}} = \frac{10}{3} \approx 3.33
\end{align}
Amdahl's law is overruled when \emph{superlinear speedup} is achieved, an
example is when a problem can be split up so it fits in the \emph{L1 cache}
of each of the available processors bypassing the ideal performance
$P_i(k) = \frac{T(n)}{k}$.

\item [Framework overhead:] On the other hand, some of the studied \emph{C++
    frameworks} have some initial, $T_{io}(n)$, and/or final, $T_{fo}(n)$,
  overhead that can affect the \emph{total completion time} \cite{MSM:2004:USA},
  $T_{tct(k)}(n)$, of the algorithm:
\begin{align}
  T_{tct(k)}(n) &= T_k(n) = T_{io}(n) + T_s(n) + \frac{T_p(n)}{k} +
  T_{fo}(n)\label{introduction:pad:tct}
  \intertext{performing (\ref{introduction:pad:amdahl:limit}) on
    (\ref{introduction:pad:tct}) }
  \text{\emph{speedup}}(k) &= \frac{T(n)}{T_k(n)} 
  = \frac{T(n)}{T_{tct(k)}(n)}\\
  &= \dots = \frac{1}{T_{io}(n) + T_s(n) + T_{fo}(n)}
\end{align}
this will also affect on the maximum speedup bound.
\end{description}  