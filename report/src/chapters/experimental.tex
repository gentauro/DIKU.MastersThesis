\chapter{Experimental setup}
\label{experimentalsetup}

In this chapter the different hardware architectures on which the experiments
were executed will be described. Afterwards, the used compilers in order to
create binaries are mentioned. Finally, how the pseudo-random input data is
created for the tests and a guide on how the reader can reproduce both
correctness and performance test is presented.

The initial experiments from the \emph{fifth} step, in Figure
\ref{fig:methodology:algorithmengineering}, were performed on following hardware:
\begin{description}
\item [Single-dualcore:] \hfill
  \begin{itemize}
    \item Hardware type: Laptop.
    \item Processor: Single Intel(R) Core 2 Duo 2.60GHz and 6144 KB cache each
      core.
    \item Memory: 4 GB RAM.
    \item Operating system: Mac OS X v.10.6.4 (64-bit).
  \end{itemize}
\end{description}

The hardware was used to reject algorithms that did not fulfill the minimal
requirement of being faster than the best sequential algorithm. If an algorithm
was not rejected, \emph{practical efficiency measures} based on \emph{speedup},
\emph{efficiency} and \emph{scalability} with real data, the \emph{seventh} step
in Figure \ref{fig:methodology:algorithmengineering}, were performed on the
following hardware:
\begin{description}
\item [Dual-hyperthreading:] \hfill
  \begin{itemize}
    \item Hardware type: Server.
    \item Processor: Dual Intel(R) Xeon(TM) 2.80GHz with Hyperthreathing enabled
      and 512 KB cache in each processor.
    \item Memory: 2 GB RAM.
    \item Address: benzbox.********.diku.dk
    \item Operating system: GNU/Linux Ubuntu 10.04 LTS (32-bit).
  \end{itemize}
\item [Dual-quadcore:] \hfill
  \begin{itemize}
    \item Hardware type: Server.
    \item Processor: Dual Intel(R) Quad-Core Xeon(R) X5355 2.66GHz and 4096 KB
      cache in each core.
    \item Memory: 16 GB RAM.
    \item Address: knapsack2.********.diku.dk
    \item Operating system: GNU/Linux Ubuntu 10.04 LTS (64-bit).
  \end{itemize}
\item [Tetra-octacore:] \hfill
  \begin{itemize}
    \item Hardware type: Server.
    \item Processor: Tetra AMD Opteron(tm) Octa-Core Processor 6136 2.40GHz and
      512 KB cache in each core.
    \item Memory: 128 GB RAM.
    \item Address: octopus.********.unipi.it
    \item Operating system: SUSE Linux Enterprise Server 11 (64-bit).
  \end{itemize}
\end{description}

The servers were used exclusively. No other processes, besides those from the
operating system, were running under the performance tests. It is mentioned, in
Section \ref{introduction:multicore}, that the ideal mapping between thread and
cores is usually \emph{1-to-1}. This is confirmed on all tested
platforms. Because of the limited time to use the servers, the tests were run
only a few times but always with the same outcome. The ideal would be to run the
test many times and calculate the average of the results.

The \emph{GNU Compiler Collection (GCC)} was used in order to compile the source
code, a minimal requirement of \emph{MCSTL} is \emph{GCC 4.3}. Note that the
\emph{Cilk++} code can only be compiled with \emph{Intel's} own version of
\emph{GCC}. \emph{EAD++} can also be compiled using \emph{Low Level Virtual
  Machine (LLVM)} and \emph{Intel C++ Compiler (ICC)}.

The input data for the sorting algorithms are pseudo-randomly generated lists of
size $2^n$. The interval of the pseudo-random numbers are in the range
$\left[1,\frac{2^n}{2}\right]$, this will ensure that there will be duplicate
numbers. The input data for the \emph{minimum-spanning-tree} algorithms are also
pseudo-randomly generated graphs with at most one minimum spanning tree. The
initial created list of vertices is \emph{shuffled} and then in linear time each
vertex will create an edge with its predecessor vertex and an edge weight in the
range $\left[1,\frac{2^n}{2}\right]$ creating a minimum spanning tree containing
all vertices. The maximum degree of the graph is set to half the number of
vertices. Based on this value several iteration will be made in order to create
new edges from random vertices, if they do not exist already, with an edge
weight in the range $\left[\frac{2^n}{2} + 1,2^n\right]$. The random input data
is generated before the algorithms are executed.

Correctness tests for the sorting algorithms checks on the output if the next
number is greater than the previous. Correctness test for the
\emph{minimum-spanning-tree} algorithms are done by checking all edges in the
outputted list against the initial vertex list by finding the first vertex from
the edge in the vertex list. When the vertex is found, the second vertex from
the edge must match the next vertex in the initial vertex list.

The execution of the algorithms are measured in \emph{nanoseconds}. Creation of
pseudo-random input data is not included in the time measuring.

Both correctness and benchmark test can be reproduced by the reader in order to
confirm the results:

\section*{Reproducing correctness tests}
\label{experimentalsetupcorrectness}
From \emph{/thesis/src} type \emph{make test}. Go to \emph{/thesis/src/test} and
type \emph{sort.bash 1 20 1 2} or \emph{mst.bash 1 20 1 2} which stand for
execute all the binary files created and give as a parameter $n = 2^1 \dots
2^{20}$ and $k = 1 \dots 2$. Where $n$ is the number of elements and $k$ is the
number of processors to be used. There will now be created a folder named after
date combined with time inside \emph{/thesis/src/test/output} containing all the
input of the executed applications each in a separated file. By typing
\emph{grep -n '0' *.txt}, will show if any algorithm has failed. If all files
only contain \emph{1}, the algorithms are tested to work properly. The source
code and scripts can be seen in Appendix \ref{appendixtestcorrectness}.

\section*{Reproducing benchmark tests}
\label{experimentalsetupbenchmark}
From \emph{/thesis/src} type \emph{make benchmark}. Go to
\emph{/thesis/src/benchmark} and type \emph{sort.bash 1 20 1 2} or
\emph{mst.bash 1 20 1 2} which stand for execute all the binary files created
and give as a parameter $n = 2^1 \dots 2^{20}$ and $k = 1 \dots 2$. Where $n$ is
the number of elements and $k$ is the number of processors to be used. There
will now be created a folder named after date combined with time inside
\emph{/thesis/src/benchmark/output} containing all the input of the executed
applications each in a separated file. The source code and scripts can be seen
in Appendix \ref{appendixtestbenchmark}.
