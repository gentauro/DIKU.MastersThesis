\chapter{Introduction}
\label{introduction}

\section{Motivation}
\label{introduction:motivation}
The increasing computation power in modern computers in form of several cores
per processor and more processors, makes it necessary to rethink or to redesign
sequential algorithms and data structures.

Consider a sequential algorithm that is executed in $T(n)$ time. If the task is
fully parallelizable, the execution time of the algorithm would be
$\frac{T(n)}{k}$ time when having $k$ cores running in parallel.

Jordan Hubbard, Director of Engineering of Unix Technologies at Apple and
co-founder of the FreeBSD project, announced at \emph{Open Source Days
  2008}\footnote{http://www.usenix.org/event/lisa08/tech/hubbard\_talk.pdf} that
Intel, according to their plan, has the intention to ship computers in 2015 with
\emph{ONE MILLION} cores. With that amount of cores, sequential algorithms and
sequential data structures will not be able to utilize optimally the available
hardware resources.

With all that computational power is it possible to design efficient algorithms
and data structures on multi-core computers, and if so which framework should be
used?  An obvious approach would be to use parallelism. Looking at
\cite{SSP:2007:France} an increased performance is obtained if parallelized
algorithms from the \emph{Multi-Core Standard Template Library (MCSTL)} are
executed on a computer with a greater amount of cores. On the other hand we have
\cite{E:2008:Denmark,D:2010:Denmark}, both implementation of a \emph{Software
  Transactional Memory library for C++ (ESTM and DikuSTM)}, where the
performance decreases after adding seven or eight threads. There are several
different programming models to implement parallelism, including:

\begin{description}
\item[Parallelizing algorithms:] Take well known efficient sequential algorithms
  and redesign them so that the execution of non-overlapping operations can be
  done concurrently on several processors, reducing the execution time of the
  algorithm.
\item[Software Transactional Memory:] By introducing a transactional model
  similar to databases transactions, concurrent read and write can be done on a
  shared data structures ensuring the following properties: \emph{atomicity},
  \emph{consistency} and \emph{isolation} (ACI).
\item[Skeletons \cite{CM:2004:Holland}:] Useful patterns of parallel computation
  and interaction can be packaged up as \emph{frameworks} or \emph{template
    constructs}.
\item[Communicating sequential processes (CSP) \cite{H:1978:USA}:] Formal
  language for describing patterns of interaction with messages between
  processes in concurrent systems.
\end{description}

It is necessary to make an experimental study on the proposed models and
evaluate them in combination with the release of an \emph{open source} parallel
framework that can be used by researchers in future experimental studies.

\section{Multi-core computers}
\label{introduction:multicore}
A \emph{multi-core computer} is defined as a computer having at least one
\emph{central processing unit (CPU)} with several cores. The \emph{core} is the
part of the processor that performs the execution of the instructions. On a
\emph{single-core} processor only one instruction can be processed at a given
time while \emph{multi-core} processors can execute several instructions. This
is interesting because it allows computers with only one CPU to be able to run
parallel applications.

The naming convention for describing how many cores a \emph{multi-core}
processor has, is done by prefixing with a word for the number of cores:
\emph{dual-core, quad-core, octa-core}, etc.

Take two processors from \emph{Intel}, \emph{Intel Core 2 Duo Processor E8200
  (6M Cache, 2.66 GHz, 1333 MHz
  FSB)}\footnote{http://ark.intel.com/Product.aspx?id=33909} and \emph{Intel
  Core 2 Quad Processor Q9400S (6M Cache, 2.66 GHz, 1333 MHz
  FSB)}\footnote{http://ark.intel.com/Product.aspx?id=40814}, with the same
\emph{cache}, \emph{CPU clock speed}, \emph{FSB Speed} and where the second
processor has twice the amount of cores. Assuming that we have the fully
parallelizable application from Section \ref{introduction:motivation}, the
computation can be done in half the time.

A thing to have in mind when speaking about \emph{multi-core} processors is that
an instance of an executed application, a \emph{process}, can create several
independent streams of instructions that can be scheduled to run concurrently by
the operating system, called \emph{threads}. The ideal mapping between the
\emph{threads} and \emph{cores} is usually \emph{1-to-1} but it is not
determined for all architectures. \emph{Sun's UltraSPARC T1
  octa-core}\footnote{http://www.oracle.com/us/products/servers-storage/microelectronics/030990.htm}
processor can execute \emph{32} threads concurrently, \emph{4} on each core.
\emph{Intel's multi-core} processor with \emph{Hyper-Threading
  Technology}\footnote{http://www.intel.com/info/hyperthreading/}, can execute
two virtual threads on each core, assuming that not all of the instructions on a
core are used in each clock cycle. The performance in the \emph{Hyper-Threading
  Technology} depends on if the threads can be executed in the same clock cycle.

In literature \cite{J:1992:USA,CLRS:2009:USA} the running time of an algorithm
on a computer with \emph{p} processors is defined as $T_p(n)$ or $T_p$
respectively. Where \cite{J:1992:USA} defines \emph{p} as a processor, the
terminology of multi-core was not defined when the book was written and
\cite{CLRS:2009:USA} defines \emph{p} as both processors and cores.

In this thesis a new terminology is introduced taking into consideration that a
core can execute several threads concurrently:
\begin{description}
\item [\boldmath{$T_k(n)$}:] The running time of an algorithm on a computer with
  \emph{k} processors, where \emph{k} is defined as a bijection of the \emph{k}
  processors to the optimal amount of threads that can be executed concurrently
  on a given \emph{hardware processor}.
\end{description}
The new terminology will ensure that no unexpected results, such as a
significant lower execution time, will occur when performing practical
efficiency measures on algorithms.

\section{Parallel algorithms and data structures}
\label{introduction:pad}

The study will be based on less-know parallel algorithms as well as the
parallelization of well-known sequential algorithms in literature
\cite{J:1992:USA,WA:2004:USA,CLRS:2009:USA} and implemented on different
\emph{C++ parallel frameworks}. The chosen \emph{parallel model} is the
\emph{shared-memory model}, where \emph{k processors} have access to a shared
memory unit concurrently in order to exchange information. Other models such as
the \emph{network model or distributed model}, where several computers
exchanging information concurrently over a network or specific models as
\emph{CUDA} for \emph{NVIDIA} graphic cards will be excluded. Because of this
limitation, techniques as \emph{skeletons} which only have one C++ parallel
library, \emph{The M\"unster Skeleton Library (Muesli)} \cite{CPK:2009:Germany},
will be excluded because it built on the \emph{Message Passing Interface (MPI)},
a framework that \emph{serializes} and sends data structures in messages in
order to exchange information. It is most suited for sending serialized data
over a network between computers. It is possible to use on a single machine, but
the serialization overhead of the data transformation to communicate between
core/processors would be excessive and unnecessary.

The \emph{multi-threaded} frameworks based on the shared-memory model that will
be reviewed are: \emph{POSIX Threads}, \emph{OpenMP}, \emph{MCSTL},
\emph{Cilk++}, \emph{C++CSP2} and can be seen in Chapter \ref{libraryreview}.

% Outline (skitsere) the solution. Do when code finish.
\section{An efficient library for C++}
\label{introduction:eadlibraryc++}

The library presented in this thesis offers efficient \emph{sorting} and
\emph{minimum-spanning-tree} algorithms that have a lower execution time and at
least the same \emph{worst-case} asymptotic time complexity as the best
sequential algorithm.

The two types of the algorithms were chosen based on that some sorting
algorithms are suited for parallelization and do not depend on complex data
structures. While \emph{minimum-spanning-tree} algorithms are not trivially
parallelizable and depend on complex data structures such as \emph{sets} and
\emph{heaps}. The source code to the implemented algorithms can be seen in
Appendix \ref{appendixsrc}.

The algorithms are implemented with the \emph{Multi-Core Standard Template
  Library (MCSTL)}, \emph{Cilk++} and the \emph{Efficient Algorithm and Data
  Structures framework for C++ (EAD++)}, developed under this Master's
thesis. The \emph{EAD++} is a \emph{multi-threaded} framework based on a
\emph{shared-memory} model and offers a \emph{pure and simple C++ API}, a
\emph{limit} on usage of threads, \emph{load-balance} between work performed by
threads, \emph{atomicity} when a shared data structure state is changed and
\emph{low memory} usage. A complete description can be seen in Chapter
\ref{library}.

\emph{Pure C++} is defined by Bjarne Stroustrup in \cite{B:2000:USA} as
\emph{``the language independently of any particular software development
  environment or foundation library (except the standard library, of
  course)''}. By offering a \emph{pure and simple C++ API}, researches can
implement and add algorithms to the library ensuring that the minimal
requirements for \emph{asymptotic performance measures} and \emph{practical
  efficiency measures} are met. The criteria to measure the \emph{asymptotic
  performance} and \emph{practical efficiency} besides the methodology to
design, analyze, implement and test algorithms can be seen in Chapter
\ref{methodology}.

Satisfactory results are presented when comparing \emph{EAD++} to other
available frameworks and libraries. The comparison shows that similar practical
efficiency is obtained, sometimes even better, with the use of less memory. In
order to ensure that the presented results are correct a set of test are
produced. Both the results and the test can be reproduced by the reader, for
more information on results and correctness test look at Chapters \ref{results}
and \ref{experimentalsetup} respectively.

