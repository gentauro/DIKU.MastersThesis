\chapter{Finding minimum spanning trees in parallel}
\label{parallelmst}

As in the previous chapter, the algorithms for \emph{finding minimum spanning
  tree} will also be studied with emphasis on how to they can be
parallelized. The \emph{worst-case} asymptotic time with minimal extra space
utilization and a minimal requirement of being at least as fast as the best
sequential algorithm will also be the main criteria to accept or reject an
algorithm. The best sequential algorithm will be chosen from the studied
algorithms because no algorithm to find minimum spanning trees is part of the
\emph{C++ STL}. The algorithms are: \algo{Prim}, \algo{Kruskal} with the
\algo{Kruskal-filter} variant and \algo{Bor\r{u}vka}. The chosen algorithms have
in common that they can be implemented on a \emph{Exclusive-Read Exclusive-Write
  (EREW)} \emph{parallel random access machine (PRAM)}.

In a \emph{connected and undirected graph}, $G(V,E)$, where $V$ is a set of
vertices and $E$ is a set of edges, a \emph{spanning tree} is defined as a tree
containing all \emph{vertices} connected through \emph{edges}. These edges must
not form cycles. The \emph{minimum spanning tree} is a spanning tree
containing all the vertices but only with the minimal amount of edges connecting
the vertices.

\section{Prim (Dijkstra-Jarn\'ik-Prim, DJP)}
\label{pmst:prim}

\begin{figure}[h!]
  \centerline{
    \includegraphics[height=6cm,keepaspectratio=true]
    {figures/mst_prim_dag.pdf}
  }
  \caption{Representation of \algo{Prim} computation as a DAG.}
  \label{fig:mst:prim}
\end{figure}

\algo{Prim}, described in Algorithm \ref{algorithm:prim}, finds the
\emph{minimum spanning tree}, $T$, in a \emph{connected and undirected graph},
$G(V,E)$, by:
\begin{itemize}
\item initially setting all the \emph{key} values to $\infty$ and the closest
  neighbor, $\pi$, to \emph{none} for all the vertices. These operations are
  made in \emph{linear time}, $O(|V|)$;
\item secondly selecting a random vertex, $r$, and setting the key value to
  $0$. The operation are made in \emph{constant time}, $O(1)$;
\item afterwards adding all the vertices to a \emph{min-heap}; The operation is
  made in \emph{linear time}, $O(|V|)$;
\item selecting the vertex with the lowest key value removing it from the
  heap. In this case the random vertex, $r$, with a key value of $0$. The
  removed vertex now updates, all its adjacent vertices key values with the
  weight of the edge from $v$ to $u$ and the closest neighbour, $\pi$, with $v$
  if the weight of the edge is less than $u$ key value, if they are in the
  heap. These operations are made in \emph{linear time}, $O(|E| + lg |V|)$;
\item repeating the previous operation until there are no more vertices in the
  heap. These operations are made in \emph{log-linear time}, $O((|E| + |V|)\lg
  |V|)$;
\item iterating through each vertex, except the randomly picked vertex, $r$, and
  adding an edge from the vertex to its closest neighbour with a weight of its
  key value. The operation are made in \emph{linear time}, $O(|V|)$;
\end{itemize}

\begin{algorithm}[h!]
  \caption{\FuncSty{Prim(}$G$\FuncSty{)}}
  \label{algorithm:prim}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $V$ be a set of vertices.}
  \KwData{Let $E$ be a set of edges.}
  \KwData{Let $G$ be a connected undirected graph $G = (V,E)$.}
  \KwData{Let $w(u,v)$ be the weight function of an edge that connects $u$ to $v$.}
  \KwData{Let $v_{k}$ be the minimum weight of any edge connecting $v$ to the
    tree.}
  \KwData{Let $v_{\pi}$ be the parent vertex of $v$ in the tree.}
  \KwData{Let $v_{adj}$ be a set of vertices that $v$ is adjacent to.}
  \KwData{Let $H$ be a \emph{minimum heap}.}
  \KwData{Let $T$ be a subgraph of $G$ where $T = \emptyset$.}
  \SetArgSty{}
  \ForEach{$v \in V$}{
    $v_{k} \leftarrow \infty$\;
    $v_{\pi} \leftarrow \text{NIL}$\;
  }
  $r \leftarrow \;$\FuncSty{Random(}$V$\FuncSty{)}\;
  $r_{k} \leftarrow 0$\;
  $H \leftarrow \;$\FuncSty{Make-heap(}$V$\FuncSty{)}\;
  \While{$H \neq \emptyset$}{
    $u \leftarrow \;$\FuncSty{Extract-heap(}$H$\FuncSty{)}\;
    \ForEach{$v \in u_{adj}$}{
      \If{$v \in Q$ \textbf{and} $w(u,v) < v_k$}{
        $v_{\pi} \leftarrow u$\;
        $v_k \leftarrow w(u,v)$\;
        \FuncSty{Decrease-heap(}$H,v$\FuncSty{)}\;
      }
    }
  }
  \ForEach{$v \in V \backslash \{r\}$}{
    $T \leftarrow T \cup (v,v_{\pi})$\;
  }
  \Return{$T$}\;
  \SetArgSty{}
\end{algorithm}

Depending on the data structures used to implement the algorithm the time
complexity varies from:
\begin{itemize}
\item $O(|V|^2)$ if an \emph{adjacency-matrix} is used, where $n = |V|$; 
\item $O((|V| + |E|)\lg |V|) = O(|E| \lg |V|)$ if an \emph{adjacency-list} is
  used in combination with a \emph{binary heap};
\item $O(|E| + |V|\lg |V|)$ if an \emph{adjacency-list} is used in combination
  with a \emph{Fibonacci heap};
\end{itemize}

A space inconvenience with this algorithm with respect to others
\emph{minimum-spanning-tree} algorithms is that it needs to store the neighbors
of each vertex in either an \emph{adjacency list or matrix}. The \emph{adjacency
  matrix} will always need $|V|^2$ extra space and the worst-case scenario for the
\emph{adjacency list} would be a \emph{complete graph}, every vertex is
connected to all the other vertices, where the \emph{adjacency list} would need
$\frac{|V|(|V|-1)}{2}$ extra space.

The main problem in order to parallelize this algorithm is to handle with the
\emph{minimum-heap}. While one \emph{thread} performs the operations from line
\emph{8-13} in Algorithm \ref{algorithm:prim}. No other thread can have access
to the data structure. Once the tread terminates it can perform once again the
same operations without needing other threads. Attempts of replacing the heap
with other data structures have been tried but without success.  The \emph{work}
is $O(|E| \lg |V|)$ and the span is $O(|E| \lg |V|)$. The \emph{critical path}
is bounded to the sequential operations of the heap combined with the updates on
the adjacency lists:
\begin{align}
  T_k(n) &= O(|V|) + O(1) + O(|V|) + O((|E| + |V|) \lg |V|) + O(|V|) \\
  &= O(|E| \lg |V|)
\end{align}
Since the algorithm will not gain an increased performance when adding more
processes, the algorithm is rejected and there will not be performed initial
experiments. A visual representation in form of a \emph{directed acyclic graph}
can be seen in Figure \ref{fig:mst:prim}.

\subsection*{Implementation details}
The algorithm is only implemented in a sequential version and can be seen in
Appendix \ref{appendixcdmstsrc}. A \emph{vertex'} class is introduced expanding
the initial \emph{vertex} class with the following properties: \emph{key value},
\emph{adjacency list}, \emph{closest neighbor ($\pi$)} and \emph{in heap}. There
is also implemented a \emph{crease} function that can be used for
\emph{decreasing} a node in a minimum heap or \emph{increasing} a node in a
maximum heap. The \emph{crease} function complies with the \emph{C++ STL
  container} policy.

\section{Kruskal}
\label{pmst:kruskal}

\begin{figure}[h!]
  \centerline{
    \includegraphics[height=6cm,keepaspectratio=true]
    {figures/mst_kruskal_dag.pdf}
  }
  \caption{Representation of \algo{Kruskal} computation as a DAG.}
  \label{fig:mst:kruskal}
\end{figure}

\algo{Kruskal} algorithm, in Algorithm \ref{algorithm:kruskal}, finds the
\emph{minimum spanning tree}, $T$, in a \emph{connected and undirected graph},
$G(V,E)$, by:
\begin{itemize}
\item initially adding each vertex to its own set of vertices. These operations
  are made in \emph{linear time}, $O(|V|)$;
\item sorting all edges in a non decreasing order by their \emph{weight}. The
  sorting operation is made in \emph{log-linear time}, $O(|E|\lg |E|)$;
\item selecting each edge in a non decreasing order by their weight and
  checking if the vertices forming the edge, $(u,v)$, are in the same set of
  vertices. If they are not, the edge will be added to the minimum spanning
  tree, $T$, and the sets will be combined. These
  operations are made in \emph{log-linear time}, $O((|E| + |V|) \lg |V|)$;
\end{itemize}

The time complexity is set by the sorting operation, $O(|E|\lg |E|)$. The graph
is connected therefore can there only be at most $|E| \leq \frac{|V|(|V|-1)}{2} <
|V|^2$ edges. The time complexity is $O(|E|\lg |E|) = O(|E|\lg |V|^2) = O(|E|\lg
|V|)$.

\begin{algorithm}[h!]
  \caption{\FuncSty{Kruskal(}$G$\FuncSty{)}}
  \label{algorithm:kruskal}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $V$ be a set of vertices.}
  \KwData{Let $E$ be a set of edges.}
  \KwData{Let $G$ be a connected undirected graph $G = (V,E)$.}
  \KwData{Let $w$ be the weight of an edge.}
  \KwData{Let $T$ be a subgraph of $G$ where $T = \emptyset$.}
  \SetArgSty{}
  \ForEach{$v \in V$}{
    $V_{v} = \{v\}$ \tcc*[]{create a subset of $V$ containing only $v$}
  }
  \FuncSty{Sort(}$E$\FuncSty{)}\tcc*[]{in a non-decreasing order by $w$}
  \ForEach(\tcc*[f]{in a non-decreasing order by $w$}){$(u,v) \in E$}{
    $V_i \leftarrow\;$ \FuncSty{Find-set(}$u$\FuncSty{)}\;
    $V_j \leftarrow\;$ \FuncSty{Find-set(}$v$\FuncSty{)}\;
    \If{$V_i \neq V_j$}{
      $T \leftarrow T \cup (u,v)$\;
      $V_k \leftarrow\;$ \FuncSty{Union-set(}$V_i,V_j$\FuncSty{)}\;
      \If{$V = V_k$}{
        \FuncSty{break}\;
      }
    }
  }
  \Return{$T$}\;
  \SetArgSty{}
\end{algorithm}

\begin{algorithm}[h!]
  \caption{\FuncSty{Kruskal-filter-helper(}$E$,$T$\FuncSty{)}}
  \label{algorithm:kruskalfilterhelper}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $V$ be a set of vertices.}
  \KwData{Let $E$ be a set of edges.}
  \KwData{Let $w$ be the weight of an edge.}
  \KwData{Let $T$ be a subgraph of $G$.}
  \SetArgSty{}
  \If{$|E| < 2$}{
    \If{$E = \{(u,v)\}$}{
      $V_i \leftarrow\;$ \FuncSty{Find-set(}$u$\FuncSty{)}\;
      $V_j \leftarrow\;$ \FuncSty{Find-set(}$v$\FuncSty{)}\;
      \If{$V_i \neq V_j$}{
        $T \leftarrow T \cup (u,v)$\;
        \FuncSty{Union-set(}$V_i,V_j$\FuncSty{)}\;
      }
    }
    \Return{}\;
  }
  $q \leftarrow \;$ \FuncSty{Pivot(}$E,p,r$\FuncSty{)}\;
  $q' \leftarrow \;$ \FuncSty{Partition(}$E,p,q,r$\FuncSty{)}\tcc*[]{in a non-decreasing order by $w$}
  $E_1 \leftarrow E\{e_p,\dots,e_{q'-1}\}$\;
  $E_2 \leftarrow E\{e_{q'},\dots,e_r\}$\;
  \FuncSty{Kruskal-filter-helper(}$E_1,T$\FuncSty{)}\;
  \ForEach{$(u,v) \in E_2$}{
    $V_i \leftarrow\;$ \FuncSty{Find-set(}$u$\FuncSty{)}\;
    $V_j \leftarrow\;$ \FuncSty{Find-set(}$v$\FuncSty{)}\;
    \If{$V_i = V_j$}{
      $E_2 \leftarrow E_2 \backslash (u,v)$\;
    }
  }
  \FuncSty{Kruskal-filter-helper(}$E_2,T$\FuncSty{)}\;
  \SetArgSty{}
\end{algorithm}

The problem with the algorithm is when a graph is \emph{dense}, each vertex has
many neighbor vertices, and all the edges have to be sorted. To optimize the
algorithm could be to break out of the loop if there only was one set of
vertices. This will ensure that all the vertex in the remaining edges would
already be in the minimum spanning tree. The problem is that the sorting
algorithm already sorted all the edges in $O(|E| \lg |E|)$ and iterating through
all the edges can be done in linear time $O(|E|)$. A possibility could be
introducing a heap that can be made in $O(|E|)$ time and even though all
elements are removed, in case a heavy weighted edge is in the minimum spanning
tree, the combined time complexity would still be in $O(|E| \lg |E|)$. Another
approach is to use a filter \cite{OSS:2009:USA} that will exclude all heavy
edges before checking for a valid edge. The algorithm is similar to
\algo{Quicksort}. It starts by choosing a random edge as pivot and it partition
the edges into two list, the first containing all the edges were their weights
are less or equal to the pivot edge weight and the other list containing all the
edges were their weights greater than the pivot edge weight. The algorithm is
called recursively on the first list until a single edge is reached. The edge
will be added to the minimum spanning tree, $T$, if the vertices forming the
edge, $(u,v)$, are not in the same set of vertices. Once the first part is
returned from the recursion, the filter will now be applied on the second list
by checking if the vertex, $(u,v)$, in each edge are in the same set of
vertices. If this is the case, the edge can safely be removed because both
vertex are already part of the minimum spanning tree. Pseudocode for the
algorithm can be seen in Algorithms \ref{algorithm:kruskalfilter} and
\ref{algorithm:kruskalfilterhelper}.

\begin{figure}[h!]
  \centerline{
    \includegraphics[height=6cm,keepaspectratio=true]
    {figures/mst_kruskalfilter_dag.pdf}
  }
  \caption{Representation of \algo{Kruskal-filter} computation as a DAG.}
  \label{fig:mst:kruskalfilter}
\end{figure}

\begin{algorithm}[h!]
  \caption{\FuncSty{Kruskal-filter(}$G$\FuncSty{)}}
  \label{algorithm:kruskalfilter}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $V$ be a set of vertices.}
  \KwData{Let $E$ be a set of edges.}
  \KwData{Let $G$ be a connected undirected graph $G = (V,E)$.}
  \KwData{Let $T$ be a a subgraph of $G$ where $T = \emptyset$.}
  \SetArgSty{}
  \ForEach{$v \in V$}{
    $V_{v} = \{v\}$ \tcc*[]{create a subset of $V$ containing only $v$}
  }
  \FuncSty{Kruskal-filter-helper(}$E,T$\FuncSty{)}\;
  \Return{$T$}\;
  \SetArgSty{}
\end{algorithm}

When parallelizing the algorithm usage of a heap must be avoided, as in the
previous section where the \algo{Prim} algorithm was not parallelizable because
it uses heap as the primary data structure. By looking at the
\algo{Kruskal-filter} algorithm there are really no parallelizable parts. The
\algo{Quicksort} is used but it cannot run both subtrees of the recursion
concurrently. The last one has to wait until the first one returns and then it
can apply the filter on the second branch of the recursion tree. In
\cite{OSS:2009:USA} the parallel sorting algorithm is called whenever the
\emph{threshold} is reached. The interesting part would be to know the precise
value of the \emph{threshold}. The performance test of the sorting algorithms
from the previous chapter, shows that \emph{MCSTL parallel sort} only performs
better than the best sequential \emph{STL sort} when the number of elements are
greater than \emph{$2^{13}$}. Why not sort all the edges with \emph{MCSTL
  parallel sort}?  The performance test of the sorting algorithms, showed a
speedup of more than \emph{20} when using a computer with \emph{32} kernels
compared to the speedup of less than \emph{2} using \emph{MCSTL quicksort},
assuming that \algo{Kruskal-filter} is implemented in the same way as the
\emph{MCSTL quicksort} algorithm. More information about performance tests on
the sorting algorithms can be seen in Chapter \ref{results}.

The \emph{work} for the \algo{Kruskal} algorithm is $O(|E| \lg |V|)$ and the
\emph{span} is $O(|E| \lg |V|)$. The \emph{critical path} is bounded by the set
operations because a parallel sorting algorithm can be used in order to the
reduce the subpath for the sort of the edges from $O(|E| \lg |E|)$ to $O(|E|)$:
\begin{align}
  T_k(n) &= \sum_{i = 1}^{\lg{|E|}}{\frac{|E|}{i}} + O((|E| + |V|) \lg |V|) \\
  &= |E| + \frac{|E|}{2} + \frac{|E|}{4} + ... + 2 + O(|E| \lg |V|) \\
  &= O(|E| \lg |V|)
\end{align}
A visual representation in form a \emph{directed acyclic graph} can be seen in
Figure \ref{fig:mst:kruskal}.

On the other hand the \emph{work} for the \algo{Kruskal-filter} algorithms is
$O(|E|^2)$ for the \emph{worst-case} and $O(|E| \lg{|V|})$ for the
\emph{average-case}. Respectively the \emph{span} is $O(|E|^2)$ for the
\emph{worst-case} and $O(|E|)$ for the \emph{average-case}.  The \emph{critical
  path} for the \emph{worst-case} is bounded to the partition and the unbalanced
recursion tree as in \algo{Quicksort}:
\begin{align}
  T_k(n) &= \sum_{i = 0}^{|E|-1}{|E|-i} + O((|E| + |V|) \lg |V|) \\
  &= |E| + (|E|-1) + (|E|-2) + ... + 2 + O(|E| \lg |V|) \\
  &= O(|E|^2)
\end{align}
The \emph{critical path} for the \emph{average-case} is bounded bounded by the
set operations:
\begin{align}
  T_k(n) &= \sum_{i = 1}^{\lg{|E|}}{\frac{|E|}{i}} + O((|E| + |V|) \lg |V|) = \\
  &= |E| + \frac{|E|}{2} + \frac{|E|}{4} + ... + 2 + O(|E| \lg |V|) \\
  &= O(|E| \lg |V|)
\end{align}
A visual representation in form a \emph{directed acyclic graph} can be seen in
Figure \ref{fig:mst:kruskalfilter}

\subsection*{Implementation details}
Both algorithms are implemented as sequential version but only \algo{Kruskal} is
implemented in a parallel version by replacing \emph{C++ STL sort} with
\emph{MCSTL parallel sort} and can be seen in Appendix
\ref{appendixcdmstsrc}. The set operations rely on \emph{C++ STL set} container.

\section{Bor\r{u}vka (Sollin)}
\label{pmst:boruvka}

\begin{figure}[h!]
  \begin{minipage}[b]{0.5\linewidth}
    \includegraphics[height=6cm,keepaspectratio=true]
    {figures/mst_boruvka_dag.pdf}
  \end{minipage}
  \caption{Representation of \algo{Bor\r{u}vka} computation as a DAG.}
  \label{fig:mst:boruvka}
\end{figure}

\begin{algorithm}[h!]
  \caption{\FuncSty{Bor\r{u}vka(}$G$\FuncSty{)}}
  \label{algorithm:boruvka}
  \SetKwComment{Comment}{/* }{ */}
  \SetArgSty{}
  \KwData{Let $V$ be a set of vertices.}
  \KwData{Let $E$ be a set of edges.}
  \KwData{Let $G$ be a connected undirected graph $G = (V,E)$.}
  \KwData{Let $w$ be the weight of an edge.}
  \KwData{Let $T$ be a subgraph of $G$ where $T = \emptyset$.}
  \KwData{Let $S$ be a set of supervertices where $S = \emptyset$.}
  \KwData{Let $s_{T}$ be a local \emph{minimum spanning tree}.}
  \KwData{Let $s_{adj}$ be a set of edges that $s$ is adjacent to.}
  \KwData{Let $s_{vertices}$ be a set of vertices contained in $s$.}
  \SetArgSty{}
  \ForEach(\tcc*[f]{convert each $v$ to a supervertex}){$v \in V$}{
    $S \leftarrow S \cup v$\;
  }
  \While{$|S| > 1$}{
    \ForEach{$s \in S$}{
      $(u,v) \in s_{adj}$\tcc*[]{select the edge with the lowest $w$}
      $S_i \leftarrow\;$ \FuncSty{Find-supervertex(}$u$\FuncSty{)}\;
      $S_j \leftarrow\;$ \FuncSty{Find-supervertex(}$v$\FuncSty{)}\;
      \If{$S_i \neq S_j$}{
        $s_T \leftarrow\; s_T \cup (u,v)$\;
        \FuncSty{Union-supervertex(}$S_i,S_j$\FuncSty{)}\;
        \ForEach{$(u',v') \in s_{adj}$}{
          $S_{i'} \leftarrow\;$ \FuncSty{Find-supervertex(}$u'$\FuncSty{)}\;
          $S_{j'} \leftarrow\;$ \FuncSty{Find-supervertex(}$v'$\FuncSty{)}\;
          \If{$S_{i'} = S_{j'}$}{
            $s_{adj} \leftarrow s_{adj} \backslash (u',v')$\;
          }
        }
      }
    }
  }
  $s \in S$\;
  \Return{$s_{T}$}\;
  \SetArgSty{}
\end{algorithm}

Bor\r{u}vkas algorithm, in Algorithm \ref{algorithm:kruskal}, works in a similar
manner as the \algo{Kruskal} algorithm and finds the \emph{minimum spanning
  tree}, $T$, in a \emph{connected and undirected graph}, $G(V,E)$, by:
\begin{itemize}
\item initially creating a \emph{supervertex} for each of the vertex. Each
  supervertex contain a local minimum spanning tree, $s_T$, a set of adjacent
  edges to the supervertex, $s_{adj}$, and all the vertices contained in the
  supervertex, $s_{vertices}$. These operations are made in \emph{linear time},
  $O(|E| + |V|)$;
\item taking for each of the supervertices set of adjacent edges, $s_{adj}$, the
  edge with the lowest weight. The two supervertices forming the edge will be
  combined into a new supervertex performing a union on the local minimum
  spanning trees, $s_T$, the sets of adjacent edges, $s_{adj}$, and and list of
  vertices contained in each of the supervertex, $s_{vertices}$. These
  operations are made in \emph{linear time}, $O(|E|)$;
\item afterwards performing a contraction on the set of adjacent edges,
  $s_{adj}$. in order to find the next lowest adjacency edge to another
  supervertex. The operation is made in \emph{linear time}, $O(|E|)$;
\item repeating the previous two operations until there is more than one
  supervertex. These operations are made in \emph{log-linear time}, $O(|E| \lg
  |V|)$;
\end{itemize}

The algorithm can also be thought of as a \emph{bottom-up mergesort} starting
with $|V|$ supervertices and \emph{merging} them together ending up with one
final supervertex containing all the vertices and the final local minimum
spanning tree. The time complexity of the the algorithm is $O(|E| \lg |V|)$.

Because of the similarity to the mergesort algorithm, the algorithm should be
parallelizable. The main problem with respect to the merge operation in
\algo{Mergesort}, where no other process will access two sublist while they are
begin merged, is that several supervertices could try to combine with a common
supervertex. In order to ensure that only two supervertices are combined at a
given time a lock mechanism needs to be introduced. This approach will create a
even bigger problem. Assuming that there exists a graph where all the edges are
connected to only one common edge. In order to combine all the supervertices
into a single vertex all the computations have to be done sequentially. As in
\algo{Prim} with the heap data structure as bottleneck, the algorithm will not
gain a better performance when adding more processes.

The \emph{work} for the algorithm is $O(|E| \lg |V|)$ for both the
\emph{worst-case} and \emph{best-case}. Respectively the \emph{span} for the
\emph{worst-case} is $O(|E| \lg |V|)$ and $O(|E|)$ for the \emph{best-case}.
The \emph{critical path} for the \emph{worst-case} is bounded to the lock
mechanism on the merging of the supervertices:
\begin{align}
  T_k(n) &= O(|E| + |V|) + O(E) + O(E) + O(|E| \lg |V|) \\
  &= O(|E| \lg |V|)
\end{align}
The \emph{critical path} for the \emph{best-case} is:
\begin{align}
  T_k(n) &= O(|E| + |V|) + O(E) + O(E) + \sum_{i = 1}^{\lg{|V|}}{\frac{|E|}{i}} \\
  &= O(|E|) + O(|E|) + O(|E|) + |E| + \frac{|E|}{2} + \frac{|E|}{4} + ... + 2 \\
  &= O(|E|)
\end{align}

Because the studied algorithms are accepted or rejected on basis of their
\emph{worst-case} or \emph{average-case} time complexities this algorithm is
rejected, as \algo{Prim} was, and there will not be performed initial
experiments. A visual representation in form a \emph{directed acyclic graph} can
be seen in Figure \ref{fig:mst:boruvka}.

\subsection*{Implementation details}
The algorithm is only implemented in a sequential version and can be seen in
Appendix \ref{appendixcdmstsrc}. A \emph{supervertex} class is introduced
expanding the initial \emph{vertex} class with the following properties:
\emph{local minimum spanning tree}, \emph{adjacency list of edges} and
\emph{list of contained vertices}.

An implementation of this algorithm exists for the \emph{boost} library,
\cite{CC:1996:USA,DG:1998:USA}. The \emph{practical efficiency
  measures}\footnote{http://www.boost.org/doc/libs/1\_41\_0/libs/graph\_parallel/doc/html/
  dehne\_gotz\_min\_spanning\_tree.html} shows an almost \emph{perfect linear
  speedup} for \emph{dense graphs}.

A \emph{dense graph} is defined as a graph, $G(V,E)$, where $V$ is a set of
vertices and $E$ is a set of edges, where the set of edges $E$ are close to the
maximum number of edges. The maximum number of edges for a \emph{connected and
  undirected graph} is $\frac{|V|(|V|-1)}{2}$ which corresponds to a
\emph{complete graph}. Taking the values used by \emph{boost} where $|V| =
100000$ and $|E| = 15000000$ the percentage of how dense the graph is, will be
calculated:
\begin{align}
  \intertext{By assuming that the graph is complete, then it would have a
    density of \emph{100\%}:}
  100\; \% & = \frac{|V|(|V| - 1)}{2} = \frac{100000 (100000 - 1)}{2}
  \intertext{Calculating the density of the graph used by boost is very simple
    with cross-multiplication:}
  x\; \% & = 15000000 \\
  x\; \% &= \frac{100 \cdot 15000000}{\frac{100000 (100000 - 1)}{2}}\\
  &= \underline{\underline{0.300003\; \%}}\label{mst:density}
\end{align}

From (\ref{mst:density}) a conclusion can be made that the used graphs are not
dense.
