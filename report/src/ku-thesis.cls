\NeedsTeXFormat{LaTeX2e}[1900/01/01]
\ProvidesClass{ku-thesis}[1900/01/01 v1.0 Copenhagen University thesis class]

%%%% Global variables

%%%% Requierment for this class (enumerate used packages)

%%%% Inherit from book.cls
\DeclareOption{a4paper}{\PassOptionsToClass{a4paper}{book}}
\DeclareOption{12pt}{\PassOptionsToClass{12pt}{book}}
\DeclareOption{draft}{\PassOptionsToClass{draft}{book}}
\DeclareOption{final}{\PassOptionsToClass{final}{book}}
\DeclareOption{openright}{\PassOptionsToClass{openright}{book}}
\DeclareOption{oneside}{\PassOptionsToClass{oneside}{book}}
\DeclareOption{twoside}{\PassOptionsToClass{twoside}{book}}
\DeclareOption{dvips}{\PassOptionsToClass{dvips}{book}}
\DeclareOption{pdftex}{\PassOptionsToClass{pdftex}{book}}

%%%% Give a warning for:
\DeclareOption{draft}{\ClassWarning{ku-thesis}
        {Option 'draft' this is not a final version of the thesis.}}

%%%% Don't inherit from book.cls
% TODO: learn how to create global variables and error messages.
\DeclareOption{10pt}{\ClassError{ku-thesis}
        {Option '10pt' not available for ku-thesis.}}
\DeclareOption{11pt}{\ClassError{ku-thesis}
        {Option '11pt' not available for ku-thesis.}}

%%%% Load report
% TODO: look if we load other stuff if they give letter format
\ExecuteOptions{a4paper,12pt,twopages,openright,pdftex}
\ProcessOptions
\LoadClass{book}[1900/01/01]

%%%% Redefine commands
\renewcommand*{\title}[1]{\gdef\kutitle{#1}}
\renewcommand*{\author}[1]{\gdef\kuauthor{#1}}
\renewcommand*{\date}[1]{\gdef\kudate{#1}}
\renewcommand*{\maketitle}{
  \pagestyle{empty}
  \makefrontpage
  % \makeinfopage <--- we don't need a info page ... maybe?
}

%%%% Add commands
\newcommand*{\subtitle}[1]{\gdef\kusubtitle{#1}}
\newcommand*{\advisor}[1]{\gdef\kuadvisor{#1}}
\newcommand*{\department}[1]{\gdef\kudepartment{#1}}

%%%% Load packages

%%% Other
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{amsthm} % newtheroem + proof env.
\usepackage[T1]{fontenc}
\usepackage{ae,aecompl}
\usepackage{tikz}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{longtable}
\usepackage{multirow}
\usepackage{rotating} % sideways envirenment
\usepackage{lscape} % landscape
\usepackage{psfrag,color} % laprint
\usepackage{pstricks} % basic pstricks support
\usepackage[small,hang,bf,up]{caption}
\usepackage[refpage]{nomencl}
\makenomenclature
\newcommand{\nomunit}[1]{%
\renewcommand{\nomentryend}{\hspace*{\fill}#1}}
\usepackage{url} % for list of references
\usepackage{listings}
\usepackage{color}
\usepackage{textcomp}
\usepackage{lscape} % for landscape
\usepackage{pdflscape} % for pdf landscape

\setcounter{tocdepth}{2} 
\setcounter{secnumdepth}{3} 
\def\quote{\list{}{\rightmargin\leftmargin}\item[]}
\let\endquote=\endlist
\def\changemargin#1#2{\list{}{\rightmargin#2\leftmargin#1}\item[]}
\let\endchangemargin=\endlist

\usepackage{todo}

%%% Avoid problems with special letter by using UTF-8. Support for EN & DA.
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

%%% Graphics support

%%% Math support

%%% Algorithm support

%%% Inlcude pdf files

%%% PDF section
\usepackage[plainpages=false,pdftex]{hyperref} 
\hypersetup{pdftitle={\title{}},
  pdfauthor={\author{}},
  colorlinks = true,
  %linkcolor = blue,
  %urlcolor  = blue,
  %citecolor = red,
  linkcolor=black,
  urlcolor=black,
  citecolor=black,
  breaklinks,
  linktocpage,
  bookmarksopen,
  bookmarksopenlevel=1,
  bookmarksnumbered
}

%%%% Layout

% Setup headings and title pages for chapters
%
%IMM PHD THESIS LAYOUT
%Adopted from Henrik Aalborg Nielsen and Thomas Fabricius
%Jan Larsen, IMM, DTU, Nov 2003

%%%%%%%%%%%%%%%%%%%%%%%   page geometry
\setlength{\parskip}{0.5\baselineskip}
\setlength{\parindent}{0pt}

%%%%%%%%%%%%%%%%%%%%%%%   Headings
\pagestyle{fancyplain}
\addtolength{\headheight}{1.8pt}
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\lhead[\fancyplain{}{\sffamily\bfseries\thepage}]%
      {\fancyplain{}{\sffamily\bfseries\rightmark}}
\rhead[\fancyplain{}{\sffamily\bfseries\leftmark}]%
      {\fancyplain{}{\sffamily\bfseries\thepage}}
\cfoot{}

%%%%%%%%%%%%%%%%%%%%%%%   Chapters
\ifx\thesislanguage\danishlang
\renewcommand{\chaptername}{K a p i t e l }
\renewcommand{\appendixname}{A p p e n d i k s }
\else
\renewcommand{\chaptername}{C h a p t e r }
\renewcommand{\appendixname}{A p p e n d i x }
\fi

\def\@makechapterhead#1{%
  \vspace*{50\p@}{
    \parindent \z@ \raggedright \normalfont
    \ifnum \c@secnumdepth >\m@ne
    \if@mainmatter
    \large\scshape \@chapapp \space\space \Huge\upshape\thechapter
    \par\nobreak
    \vskip 20\p@
    \fi
    \fi
    \interlinepenalty\@M
    \flushleft\parbox{\textwidth}{\raggedleft \sffamily\Huge\bfseries  #1}
    \par\nobreak
    \vskip 15pt
    \hrule height 0.4pt
    \vskip 80\p@
  }
}

\def\@makeschapterhead#1{%
  \vspace*{50\p@}{
    \parindent \z@ \raggedright \normalfont
    \interlinepenalty\@M
    \sffamily\huge\bfseries  #1
    \par\nobreak
    \vskip 15pt
    \hrule height 0.4pt
    \vskip 80\p@
  }
}

%%% Coverpages
%\RequirePackage[OT2,OT4]{fontenc}
\RequirePackage{eso-pic,graphicx,fix-cm,ae,aecompl,ifthen}         %
\RequirePackage[usenames]{color} %

% TODO: hardcoded, fix this
\newcommand*{\makefrontpage}{
  \begin{titlepage}
    \AddToShipoutPicture*{
      \put(0,0){
        \includegraphics*[viewport=0 0 700 600]{ku-covers/nat-farve.pdf}
      }
    }
    \AddToShipoutPicture*{
      \put(0,602){
        \includegraphics*[viewport=0 600 700 1600]{ku-covers/nat-farve.pdf}
      }
    }
    \AddToShipoutPicture*{
      \put(0,0){
        \includegraphics*{ku-covers/nat-en.pdf}
      }
    }
    \AddToShipoutPicture*{
      \put(50,583.5){
        %\fontsize{20 pt}{22 pt}\selectfont Master's thesis  
        \fontsize{18 pt}{22 pt}\selectfont Master's thesis  
      }
    }
    \AddToShipoutPicture*{
      \put(50,555.3){
        \fontsize{14 pt}{16 pt}\selectfont \kuauthor  
      }
    }
    \AddToShipoutPicture*{
      \put(50,499){
        %\fontsize{20 pt}{24 pt}\selectfont \kutitle
        \fontsize{20 pt}{24 pt}\selectfont \kutitle
      }
    }
    \AddToShipoutPicture*{
      \put(50,480.5){
        \fontsize{14 pt}{16 pt}\selectfont \kusubtitle  
      }
    }
    \AddToShipoutPicture*{
      \put(50,92){
        \fontsize{11 pt}{12 pt}\selectfont Advisor: \kuadvisor
      }
    }
    \AddToShipoutPicture*{
      \put(50,66.7){
        \fontsize{11 pt}{12 pt}\selectfont Submitted: \kudate
      }
    }
    \phantom{}
  \end{titlepage}
}

\newcommand*{\makeinfopage}{
  \begin{titlepage}
    \AddToShipoutPicture*{
      \put(50,583.5){
        \fontsize{20 pt}{22 pt}\selectfont Information page
      }
    }
    \phantom{}
  \end{titlepage}
}

\newcommand*{\makebackpage}{
  \begin{titlepage}
    \phantom{}
  \end{titlepage}
}

%%%% 

%%% Computer Science
%% Listings for code hightligt

%% Algorithms package
%\usepackage[algosection,boxruled,noend]{algorithm2e}
%\usepackage[algochapter, vlined, ruled, linesnumbered]{algorithm2e}
\usepackage[algosection, vlined, ruled, linesnumbered]{algorithm2e}

%% Inference

%%% Mathematics

%% Look at DTU

%%% Other
